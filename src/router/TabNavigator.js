import React, {useState, useEffect} from 'react';
import {Modal, View, StyleSheet} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import {FAB} from 'react-native-paper';

import TabBar from '../components/TabBar';
import Chats from '../pages/Chats';
import GroupChats from '../pages/GroupChats';

const Tabs = createMaterialTopTabNavigator(
  {
    Chats: {screen: Chats},
    Group: {screen: GroupChats},
  },
  {
    initialRouteName: 'Chats',
    tabBarComponent: props => <TabBar {...props} />,
  },
);

const TabNavigator = props => {
  const {navigation} = props;
  const {index} = navigation.state;

  const renderFABonIndex = () => {
    switch (index) {
      case 0:
        return (
          <FAB
            style={styles.fab}
            icon="message-plus"
            color="white"
            onPress={() => navigation.navigate('NewChat')}
          />
        );
      case 1:
        return (
          <View>
            <FAB
              style={styles.fab}
              icon="account-multiple-plus"
              color="white"
              onPress={() => navigation.navigate('AddParticipants')}
            />
          </View>
        );
    }
  };

  return (
    <View style={styles.container}>
      <Tabs navigation={navigation} />
      {renderFABonIndex()}
    </View>
  );
};

TabNavigator.router = Tabs.router;

const styles = StyleSheet.create({
  container: {flex: 1},
  fab: {
    position: 'absolute',
    bottom: 32,
    right: 24,
    backgroundColor: '#917369',
  },
  editFab: {
    position: 'absolute',
    bottom: 100,
    right: 32,
  },
});

export default TabNavigator;
