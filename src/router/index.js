import React from 'react';
import {View, StatusBar, Platform} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Splash,
  Login,
  Profile,
  Home,
  Simulasi,FilePendukung,History,Panduan,Survey,StatusPermohonan,StatusPermohonanOperator,
  PengajuanVerifikasi,ProsesVerifikasi,BeforeFeedback,
  PenerbitanBilling,PengajuanPembayaran,PdfExample,PengajuanPerbaikan,DraftPermohonanPerbaikan,
  PengajuanPembayaran2,
  Esertifikat,DiTerima,
  Feedback,
  Informasi,
  Archive,
  Profile1,
  ProfileEdit,WebViewScreen,
  Notifikasi,HomeOperator,Useraktivasi
  
} from '../pages';
import Chats from '../pages/Chats';
import { useDispatch, useSelector } from 'react-redux';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import BottomNavigator from '../components/BottomNavigator';
import {colors} from '../utils';

import NewChat from '../pages/Chats/NewChat';
import ChatRoom from '../pages/Chats/ChatRoom';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <>
      {Platform.OS === 'ios' && (
        <View style={{backgroundColor: colors.primary, height: 40}}>
          <StatusBar
            backgroundColor={colors.primary}
            barStyle="light-content"
          />
        </View>
      )}
      <Tab.Navigator
        screenOptions={{headerShown: false}}
        tabBar={props => <BottomNavigator {...props} />}>
       <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Profile" component={Profile} /> 
      </Tab.Navigator>
    </>
  );
};

const OperatorApp = () => {
  return (
    <>
      {Platform.OS === 'ios' && (
        <View style={{backgroundColor: colors.primary, height: 40}}>
          <StatusBar
            backgroundColor={colors.primary}
            barStyle="light-content"
          />
        </View>
      )}
      <Tab.Navigator
        screenOptions={{headerShown: false}}
        tabBar={props => <BottomNavigator {...props} />}>
         {/* Menu Home Operator*/}
        <Tab.Screen name="Home" component={HomeOperator} />
        {/* Etc */}
        <Tab.Screen name="Profile" component={Profile} /> 
      </Tab.Navigator>
    </>
  );
};

const Router = () => {
 
  const {reducerSession} =  useSelector(state => state); 
  return (
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Login" component={Login} />
      {/* Menu Home Pengguna Jasa*/}
      {reducerSession.role.name==='Operator' ? <Stack.Screen name="OperatorApp" component={OperatorApp} /> : <Stack.Screen name="MainApp" component={MainApp} />}

      <Stack.Screen name="ChatRoom" component={ChatRoom} />
      <Stack.Screen name="Chats" component={Chats} />
      <Stack.Screen name="NewChat" component={NewChat} />
      <Stack.Screen name="StatusPermohonan" component={StatusPermohonan} />
      <Stack.Screen name="StatusPermohonanOperator" component={StatusPermohonanOperator} />
      <Stack.Screen name="Useraktivasi" component={Useraktivasi} />
      <Stack.Screen name="ProsesVerifikasi" component={ProsesVerifikasi} />
      <Stack.Screen name="Simulasi" component={Simulasi} />
      <Stack.Screen name="FilePendukung" component={FilePendukung} />
      <Stack.Screen name="History" component={History} />
      <Stack.Screen name="Panduan" component={Panduan} />
      <Stack.Screen name="Survey" component={Survey} />
      <Stack.Screen name="Profile1" component={Profile1} />
      <Stack.Screen name="ProfileEdit" component={ProfileEdit} />
      <Stack.Screen name="WebViewScreen" component={WebViewScreen} />
      <Stack.Screen name="PengajuanVerifikasi" component={PengajuanVerifikasi} />
      <Stack.Screen name="BeforeFeedback" component={BeforeFeedback} />
      <Stack.Screen name="PenerbitanBilling" component={PenerbitanBilling} />
      <Stack.Screen name="PengajuanPembayaran" component={PengajuanPembayaran} />
      <Stack.Screen name="PdfExample" component={PdfExample} />
      <Stack.Screen name="PengajuanPerbaikan" component={PengajuanPerbaikan} />
      <Stack.Screen name="DraftPermohonanPerbaikan" component={DraftPermohonanPerbaikan} />
      <Stack.Screen name="PengajuanPembayaran2" component={PengajuanPembayaran2} />
      <Stack.Screen name="Esertifikat" component={Esertifikat} />
      <Stack.Screen name="DiTerima" component={DiTerima} />
      <Stack.Screen name="Feedback" component={Feedback} />
      <Stack.Screen name="Informasi" component={Informasi} />
      <Stack.Screen name="Archive" component={Archive} />
      <Stack.Screen name="Notifikasi" component={Notifikasi} />
      
      
     
      
    </Stack.Navigator>
  );
};

export default Router;
