import React, { useEffect,createRef } from 'react';
import {Platform,SafeAreaView, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {Provider, useSelector} from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import FlashMessage from 'react-native-flash-message';
import messaging from '@react-native-firebase/messaging';
import { localPushNotification } from './utils/pushNotification';

import Router from './router';
import store, { persistor } from './redux/store';
import {Loading} from './components';
import { colors } from './utils';
import PushNotification, { PushNotificationObject } from 'react-native-push-notification';
import { OrientationLocker, PORTRAIT } from "react-native-orientation-locker";
import { NativeBaseProvider, Text, Box } from 'native-base';

export let navigationRef = createRef();

const MainApp = () => {
  const {loading} = useSelector(state => state.loading);
  return (
    <NativeBaseProvider>
    <NavigationContainer ref={navigationRef}>
      {loading && <Loading />}
      <OrientationLocker
        orientation={PORTRAIT}
        onChange={orientation => console.log('onChange', orientation)}
        onDeviceChange={orientation => console.log('onDeviceChange', orientation)}
      />
      <Router />
      
      <FlashMessage position="up"/>
    </NavigationContainer>
    </NativeBaseProvider>
  );
};

const App = () => {

  
  useEffect(() => {

    PushNotification.createChannel(
      {
        channelId: "fcm_fallback_notification_channel", // (required)
        channelName: "fcm_fallback_notification_channel", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        playSound: false, // (optional) default: true
       // soundName: "sampleaudio", // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );

    const requestPermission = async () => {
      const authStatus = await messaging().requestPermission();
      const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;
        console.log('Authorization status:', authStatus);
    
      if (enabled) {
        console.log('Authorization status:', authStatus);
      }
     
    }

    const onNotification = (data) => {
      const { messageId, title, body } = data;
      console.log('=== notif ===', data);
      
      localPushNotification.showNotification(
        messageId,
        title,
        body,
        
      );


    }

    requestPermission();

    const unsubscribe = messaging().onMessage(async remoteMessage => {
      if (remoteMessage) {
        console.log('=== remoteMessage ===', remoteMessage);
        const data = {
          messageId: remoteMessage.messageId,
          title: Platform.OS === 'ios' ? remoteMessage.data.notification.title : remoteMessage.notification.title,
          body: Platform.OS === 'ios' ? remoteMessage.data.notification.body : remoteMessage.notification.body,
        };

        onNotification(data);
      }
    });

    const unsubsOpenApp = messaging().onNotificationOpenedApp(async remoteMessage => {
      alert('notif dibuka', remoteMessage.data);
    });

    return () => {
      unsubscribe();
      unsubsOpenApp();
    }
  })
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <StatusBar backgroundColor={colors.primary} />
        <SafeAreaView style={{backgroundColor: colors.primary}} />
        <MainApp />
      </PersistGate>
    </Provider>
  );
};

export default App;
