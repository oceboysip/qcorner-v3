const mainColors = {
    green1: '#f0daab',
    grey2: '#E9E9E9',
    grey3:'#494747',
    grey4:'#959595',
    grey5:'#CACACA',
    blue1: '#0066CB',
    white1:'#FFFFFF',
    black1:'#000000',
    dark2: '#495A75',
    red1: '#E06379',
  };

  export const colors = {
    primary: mainColors.green1,
    border1: mainColors.grey3,
    placeholder: '#858885',
    enable: mainColors.blue1,
    white1: mainColors.white1,
    text: {
      primary: mainColors.white1,
      secondary: mainColors.black1,
      label: mainColors.grey4,
      menuInactive: mainColors.dark2,
      menuActive: mainColors.white1,
    },
    success: '#0BBC2E',
    info: '#0787D7',
    warning: '#F5B72C',
    error: mainColors.red1,
    darkGrey: '#666666',
  };