export const handleStatePaginated = (status, data, currentArr, currentPage, isInitial) => {
    let safeArr = Array.isArray(data) ? data : [];
    let newArr = [];
    let newPage = -1;

    if (status && safeArr.length > 0) {
        newArr = safeArr.map(e => {
            return e;
        });
    }

    if (isInitial && newArr.length > 0) {
        newPage = 2;
    } else if (!isInitial && newArr.length > 0) {
        newPage = currentPage + 1;
    }

    return {
        data: isInitial ? newArr : currentArr.concat(newArr),
        page: newPage,
    };
}