import axios from 'axios';
import {CommonActions} from '@react-navigation/native';
import base64 from 'react-native-base64';

import {navigationRef} from '../App';
import store from '../redux/store';

// 'http://apiv2.bbkpsoetta.com/'; // staging

export const baseURL = 'http://apiv2.bbkpsoetta.com/'; // Live
const basicPrefix = '';
const bearerPrefix = '';
const principal = 'principal/api/';
const screenLogin = 'Login';
const statusCodeTokenExpired = [401];
const username = 'champion';
const password = 'juara2022';
const basicAuth = base64.encode(`${username}:${password}`);
const requestHttp = axios.create({baseURL});

const redirect = async () => {
  // clear session token
  await store.dispatch({type: 'USER.CLEAR_AUTH'});

  // handle multiple redirect
  const safeNavigation =
    navigationRef.current &&
    typeof navigationRef.current.getRootState === 'function';
  if (safeNavigation) {
    const navRoutes = await navigationRef.current.getRootState();
    if (
      Array.isArray(navRoutes.routes) &&
      typeof navRoutes.index === 'number' &&
      typeof navRoutes.routes[navRoutes.index] !== 'undefined' &&
      navRoutes.routes[navRoutes.index].name === screenLogin
    ) {
      return;
    }
  }

  // redirect
  await navigationRef.current.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{name: screenLogin, params: {logout: true}}],
    }),
  );
};

const errorResponse = (data, messageCode) => {
  return {
    status: typeof data.status !== 'undefined' ? data.status : messageCode,
    message:
      typeof data.message !== 'undefined'
        ? data.message
        : 'Tidak terhubung ke server',
    content: data,
  };
};


export const postLOGIN = async(endpoint,data,headers) =>{
  //console.log(endpoint);
  const url = baseURL+endpoint;
  const body = data;
  const config = {
    headers: {
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  try {
    
    const result = await requestHttp.post(url, body, config);
    if (result.data.status === true) {
      return result.data;
    }

    throw {
      status: result.status,
      message: result.statusText,
      content: result,
    };
  } catch (error) {
    if (statusCodeTokenExpired.includes(error.response.status)) {
      redirect();
    }

    throw errorResponse(error.response.data, error.message);
  }
}
export const postHttpBasic = async (endpoint, data, headers) => {
  const url = basicPrefix + endpoint;
  const body = data;
  const config = {
    headers: {
      Authorization: 'Basic ' + basicAuth,
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  try {
    const result = await requestHttp.post(url, body, config);

    if (result.status === 200) {
      return result.data;
    }

    throw {
      status: result.status,
      message: result.statusText,
      content: result,
    };
  } catch (error) {
    if (statusCodeTokenExpired.includes(error.response.status)) {
      redirect();
    }

    throw errorResponse(error.response.data, error.message);
  }
};

export const postAPI = async (endpoint, body, headers) => {
  const auth = await store.getState()['reducerSession'];

  if (auth.token === null || auth.token === '') {
    redirect();
    return;
  }

  const url = bearerPrefix + endpoint;
  const config = {
    headers: {
      Authorization: 'Bearer ' + auth.token,
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  try {
    const result = await requestHttp.post(url, body, config);

    if (result.status === 200) {
      return result.data;
    }

    throw {
      status: result.status,
      message: result.statusText,
      content: result,
    };
  } catch (error) {
    if (statusCodeTokenExpired.includes(error.response.status)) {
      redirect();
    }

    throw errorResponse(error.response.data, error.message);
  }
};

export const putAPI = async (endpoint, body, headers) => {
  const auth = await store.getState()['reducerSession'];

  if (auth.token === null || auth.token === '') {
    redirect();
    return;
  }

  const url = bearerPrefix + endpoint;
  const config = {
    headers: {
      Authorization: 'Bearer ' + auth.token,
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  try {
    const result = await requestHttp.put(url, body, config);

    if (result.status === 200) {
      return result.data;
    }

    throw {
      status: result.status,
      message: result.statusText,
      content: result,
    };
  } catch (error) {
    if (statusCodeTokenExpired.includes(error.response.status)) {
      redirect();
    }

    throw errorResponse(error.response.data, error.message);
  }
};

export const getAPI = async (endpoint, headers) => {
  const auth = await store.getState()['reducerSession'];

  if (auth.token === null || auth.token === '') {
    redirect();
    return;
  }

  const url = bearerPrefix + endpoint;
  const config = {
    headers: {
      Authorization: 'Bearer ' + auth.token,
      'Content-Type': 'application/json',
      ...headers,
    },
  };
  console.log('ini configan:',config);
  try {
    const result = await requestHttp.get(url, config);

    if (result.status === 200) {
      return result.data;
    }

    throw {
      status: result.status,
      message: result.statusText,
      content: result,
    };
  } catch (error) {
    if (statusCodeTokenExpired.includes(error.response.status)) {
      redirect();
    }

    throw errorResponse(error.response.data, error.message);
  }
};

export const deleteAPI = async (endpoint, headers) => {
  const auth = await store.getState()['reducerSession'];

  if (auth.token === null || auth.token === '') {
    redirect();
    return;
  }

  const url = bearerPrefix + endpoint;
  const config = {
    headers: {
      Authorization: 'Bearer ' + auth.token,
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  try {
    const result = await requestHttp.delete(url, config);

    if (result.status === 200) {
      return result.data;
    }

    throw {
      status: result.status,
      message: result.statusText,
      content: result,
    };
  } catch (error) {
    if (statusCodeTokenExpired.includes(error.response.status)) {
      redirect();
    }

    throw errorResponse(error.response.data, error.message);
  }
};

export const getPrincpal = async (endpoint, headers) => {
  const url = principal + endpoint;
  const config = {
    headers: {
      Authorization: `Basic ${basicAuth}`,
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  try {
    const result = await requestHttp.get(url, config);

    if (result.status === 200) {
      return result.data;
    }

    throw {
      status: result.status,
      message: result.statusText,
      content: result,
    };
  } catch (error) {
    if (statusCodeTokenExpired.includes(error.response.status)) {
      redirect();
    }

    throw errorResponse(error.response.data, error.message);
  }
};
