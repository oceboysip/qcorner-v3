import BGHeader from './header.png';
import BGNotif from './notif.png';
import BGtri from './3.png';
import BGPnm from './pnmplsa.png';
import BGLink from './linkpulsa.png';
import BGLinkAja from './payment/link-aja.png';
import BGovo from './payment/ovo.png';
import BGGopay from './payment/gopay.png';
import BGDana from './payment/dana.png';
import BGPnmJuara from './payment/pnm-juara.png';
export {
  BGHeader,
  BGNotif,
  BGtri,
  BGPnm,
  BGLink,
  BGLinkAja,
  BGovo,
  BGGopay,
  BGDana,
  BGPnmJuara,
};
