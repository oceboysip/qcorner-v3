import ILBanner1 from './1.jpg';
import ILBanner2 from './2.jpg';
import ILBanner3 from './3.jpeg';
import ILBook1 from './book.png';
import ILStars from './stars.png';
import ILDiskon from './Diskon.png';
import ILPhoto from './poto.png';
import ILOnePiece from './one-piece.jpg';

export {
  ILOnePiece,
  ILBanner1,
  ILBanner2,
  ILBanner3,
  ILBook1,
  ILStars,
  ILDiskon,
  ILPhoto,
};
