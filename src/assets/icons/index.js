import IconSplash from './LogoUlamm.png';
import IconUser from './personal.png';
import IconKey from './Key.png';
import IconEye from './eye.png';
import IconPemandangan from './Pemandangan.png';
import IconKedai from './kedai.png';
import IconDompet from './dompet.png';
import IconPersonalOrder from './personal-order.png';
import IconGroupOrder from './group-order.png';
import IconHistoryOrder from './history.svg';
import IconPaketData from './paket-data.svg';
import IconLainnya from './lainnya.svg';
import IconPulsa from './pulsa.svg';
import IconPLN from './pln.svg';
import IconNotif from './notif.svg';
import IconNotifOff from './notif-off.svg';
import IconCart from './cart.svg';
import IconCartOff from './cart-off.svg';
import IconAccount from './profileoff.svg';
import IconAccountOff from './profileon.svg';
import IconBack from './back.png';
import IconJual from './Jual.svg';
import IconBeli from './Beli.svg';
import IconHome from './home.svg';
import IconHomeOff from './home_off.svg';
import IconBuku from './buku.png';
import IconDapur from './dapur.png';
import IconElektronik from './elektronik.png';
import IconFashionAnak from './fashionanak.png';
import IconHijab from './Hijab.png';
import IconPria from './pria.png';
import IconWanita from './wanita.png';
import IconFilm from './film.png';
import IconGame from './game.png';
import IconPhone from './hanphone.png';
import IconIbu from './ibu.png';
import IconCamera from './kamera.png';
import IconKecantikan from './kecantikan.png';
import IconKesehatan from './kesehatan.png';
import IconKomputer from './komputer.png';
import IconOlahraga from './olahraga.png';
import IconOtomotif from './motor.png';
import IconHewan from './hewan.png';
import IconTubuh from './tubuh.png';
import IconTukang from './tukang.png';
import IconRumah from './rumahtangga.png';
import IconWedding from './wedding.png';
import IconSearch from './search.png';
import IconButtonRight from './buttonright.svg';
import IconNotifBell from './notif-bell.png';
import IconPNMTrans from './pnm-transparent.svg';
import IconCartSm from './ic-cart.svg';
import IconCoug from './ic-coug.svg';
import IconMail from './ic-mail.svg';
import IconProduct from './ic-product.svg';
import IconChevronUp from './chevron-up.svg';
import IconPromo from './promo.svg';
import IconSecure from './secure.svg';
import IconStarsD from './stars_detail.png';
import IconHeart from './heart.png';
import IconWhatsApp from './whatsapp.svg';
import IconTrash from './trash.svg';
import IconTriangle from './triangle.svg';
import IconPencil from './pencil.svg';
import IconCameraPNG from './camera.png';
import IconEdit from './ic-edit.svg';
import IconRemove from './ic-remove.svg';
import IconContact from './contactlist.png';
import IconClose from './close.svg';
import IconPlus from './plus.svg';
import IconRefresh from './refresh.svg';
import IconWallet from './wallet.svg';
import IconCross from './cross-svg.svg';
import IconOption from './option.svg';
import IconCheck from './check.png';
import IconAlertCircle from './alert-circle.svg';
import IconOptionGrey from './option-grey.svg';
import IconCookingSet from './cooking-set.png';
import IconBoxEmpty from './box-open-empty.svg';
import IconShelf from './shelfs.svg';
import IconChartCheck from './chart-checklist.svg';
import IconVoucher from './voucher.svg';
import IconChevronWhite from './chevron-white.svg';
import IconWalletWhite from './wallet-white.svg';
import IconKedaiSvg from './kedai-svg.svg';
import IconMinus from './minus.svg';
import IconBell from './bell.svg';
import IconBucket from './bucket.svg';
import IconShop from './shop.svg';
import IconCalendar from './calendar.png';
import IconInfo from './information.svg';
import IconToko from './toko.svg';
import IconCameraTrans from './camera-trans.svg';
import IconBarChart from './bar-chart.svg';
import IconContainer from './container-trans.svg';
import IconCartTrans from './cart-trans.svg';
import IconCog from './cog-trans.svg';
import IconMenu from './item-trans.svg';
import IconSearchon from './search.svg';
import IconSearchOff from './search_off.svg';
import IconHistory from './IconHistory.svg';
import IconHistoryOff from './IconHistoryOff.svg';
export {
  IconContainer,
  IconHistory,
  IconHistoryOff,
  IconCartTrans,
  IconCog,
  IconSearchon,
  IconSearchOff,
  IconMenu,
  IconBarChart,
  IconCameraTrans,
  IconToko,
  IconInfo,
  IconCalendar,
  IconShop,
  IconBucket,
  IconBell,
  IconMinus,
  IconKedaiSvg,
  IconWalletWhite,
  IconChevronWhite,
  IconVoucher,
  IconCookingSet,
  IconBoxEmpty,
  IconShelf,
  IconChartCheck,
  IconOptionGrey,
  IconAlertCircle,
  IconCheck,
  IconOption,
  IconCross,
  IconWallet,
  IconPlus,
  IconRefresh,
  IconCameraPNG,
  IconPencil,
  IconWhatsApp,
  IconTriangle,
  IconTrash,
  IconEdit,
  IconRemove,
  IconChevronUp,
  IconPromo,
  IconSecure,
  IconCartSm,
  IconCoug,
  IconMail,
  IconProduct,
  IconBack,
  IconNotif,
  IconNotifOff,
  IconCart,
  IconCartOff,
  IconAccount,
  IconAccountOff,
  IconPaketData,
  IconLainnya,
  IconPulsa,
  IconPLN,
  IconSplash,
  IconUser,
  IconKey,
  IconEye,
  IconPemandangan,
  IconKedai,
  IconDompet,
  IconPersonalOrder,
  IconGroupOrder,
  IconHistoryOrder,
  IconBeli,
  IconJual,
  IconHome,
  IconHomeOff,
  IconBuku,
  IconDapur,
  IconElektronik,
  IconFashionAnak,
  IconHijab,
  IconPria,
  IconWanita,
  IconFilm,
  IconGame,
  IconPhone,
  IconTubuh,
  IconIbu,
  IconCamera,
  IconKecantikan,
  IconKesehatan,
  IconKomputer,
  IconOlahraga,
  IconOtomotif,
  IconHewan,
  IconTukang,
  IconRumah,
  IconWedding,
  IconSearch,
  IconButtonRight,
  IconNotifBell,
  IconPNMTrans,
  IconStarsD,
  IconHeart,
  IconContact,
  IconClose,
};
