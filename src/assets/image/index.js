import background from './background.png';
import BGprofilLIUKnogaris from './BGprofilLIUKnogaris.png';
import logo from './logo.png';
import back from './back.png'
import edit from './editprofil.png'
import cewekditolak from './cewekditolak.png'
import editprofil from './editprofil.png';
import dokumensaya from './dokumensaya.png'
import karaketer2 from './karaketer2.png';
import onboarding from './onboarding.png';
import onboarding2 from './onboarding2.png';
import boarding3 from './boarding3.png';
import homekartun from './homekartun.png';
import icon_bell_coklat from './icon_bell_coklat.png';
import homesimulasi from './simulasi.png';
import homecekstatus from './cek_status_permohonan.png';
import berhasil from './berhasil.png';
import homehistory from './history_permohonan.png';
import homepanduan from './karimin_cewek_baca.png';
import qna from './qna.png';
import Logo_Barantan from './Logo_Barantan.png';
import kontak_kami from './kontak_kami.png';
import logoutimg from './wew.png';
import simulasikartun from './simulasikartun.png';
import pengajuan_ppk from './pengajuan_ppk.png';
import bgadacewek from './bgstatuspakecewe.png';
import cari from './Cari.jpeg';
import dua from './2.png'
import pengajuansertifikat from './pengjuanpreviewcertifikat.png';
import tiga from './3.png'
import icondown from './icons8-collapse-arrow-down.png'
import banktransfer from './banktransfer.png'
import sertifikat from './sertifikat.png';
import pengajuanpreviewsertifikat from './sertifikatpreview.png'
import esertifikatbwh from './esertifikatbwh.jpeg'
import billings from './billings.png';
import cetakpermohonan from './bgcekcetatuspermohonan.png';
export {
  background,edit,cewekditolak,simulasikartun,pengajuan_ppk,logoutimg,kontak_kami,dokumensaya,editprofil,Logo_Barantan,back,logo,karaketer2,onboarding2,onboarding,boarding3,homekartun,icon_bell_coklat,
  homecekstatus,BGprofilLIUKnogaris,homesimulasi,berhasil,homepanduan,homehistory,qna,bgadacewek,cari,dua,pengajuansertifikat,tiga,icondown,banktransfer,sertifikat,pengajuanpreviewsertifikat,
  esertifikatbwh,billings,cetakpermohonan,
};
