import JSONListProduct from './product-list.json';
import JSONListPulsa from './pulsa-list.json';
import JSONAddresses from './addresses.json';
import JSONBalanceHistory from './balance-histories.json';
import JSONBanks from './banks.json';
import JSONMyProduct from './my-product.json';
import JSONStorefront from './storefront.json';
import JSONHistory from './history.json';
import JSONNotification from './notification.json';
import JSONProductPerform from './product-perform.json';

export {
  JSONProductPerform,
  JSONNotification,
  JSONHistory,
  JSONStorefront,
  JSONMyProduct,
  JSONListProduct,
  JSONListPulsa,
  JSONAddresses,
  JSONBalanceHistory,
  JSONBanks,
};
