import React from 'react'
import { StyleSheet, Text, View,StatusBar as StatusBarRN } from 'react-native'

const StatusBar = () => {
    return (
        <View style={styles.container}>
            <StatusBarRN backgroundColor="#24B1A8"/>
        </View>
    )
}

export default StatusBar

const styles = StyleSheet.create({container:{
    flex:1,
}})
