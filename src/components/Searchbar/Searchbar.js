import React from 'react';
import {View, Text, TextInput, Image, StyleSheet} from 'react-native';
import {Line} from '..';
import {IconSearch} from '../../assets';

export default function Searchbar({placeholder = '', onFocus = null}) {
  return (
    <View style={styles.view}>
      <TextInput
        placeholder={placeholder}
        style={styles.input}
        onFocus={onFocus}
      />
      <Line height="80%" color="grey" />
      <Image source={IconSearch} style={styles.img} resizeMethod="scale" />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    fontFamily: 'Poppins-Regular',
    includeFontPadding: false,
    paddingHorizontal: 15,
    flex: 1,
  },
  view: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderColor: 'grey',
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
  },
  img: {
    width: 28,
    height: 28,
    marginHorizontal: 10,
  },
});
