import React, { useState, useEffect } from 'react';
import { View, TextInput, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Octicons from 'react-native-vector-icons/Octicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { colors as Color } from '../utils';
import { Text } from '.';

const propTypes = {
    // children: PropTypes.node.isRequired,
    type: PropTypes.string,
    style: PropTypes.object,
    color: PropTypes.string,
    onPress: PropTypes.func,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    onChangeText: PropTypes.func,
};

const defaultProps = {
    type: 'component',
    style: {},
    onPress: () => {},
    value: '',
    placeholder: '',
    label: '',
    onChangeText: () => {},
};

const FormInputSelect = (props) => {
    const { type, style, color, onPress, value, onChangeText, placeholder, label } = props;

    if (type === 'input') {
        return (
            <View style={{paddingHorizontal: 16, ...style}}>
                <View style={{flexDirection: 'row', paddingHorizontal: 16, minHeight: 40, alignItems: 'center', justifyContent: 'space-between', backgroundColor: Color.theme, borderRadius: 8, borderColor: Color.placeholder, borderWidth: 0.5}}>
                    <TextInput
                        placeholder='Cari lokasi'
                        placeholderTextColor={Color.placeholder}
                        value={value}
                        onChangeText={onChangeText}
                        style={{width: '90%', height: '100%'}}
                    />
                    <Octicons name='search' size={16} />
                </View>
            </View>
        );
    }

    return (
        <TouchableOpacity
            activeOpacity={0.75}
            onPress={() => onPress()}
            style={{width: '100%'}}
        >
            {label !== '' && <Text size={15} align='left' color={color} letterSpacing={0.16}>
                {label}
            </Text>}

            <View style={{flexDirection: 'row', paddingRight: 8, paddingBottom: 8, minHeight: 40, alignItems: 'center', justifyContent: 'space-between', backgroundColor: Color.theme, borderColor: color || Color.placeholder, borderBottomWidth: 0.5, ...style}}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                    <View>
                        <Text
                            size={15}
                            color={color || (value ? Color.text.label : Color.placeholder)}
                            style={{marginTop: 2}}
                        >
                            {value || placeholder}
                        </Text>
                    </View>
                </View>
                <FontAwesome
                    name='caret-down'
                    size={18}
                />
            </View>
        </TouchableOpacity>
    );
}

FormInputSelect.propTypes = propTypes;
FormInputSelect.defaultProps = defaultProps;
export default FormInputSelect;