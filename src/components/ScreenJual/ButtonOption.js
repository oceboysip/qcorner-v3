import {StyleSheet, Text, TouchableNativeFeedback, View} from 'react-native';
import React from 'react';

export default function ButtonOption({title = '', icon, onPress, badgeCount}) {
  return (
    <TouchableNativeFeedback useForeground onPress={onPress}>
      <View style={styles.view}>
        {icon}
        <Text>{title}</Text>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'white',
    elevation: 3,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
