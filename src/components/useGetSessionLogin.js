import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

const useGetSessionLogin = () => {
  const [sessionLogin, setSessionLogin] = useState(null);
  const [loading, setLoading] = useState(false);

  const getToken = async () => {
    setLoading(true);
    try {
      let jsonValue = await AsyncStorage.getItem('mySess');
      setSessionLogin(jsonValue != null ? JSON.parse(jsonValue) : null);
      setLoading(false);
    } catch (error) {
      setSessionLogin(null);
      setLoading(false);
      console.log('==============ERROR=============');
      console.log(error);
    }
  };
  useEffect(() => {
    getToken();
  }, []);

  return {sessionLogin, loading};
};

export default useGetSessionLogin;
