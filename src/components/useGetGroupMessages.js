import React, {useState, useEffect} from 'react';
import database from '@react-native-firebase/database';
//import firebase from 'react-native-firebase';
import utf8 from 'utf8';
import base64 from 'base-64';

export default function useGetGroupMessages(option) {
  const {email, groupName} = option;
  const [dataMessages, setDataMessages] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (email !== undefined && groupName !== undefined) {
      // Convert to base64
      const group_name_bytes = utf8.encode(option.groupName);
      const group_name_encode = base64.encode(group_name_bytes);
      // const user_bytes = utf8.encode(option.email);
      // const user_encode = base64.encode(user_bytes);
      setLoading(true);
      const onMessagesChange = database()
        .ref(`/messages/${group_name_encode}`)
        .on('value', snapshot => {
          // set here
          setDataMessages(snapshot.val());
          setLoading(false);
        });
      return () =>
        database()
          .ref(`/messages/${group_name_encode}`)
          .off('value', onMessagesChange);
    }
  }, [email, groupName, option.email, option.groupName]);

  return {
    dataMessages,
    loading,
  };
}
