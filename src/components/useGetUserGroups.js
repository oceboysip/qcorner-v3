import React, {useState, useEffect} from 'react';
 import database from '@react-native-firebase/database';
//import firebase from 'react-native-firebase';
import utf8 from 'utf8';
import base64 from 'base-64';
import _ from 'lodash';

export default function useGetUserGroups(userEmail) {
  const [dataGroups, setDataGroups] = useState([]);
  const [loading, setLoading] = useState(false);
  let userValue = null;
  let conversationValue = null;

  const fetchAllGroups = current_email_encode => {
    setLoading(true);
    userValue = database()
      .ref(`/users/${current_email_encode}`)
      .on('value', user => {
        if (!_.isNil(user.val())) {
          const filterGroups = _.pickBy(user.val().groups, group => {
            return group === true;
          });
          const groups = _.map(filterGroups, (val, groupId) => {
            return groupId;
          });
          for (let i = 0; i < groups.length; i++) {
            conversationValue = firebase
              .database()
              .ref(`/user_conversations/${groups[i]}`)
              .on('value', user_conversations => {
                database()
                  .ref('groups/' + groups[i])
                  .on('value', group => {
                    const filterMembers = _.pickBy(
                      group.val().members,
                      member => {
                        return member === true;
                      },
                    );
                    const members = _.map(filterMembers, (value, idMember) => {
                      return idMember;
                    });
                    // //set here
                    if (user_conversations.val() !== null) {
                      setDataGroups(prev => {
                        let newGroups = [...prev];
                        const indexGroup = newGroups.findIndex(
                          group => group.groupId === groups[i],
                        );
                        if (indexGroup !== -1) {
                          newGroups[indexGroup] = {
                            groupId: groups[i],
                            groupName: group.val().group_name,
                            groupDescription: group.val().group_description,
                            members,
                            lastMessage: user_conversations.val().lastMessage,
                            name: user_conversations.val().name,
                            time: user_conversations.val().time,
                          };
                        } else {
                          newGroups = [
                            ...newGroups,
                            {
                              groupId: groups[i],
                              groupName: group.val().group_name,
                              groupDescription: group.val().group_description,
                              members,
                              lastMessage: user_conversations.val().lastMessage,
                              name: user_conversations.val().name,
                              time: user_conversations.val().time,
                            },
                          ];
                        }
                        return newGroups;
                      });
                    } else {
                      setDataGroups(prev => {
                        let newGroups = [...prev];
                        const indexGroup = newGroups.findIndex(
                          group => group.groupId === groups[i],
                        );
                        if (indexGroup !== -1) {
                          newGroups[indexGroup] = {
                            groupId: groups[i],
                            groupName: group.val().group_name,
                            groupDescription: group.val().group_description,
                            members,
                          };
                        } else {
                          newGroups = [
                            ...newGroups,
                            {
                              groupId: groups[i],
                              groupName: group.val().group_name,
                              groupDescription: group.val().group_description,
                              members,
                            },
                          ];
                        }
                        return newGroups;
                      });
                    }
                  });
              });
          }
        }

        setLoading(false);
      });
  };
  const refresh = () => {
    setDataGroups([]);
    const current_email_bytes = utf8.encode(userEmail);
    const current_email_encode = base64.encode(current_email_bytes);
    fetchAllGroups(current_email_encode);
  };

  useEffect(() => {
    if (userEmail !== null) {
      // Convert to base64
      const current_email_bytes = utf8.encode(userEmail);
      const current_email_encode = base64.encode(current_email_bytes);
      fetchAllGroups(current_email_encode);
      return () => {
        database()
          .ref(`/users/${current_email_encode}`)
          .off('value', userValue);
      };
    }
  }, [userEmail]);
  return {
    dataGroups,
    loading,
    refresh,
  };
}
