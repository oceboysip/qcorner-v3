import React, { } from 'react';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';
import { View, StatusBar, SafeAreaView, useWindowDimensions } from 'react-native';

import { Header } from '.';
import { colors as Color } from '../utils';
import { isIphoneNotch, statusBarHeight } from '../utils';
import { ScreenEmptyData, ScreenIndicator } from '.';
// import Popup from 'src/components/Popup';

const propTypes = {
    children: PropTypes.node,
    header: PropTypes.node,
    headerTitle: PropTypes.string,
    showHeader: PropTypes.bool,
    iconRightButton: PropTypes.node,
    fallback: PropTypes.bool,

    empty: PropTypes.bool,
    emptyTitle: PropTypes.string,
    emptyComponent: PropTypes.node,
    emptyButtonLabel: PropTypes.string,
    emptyButtonColor: PropTypes.string,
    emptyButtonPress: PropTypes.func,

    popupProps: PropTypes.object,
    isLoading: PropTypes.bool,
    style: PropTypes.object,
    useSafeArea: PropTypes.bool,
    translucent: PropTypes.bool,
    statusBarColor: PropTypes.string,
    floatingActionButton: PropTypes.PropTypes.node,
};

const defaultProps = {
    headerTitle: '',
    useSafeArea: true,
    showHeader: true,
    fallback: false,

    empty: false,
    emptyTitle: 'Data tidak tersedia',
    emptyButtonLabel: '',
    emptyButtonPress: () => {},

    popupProps: {
        visible: false
    },
    isLoading: false,
    style: {},
    translucent: false,
    floatingActionButton: null,
};

const Scaffold = ({
    children,
    header,
    headerTitle,
    useSafeArea,
    translucent,
    statusBarColor,
    showHeader,
    iconRightButton,
    onPressLeftButton,
    fallback,

    empty,
    emptyTitle,
    emptyComponent,
    emptyButtonLabel,
    emptyButtonColor,
    emptyButtonPress,

    popupProps,
    isLoading,
    style,
    floatingActionButton,
}) => {
    const navigation = useNavigation();
    const { width, height } = useWindowDimensions();

    const MainView = useSafeArea ? SafeAreaView : View;

    return (
        <MainView style={{flex: 1, backgroundColor: Color.theme, ...style }}>
            <StatusBar
                translucent={translucent}
                backgroundColor={statusBarColor || Color.theme}
            />

            {translucent && <View style={{height: statusBarHeight, backgroundColor: statusBarColor || Color.theme}} />}

            {showHeader && header ?
                header
            : showHeader ?
                <Header
                    title={headerTitle}
                    // onPressLeftButton
                    onPress={() => onPressLeftButton ? onPressLeftButton() : navigation.pop()}
                    iconRightButton={iconRightButton}
                />
            : null}

            {fallback ?
                <ScreenIndicator transparent />
            : empty && !isLoading ?
                emptyComponent ?
                    emptyComponent
                :
                    <ScreenEmptyData
                        message={emptyTitle}
                        buttonLabel={emptyButtonLabel}
                        buttonColor={emptyButtonColor}
                        onButtonPress={() => emptyButtonPress()}
                    />
            :
                children
            }

            {isLoading && <View style={{
                position: 'absolute',
                alignSelf: 'flex-end',
                alignItems: 'center',
                justifyContent: 'center',
                width,
                height: translucent ?
                    height + statusBarHeight
                :
                    height - (showHeader ? 60 + (isIphoneNotch() ? statusBarHeight : 0) : 0),
                top: showHeader ? 60 + (isIphoneNotch() ? statusBarHeight : 0) : 0,
                backgroundColor: Color.overflow,
            }}>
                <ScreenIndicator transparent={false} />
            </View>}

            {translucent && <View style={{height: isIphoneNotch() ? statusBarHeight : 0}} />}

            {floatingActionButton &&
                <View style={{position: 'absolute', right: 16, bottom: 0, backgroundColor: 'transparent'}}>
                    {floatingActionButton}
                </View>
            }

            {/* <Popup isTranslucent={translucent} { ...popupProps } /> */}
        </MainView>
    )
};

Scaffold.propTypes = propTypes;
Scaffold.defaultProps = defaultProps;

export default Scaffold;