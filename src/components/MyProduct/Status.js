import React from 'react';
import {View, Text, TouchableNativeFeedback, StyleSheet} from 'react-native';
import {colors} from '../../utils';

const StatusProduct = ({onPress, title = '', selected, index}) => {
  return (
    <TouchableNativeFeedback useForeground onPress={onPress}>
      <View
        style={{
          ...styles.view,
          borderColor: selected == index ? colors.primary : 'grey',
        }}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </TouchableNativeFeedback>
  );
};

const styles = StyleSheet.create({
  view: {
    padding: 5,
    paddingHorizontal: 15,
    marginRight: 5,
    elevation: 3,
    borderRadius: 35 / 2,
    borderWidth: 1,
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: 'white',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    includeFontPadding: false,
  },
});

export default StatusProduct;
