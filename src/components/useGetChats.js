import React, {useState, useEffect} from 'react'; 
import database from '@react-native-firebase/database';

import utf8 from 'utf8';
import base64 from 'base-64';
import _ from 'lodash';

export default function useGetChats(userEmail) {
  const [loading, setLoading] = useState(false);
  const [dataChats, setDataChats] = useState([]);

  const fetchCurrentUserChats = currentUserEmail => {
    setLoading(true);
    database()
      .ref(`/user_conversations/${currentUserEmail}`)
      .on('value', user_conversations => {
       database()
          .ref(`/users`)
          .on('value', users_of_contacts => {
            const contacts = _.map(users_of_contacts.val(), (value, uid) => {
              return {...value, uid};
            });
            const conversations = _.map(
              user_conversations.val(),
              (value, uid) => {
                return {...value, uid};
              },
            );
            let array_merged = [];
            let count = 0;
            let i = 0;
            let y = 0;
            for (i = 0; i < conversations.length; i++) {
              for (y = 0; y < contacts.length; y++) {
                if (conversations[i].email === contacts[y].email) {
                  array_merged[count] = {...conversations[i], ...contacts[y]};
                  count++;
                }
              }
            }
            //set here
            setDataChats(array_merged);
            setLoading(false);
          });
      });
  };

  const refresh = () => {
    setDataChats([]);
    const current_email_bytes = utf8.encode(userEmail);
    const current_email_encode = base64.encode(current_email_bytes);
    fetchCurrentUserChats(current_email_encode);
  };

  useEffect(() => {
    if (userEmail !== null) {
      // Convert to base64
      const current_email_bytes = utf8.encode(userEmail);
      const current_email_encode = base64.encode(current_email_bytes);
      fetchCurrentUserChats(current_email_encode);
    }
  }, [userEmail]);
  return {
    loading,
    dataChats,
    refresh,
  };
}
