import React from 'react';
import {StyleSheet, Text, View, TextInput, Image} from 'react-native';
import {colors} from '../../utils';
import {IconSearch} from '../../assets';

const Search = ({placeholder}) => {
  return (
    <View style={styles.container}>
      <TextInput placeholder="masukkan nama" style={styles.input} />
      <Image source={IconSearch} style={styles.images} />
    </View>
  );
};

export default Search;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.border1,
    borderRadius: 5,
    height: 35,
    marginHorizontal: 20,
    marginVertical: 20,
  },
  input: {
    fontSize: 14,
    flex: 1,
  },
  images: {
    width: 25,
    height: 25,
  },
});
