import React from 'react';
import {FlatList, ImageBackground, StyleSheet, Text, View} from 'react-native';

import {Avatar, Divider, TouchableRipple} from 'react-native-paper';

import ChatText from './ChatText';

const defaultAvatar = '';

const ChatItem = props => {
  const {item, onPress} = props;

  return (
    <View>
      <TouchableRipple
        onPress={onPress}
        rippleColor="rgba(0, 0, 0, .32)"
        style={styles.touchable}>
        <View style={styles.item}>
          <Avatar.Image
            size={50}
            source={
              item.picture
                ? {
                    uri: item.picture.thumbnail,
                  }
                : defaultAvatar
            }
          />
          <View style={styles.main}>
            <View style={styles.desp}>
              <ChatText style={styles.name} value={item.name} />
              <ChatText style={styles.time} value={'6:13 am'} />
            </View>

            <ChatText
              style={styles.msg}
              numberOfLines={1}
              value={'Hi, Man, How are you?'}
            />
          </View>
        </View>
      </TouchableRipple>
      <Divider style={styles.divider} />
    </View>
  );
};

const styles = StyleSheet.create({
  touchable: {paddingHorizontal: 10, paddingVertical: 2},
  item: {flex: 1, flexDirection: 'row', height: 70, alignItems: 'center'},
  main: {
    flex: 1,
    paddingLeft: 15,
    justifyContent: 'center',
  },
  desp: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 6,
  },
  name: {fontWeight: 'bold', fontSize: 18, color: 'black'},
  time: {fontSize: 14, color: 'grey'},
  msg: {fontWeight: '400', fontSize: 14, color: 'grey'},
  divider: {marginLeft: 72},
});

export default ChatItem;
