import React from 'react';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';

const ChatText = props => {
  const {style, value, ...otherProps} = props;
  return (
    <Text style={[styles.font, style]} {...otherProps}>
      {value}
    </Text>
  );
};

const styles = StyleSheet.create({
  font: {
    fontFamily: 'Roboto-Regular',
  },
});

export default ChatText;
