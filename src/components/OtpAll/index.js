import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors} from '../../utils';
import OTPInputView from '@twotalltotems/react-native-otp-input';
const OtpAll = ({text, value, onChangeText, onCodeFilled}) => {
  const [pin, setPin] = useState();
  return (
    <View>
      <View style={styles.title}>
        <OTPInputView
          style={{width: '85%', height: 100, color: 'red'}}
          pinCount={6}
          code={value}
          onCodeChanged={val => setPin(val)}
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          onCodeFilled={onCodeFilled}
        />
        <Text style={styles.label}>
          Silahkan masukkan kode otentikasi yang sudah dikirimkan ke nomor
          +628123xxxxxxx.Berlaku hingga 1 Menit, 55 Detik
        </Text>
      </View>
    </View>
  );
};

export default OtpAll;

const styles = StyleSheet.create({
  title: {
    alignItems: 'center',
    alignContent: 'center',
    paddingVertical: 40,
  },
  borderStyleBase: {
    width: 30,
    height: 20,
  },

  borderStyleHighLighted: {
    borderColor: '#03DAC6',
    color: '#000000',
  },

  underlineStyleBase: {
    width: 50,
    height: 40,
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: '#24B1A8',
    color: 'black',
  },

  underlineStyleHighLighted: {
    borderColor: '#000000',
    color: 'black',
  },
  label: {
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    textAlign: 'center',
    paddingHorizontal: 20,
    bottom: 15,
    color: colors.border1,
  },
});
