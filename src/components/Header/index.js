import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

import {back, IconBack} from '../../assets';
import {colors} from '../../utils';

const propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.string,
  automaticallyImplyLeading: PropTypes.bool,
};

const defaultProps = {
  onPress: () => {},
  title: '',
  automaticallyImplyLeading: true,
};


const Header = ({onPress,noback, title}) => {
  return ( <>
    {!noback ? 
<View style={{ flexDirection: "row", paddingVertical: 10 }}>
<TouchableOpacity onPress={onPress} >
<View
  style={{
    flex: 1,
    alignItems: "flex-start",
    paddingTop: 15,
    paddingLeft: 10,
    paddingRight: 10
  }}
>
 
  <Image
    style={{ width: 40, height: 40 }}
    source={back}
  />
</View>
</TouchableOpacity>
<View style={{ flex: 6, alignContent: "flex-start", paddingTop: 20 }}>
<Text style={{ fontSize: 18 }}>{title}</Text>
</View>
</View> : <View style={{ flexDirection: "row", paddingVertical: 10}}>
<TouchableOpacity onPress={onPress} >
<View
  style={{
    flex: 1,
    alignItems: "flex-start",
    paddingTop: 15,
    paddingLeft: 10,
    paddingRight: 10
  }}
>
{/*  
  <Image
    style={{ width: 40, height: 40 }}
    source={back}
  /> */}
</View>
</TouchableOpacity>
<View style={{ flex: 6, alignContent: "flex-start", paddingTop: 20 }}>
<Text style={{ fontSize: 18 ,color:'#ffffff'}}></Text>
</View>
</View> }</>
  );
};

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;
export default Header;
