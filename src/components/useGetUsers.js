import React, {useState, useEffect} from 'react';
import database from '@react-native-firebase/database';
//import firebase from 'react-native-firebase';
import _ from 'lodash';

export default function useGetUsers(email) {
  const [users, setUsers] = useState(null);
  const [loading, setLoading] = useState(false);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchUsers = () => {
    setLoading(true);
    database()
      .ref('users')
      .on('value', snapshot => {
        const person = _.pickBy(snapshot.val(), (snap, id) => {
          return snap.email !== email;
        });
        setUsers(person);
        setLoading(false);
      });
  };

  useEffect(() => {
    if (email !== null && users === null) {
      fetchUsers();
    }
  }, [email, users, fetchUsers]);

  return {
    users,
    loading,
  };
}
