import React from 'react';
import {View, StyleSheet} from 'react-native';

function Line({
  width,
  height,
  color = 'grey',
  flex,
  borderWidth = 0.3,
  align,
  gap,
  gapHorizontal,
  opacity,
}) {
  return (
    <View
      style={{
        width: width,
        height: height,
        flex: flex,
        borderWidth: borderWidth,
        borderColor: color,
        alignSelf: align,
        marginVertical: gap,
        marginHorizontal: gapHorizontal,
        backgroundColor: color,
        opacity: opacity,
      }}
    />
  );
}
export default Line;
