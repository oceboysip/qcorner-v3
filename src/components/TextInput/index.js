import React, {useState} from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View,
  TextInput as TextInputRN,
  Pressable,
} from 'react-native';
import {IconUser, IconKey, IconEye} from '../../assets';
import {Gap} from '../Gap';
import Forms from './Forms';

const TextInput = ({
  title,
  placeholder,
  keyboardType,
  isPassword,
  value,
  onChangeText,
  source,
  type,
  subtext,
  password,
  text,
}) => {
  const [secureTextEntry, setSecureTextEntry] = useState(false);

  const showHidePasswod = () => setSecureTextEntry(!secureTextEntry);
  if (isPassword) {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>{title}</Text>
        <View style={styles.row}>
          <View>
            <Image source={source} style={styles.image} />
          </View>
          <View style={styles.textInputPassword}>
            <TextInputRN
              style={styles.input}
              placeholder={placeholder}
              keyboardType={keyboardType}
              secureTextEntry={!secureTextEntry ? true : false}
              value={value}
              onChangeText={onChangeText}
            />
            <Pressable onPress={showHidePasswod}>
              <Image source={IconEye} style={styles.eye} />
            </Pressable>
          </View>
        </View>
      </View>
    );
  }
  if (type === 'Forms') {
    return (
      <Forms
        title={title}
        placeholder={placeholder}
        subtext={subtext}
        password={password}
        text={text}
        value={value}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
      />
    );
  }
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{title}</Text>
      <View style={styles.row}>
        <View>
          <Image source={source} style={styles.image} />
        </View>
        <TextInputRN
          style={styles.input}
          placeholder={placeholder}
          keyboardType={keyboardType}
          secureTextEntry={secureTextEntry}
          value={value}
          onChangeText={onChangeText}
        />
      </View>
    </View>
  );
};

export default TextInput;

const styles = StyleSheet.create({
  container: {marginBottom: 10, width: '100%'},
  label: {
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: '#202020',
  },
  textInputPassword: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 35,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: 'yellow',
    paddingHorizontal: 16,
    borderRadius: 10,
    paddingVertical: 6,
    backgroundColor: '#fff',
    height: 50,
  },
  image: {
    width: 30,
    height: 30,
  },
  input: {
    flex: 1,
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    paddingLeft: 10,
  },
  eye: {
    width: 30,
    height: 30,
  },
});
