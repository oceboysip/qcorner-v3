import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../../utils';
import {IconEye} from '../../assets';

const Forms = ({
  title,
  placeholder,
  subtext,
  password,
  text,
  value,
  onChangeText,
  keyboardType,
}) => {
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  return (
    <>
      <Text style={styles.text(text)}>{title}</Text>
      <View style={styles.wrapper}>
        <TextInput
          style={styles.input}
          placeholder={placeholder}
          secureTextEntry={password ? secureTextEntry : false}
          subtext={subtext}
          value={value}
          onChangeText={onChangeText}
          keyboardType={keyboardType}
        />
        {password && (
          <TouchableOpacity
            onPress={() => setSecureTextEntry(!secureTextEntry)}>
            <Image source={IconEye} style={styles.img} />
          </TouchableOpacity>
        )}
      </View>
      {subtext && <Text style={styles.textExample}>{subtext}</Text>}
    </>
  );
};

export default Forms;

const styles = StyleSheet.create({
  input: {
    paddingHorizontal: -5,
    flex: 1,
  },
  wrapper: {
    flexDirection: 'row',
    borderBottomColor: 'black',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  text: text => ({
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: text === 'secondary' ? colors.text.label : 'black',
  }),
  textExample: {
    fontSize: 10,
    marginHorizontal: 5,
    marginTop: 5,
  },
  img: {
    width: 30,
    height: 30,
  },
});
