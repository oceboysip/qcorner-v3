import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {colors} from '../../utils';

export default function TextInputCommon({
  title = '',
  placeholder = '',
  onChangeText,
  maxLength,
  value,
  defaultValue,
  flex,
  bgInputColor,
  borderColor = 'grey',
  multiline = false,
  height,
}) {
  const [inputLength, setInputLength] = useState(
    defaultValue ? defaultValue.length : 0,
  );
  const inputCounter = input => setInputLength(input.length);
  return (
    <View style={{flex: flex}}>
      {title != '' && <Text style={styles.title}>{title}</Text>}
      <View
        style={{
          ...styles.viewInput,
          borderColor: borderColor,
          backgroundColor: bgInputColor,
          height: height,
        }}>
        <TextInput
          placeholder={placeholder}
          onChangeText={onChangeText}
          onChange={input => inputCounter(input.nativeEvent.text)}
          style={{...styles.input}}
          maxLength={maxLength}
          value={value}
          defaultValue={defaultValue}
          multiline={multiline}
        />
      </View>
      {maxLength && (
        <Text style={styles.textCounter}>
          {inputLength}/{maxLength}
        </Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  textCounter: {
    fontFamily: 'Poppins-Regular',
    color: colors.text.label,
    textAlign: 'right',
  },
  title: {
    fontFamily: 'Poppins-Medium',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    marginBottom: -3,
    paddingHorizontal: 10,
  },
  viewInput: {
    borderWidth: 1,
    borderRadius: 5,
  },
});
