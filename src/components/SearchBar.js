import React from 'react';
import { View, TextInput, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Octicons from 'react-native-vector-icons/Octicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { colors as Color } from '../utils';
import { Text } from '.';

const propTypes = {
    // children: PropTypes.node.isRequired,
    type: PropTypes.string,
    style: PropTypes.object,
    onPress: PropTypes.func,
    value: PropTypes.string,
    onChangeText: PropTypes.func,
};

const defaultProps = {
    type: 'component',
    style: {},
    onPress: () => {},
    value: '',
    onChangeText: () => {},
};

const SearchBar = (props) => {
    const { type, style, color, onPress, value, onChangeText } = props;

    if (type === 'input') {
        return (
            <View style={{paddingHorizontal: 16, ...style}}>
                <View style={{flexDirection: 'row', paddingHorizontal: 16, minHeight: 40, alignItems: 'center', justifyContent: 'space-between', backgroundColor: Color.theme, borderRadius: 8, borderColor: Color.border, borderWidth: 0.5}}>
                    <TextInput
                        placeholder='Cari lokasi'
                        placeholderTextColor={Color.placeholder}
                        value={value}
                        onChangeText={onChangeText}
                        style={{
                            width: '85%',
                            height: '100%',
                        }}
                    />
                    {value !== '' && <Ionicons name='close' color={Color.red} size={18} onPress={() => onChangeText('')} />}
                    <Octicons name='search' size={16} />
                </View>
            </View>
        );
    }

    return (
        <TouchableOpacity onPress={() => onPress()} style={{paddingHorizontal: 16}}>
            <View style={{flexDirection: 'row', paddingRight: 16, paddingBottom: 8, minHeight: 40, alignItems: 'center', justifyContent: 'space-between', backgroundColor: Color.theme, borderColor: color || Color.border, borderBottomWidth: 1, ...style}}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                    <View style={{marginRight: 12}}>
                        <Entypo name='location-pin' size={22} />
                    </View>
                    <View>
                        <Text size={12} align='left' type='light' color={color}>Lokasi  <Fontisto name='angle-down' color={Color.primaryDark} size={8} /></Text>
                        <Text type='bold' align='left' color={color || Color.primary} style={{marginTop: 2}}>{value}</Text>
                    </View>
                </View>
                <Octicons name='search' color={color || Color.text} size={16} />
            </View>
        </TouchableOpacity>
    );
}

SearchBar.propTypes = propTypes;
SearchBar.defaultProps = defaultProps;
export default SearchBar;