import React from 'react';
import {View, StyleSheet} from 'react-native';

import {isIphoneX} from '../utils/isIphoneX';

export default function TabPadding() {
  return <View style={styles.container} />;
}

const styles = StyleSheet.create({
  container: {
    paddingTop: isIphoneX() ? 154 : 140,
  },
});
