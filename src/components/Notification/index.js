import React from 'react'
import { StyleSheet, Text, View ,Image} from 'react-native'
import  Header  from '../Header';
import  Gap  from '../Gap';
import { BGNotif } from '../../assets/image';

const Notification = ({label}) => {
    return (
        <View style={styles.container}>
            <Header title="Notifikasi" />
                <View style={styles.wrapper}>
                <Image source={BGNotif} style={styles.image}/>
                </View>
                <Text>{label}</Text>
        </View>
    )
}

export default Notification

const styles = StyleSheet.create({
    image:{
        width:147,
        height:147,        
    },
    wrapper:{
        alignItems:'center',
        alignSelf:'center',
        marginTop:170,
    },
    container:{
    
    }
})
