import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {IconButtonRight} from '../../assets';
import {colors} from '../../utils';

const Profile = ({onPress, name, desc, button}) => {
  console.log('desc', desc);
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.content}>
        <Text style={styles.name}>{name}</Text>
        {typeof desc !== 'undefined' && <Text style={styles.desc}>{desc}</Text>}
      </View>
      {button === 'next' && <IconButtonRight style={styles.icon} />}
    </TouchableOpacity>
  );
};

export default Profile;

const styles = StyleSheet.create({
  avatar: {
    width: 46,
    height: 46,
    borderRadius: 46 / 2,
  },
  container: {
    flexDirection: 'row',
    padding: 5,
    borderBottomWidth: 0.5,
    borderBottomColor: '#CBCBCB',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  name: {
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    color: colors.text.secondary,
  },
  desc: {
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
    color: colors.text.secondary,
  },
  content: {
    flex: 1,
    marginLeft: 16,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingVertical: 5,
  },
 icon:{
    paddingLeft: 25,
  }
});
