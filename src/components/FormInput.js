import React, { forwardRef } from 'react';
import { View, TextInput } from 'react-native';
import PropTypes from 'prop-types';
// import CurrencyInput from 'react-native-currency-input';

import {Text} from '.';
import { colors as Color } from '../utils';
import { Container, Line } from '../styled';

const propTypes = {
    placeholder: PropTypes.string,
    onChangeText: PropTypes.func,
    value: PropTypes.any,
    onBlur: PropTypes.func,
    returnKeyType: PropTypes.string,
    onSubmitEditing: PropTypes.func,
    keyboardType: PropTypes.string,
    error: PropTypes.string,

    secureTextEntry: PropTypes.bool,
    suffixIcon: PropTypes.node,
    prefixIcon: PropTypes.node,
};

const defaultProps = {
    placeholder: '',
    onChangeText: () => {},
    onBlur: () => {},
    returnKeyType: 'next',
    onSubmitEditing: () => {},
    keyboardType: 'default',
    error: null,
    
    secureTextEntry: false,
    suffixIcon: null,
    prefixIcon: null,
    multiline: false,
    editable: true,
    format: '', // currency
};

const FormInput = forwardRef((props, ref) => {
    const { placeholder, onChangeText, value, onBlur, returnKeyType, onSubmitEditing, keyboardType, error, secureTextEntry, prefixIcon, suffixIcon, multiline, editable, format } = props;

    return (
        <Container width='100%'>
            {/* <View style={{
                width: 100%;
                justifyContent: flex-start;
                alignItems: flex-start;
            }}> */}
                {/* <Text size={12} letterSpacing={0.08} style={{opacity: 0.6}}>Nama Lengkap</Text> */}
            {/* </View> */}
            <View style={{
                width: '100%',
                flexDirection: 'row',
                height: multiline ? 100 : 50,
                borderRadius: 10,
                backgroundColor: Color.textInput,
            }}>
                {prefixIcon}

                <TextInput
                    ref={ref}
                    placeholder={placeholder}
                    placeholderTextColor={Color.placeholder}
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    onChangeText={(val) => onChangeText(val)}
                    selectionColor={Color.text}
                    value={value}
                    onBlur={() => onBlur()}
                    returnKeyType={returnKeyType}
                    onSubmitEditing={() => onSubmitEditing()}
                    blurOnSubmit={false}
                    keyboardType={keyboardType}
                    style={{
                        textAlignVertical: multiline ? 'top' : 'center',
                        flex: 1,
                        fontSize: 15,
                        fontFamily: 'Poppins-Regular',
                        marginTop: multiline ? 8 : 0,
                        color: Color.gray,
                        includeFontPadding: false,
                    }}
                    secureTextEntry={secureTextEntry}
                    multiline={multiline}
                    editable={editable}
                />

                {/* {format === 'currency' ? 
                    <CurrencyInput
                        onChangeValue={(val) => onChangeText(val)}
                        precision={0}
                        ref={ref}
                        placeholder={placeholder}
                        placeholderTextColor={Color.gray}
                        underlineColorAndroid='transparent'
                        autoCorrect={false}
                        selectionColor={Color.text}
                        value={value}
                        onBlur={() => onBlur()}
                        returnKeyType={returnKeyType}
                        onSubmitEditing={() => onSubmitEditing()}
                        blurOnSubmit={false}
                        keyboardType={keyboardType}
                        style={{
                            // width: '100%',
                            // height: '100%',
                            textAlignVertical: multiline ? 'top' : 'center',
                            flex: 1,
                            fontSize: 15,
                            fontFamily: 'Poppins-Regular',
                            marginTop: multiline ? 8 : 0,
                            paddingHorizontal: 16,
                            color: Color.gray,
                            includeFontPadding: false,
                        }}
                        secureTextEntry={secureTextEntry}
                        multiline={multiline}
                        editable={editable}
                    />
                :
                    <TextInput
                        ref={ref}
                        placeholder={placeholder}
                        placeholderTextColor={Color.gray}
                        underlineColorAndroid='transparent'
                        autoCorrect={false}
                        onChangeText={(val) => onChangeText(val)}
                        selectionColor={Color.text}
                        value={value}
                        onBlur={() => onBlur()}
                        returnKeyType={returnKeyType}
                        onSubmitEditing={() => onSubmitEditing()}
                        blurOnSubmit={false}
                        keyboardType={keyboardType}
                        style={{
                            // width: '100%',
                            // height: '100%',
                            textAlignVertical: multiline ? 'top' : 'center',
                            flex: 1,
                            fontSize: 15,
                            fontFamily: 'Poppins-Regular',
                            marginTop: multiline ? 8 : 0,
                            paddingHorizontal: 16,
                            color: Color.gray,
                            includeFontPadding: false,
                        }}
                        secureTextEntry={secureTextEntry}
                        multiline={multiline}
                        editable={editable}
                    />
                } */}

                {suffixIcon}
            </View>

            <Line height={0.5} width='100%' color={Color.placeholder} />

            <View style={{
                width: '100%',
                paddingTop: 2,
                paddingBottom: 10,
                alignItems: 'flex-start',
            }}>
              <Text size={12} color={Color.error} type='medium' align='left'>{error}</Text>
            </View>
        </Container>
    );
});

FormInput.propTypes = propTypes;
FormInput.defaultProps = defaultProps;
export default FormInput;