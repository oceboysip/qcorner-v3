import React,{useState} from 'react'
import { StyleSheet, Text, View,TouchableOpacity ,Image} from 'react-native'

const ProfileToko = ({onPress}) => {
    const [profile, setProfile] = useState({
        photo:'' ,
        fullName: '',
        profession: '',
      });
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
        <Image source={profile.photo} style={styles.avatar} />
        <View>
          <Text style={styles.name}>{profile.fullName}</Text>
          <Text style={styles.job}>{profile.profession}</Text>
        </View>
      </TouchableOpacity>
    )
}

export default ProfileToko

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
    },
    avatar: {
      height: 46,
      width: 46,
      borderRadius: 46 / 2,
      marginRight: 12,
    },
    name: {
      fontSize: 16,
      color: 'black',
      textTransform: 'capitalize',
    },
    job: {
      fontSize: 12,
      color: 'black',
      textTransform: 'capitalize',
    },
  });
  
