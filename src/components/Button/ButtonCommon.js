import React from 'react';
import {StyleSheet, Text, TouchableNativeFeedback, View} from 'react-native';
import {colors} from '../../utils';

function ButtonCommon({onPress, title = ''}) {
  return (
    <TouchableNativeFeedback useForeground onPress={onPress} style={{ borderRadius:20}}>
      <View style={styles.view}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Medium',
    color: 'white', 
    fontSize: 17,
    textAlign: 'center',
    marginBottom: -3,
  },
  view: {
    marginHorizontal: 30,
    height: 40,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    marginTop: 8,
    borderRadius: 20,
    backgroundColor: '#917369',
    alignItems: 'center',
  },
});

export default ButtonCommon;
