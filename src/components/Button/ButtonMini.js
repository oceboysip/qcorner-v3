import React from 'react';
import {StyleSheet, Text, TouchableNativeFeedback, View} from 'react-native';
import {colors} from '../../utils';

export default function ButtonMini({title = '', onPress, disabled}) {
  return (
    <TouchableNativeFeedback
      useForeground
      onPress={onPress}
      disabled={disabled}>
      <View
        style={{
          ...styles.view,
          backgroundColor: disabled ? 'grey' : colors.primary,
        }}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  view: {
    padding: 5,
    paddingHorizontal: 15,
    borderRadius: 3,
    elevation: 3,
  },
  text: {
    fontFamily: 'Poppins-Medium',
    color: 'white',
    marginBottom: -3,
  },
});
