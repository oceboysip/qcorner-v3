import React from 'react';
import {
  StyleSheet,
  Text,
  Pressable,
  TouchableOpacity,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import {colors} from '../../utils';
import ButtonPlus from './ButtonPlus';
import ButtonSmall from './ButtonSmall';

const Button = ({
  title,
  onPress,
  type,
  disabled,
  children,
  borderRadius = 5,
  fontFamily = 'Poppins-Regular',
}) => {
  if (type === 'button-small') {
    return <ButtonSmall title={title} children={children} onPress={onPress} />;
  }
  if (type === 'button-plus') {
    return <ButtonPlus onPress={onPress} />;
  }
  return (
    <TouchableNativeFeedback
      onPress={onPress}
      useForeground
      disabled={disabled}>
      <View
        style={{
          ...styles.button(type),
          borderRadius: borderRadius,
          backgroundColor: disabled ? 'grey' : colors.primary,
        }}>
        <Text
          style={{
            ...styles.textButton(type),
            fontFamily: fontFamily,
            marginBottom: fontFamily == 'Poppins-Regular' ? -3 : 0,
          }}>
          {title}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
};

export default Button;

const styles = StyleSheet.create({
  button: type => ({
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    elevation: 3,
    padding: 10,
    maxWidth: 480,
    alignSelf: 'center',
    width: '100%',
  }),
  textButton: type => ({
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    color: type === 'secondary' ? colors.primary : colors.white1,
  }),
});
