import React from 'react';
import {Text, StyleSheet, TouchableNativeFeedback, View} from 'react-native';
import {colors} from '../../utils';

function ButtonFloat({onPress, title = '', disabled = false, float = true}) {
  return (
    <TouchableNativeFeedback
      useForeground
      onPress={onPress}
      disabled={disabled}>
      <View
        style={{
          ...styles.buttonSimpan,
          backgroundColor: disabled ? 'grey' : colors.primary,
          ...(float && styles.float),
        }}>
        <Text style={styles.textSimpan}>{title}</Text>
      </View>
    </TouchableNativeFeedback>
  );
}
const styles = StyleSheet.create({
  textSimpan: {
    fontFamily: 'Poppins-Regular',
    color: 'white',
    marginBottom: -3,
    fontSize: 17,
    textAlign: 'center',
  },
  buttonSimpan: {
    width: '80%',
    alignSelf: 'center',
    maxWidth: 480,
    borderRadius: 3,
    elevation: 3,
    paddingVertical: 10,
    overflow: 'hidden',
  },
  float: {
    position: 'absolute',
    bottom: 15,
  },
});

export default ButtonFloat;
