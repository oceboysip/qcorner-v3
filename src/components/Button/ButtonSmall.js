import React, {ReactNode} from 'react';
import {Pressable, Text, StyleSheet, Dimensions} from 'react-native';
import {IconProduct} from '../../assets';
import {colors} from '../../utils';

const ButtonSmall = ({children, title, onPress}) => {
  return (
    <Pressable style={styles.container} onPress={onPress}>
      {children}
      <Text style={styles.title}>{title}</Text>
    </Pressable>
  );
};

export default ButtonSmall;

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.primary,
    width: width * 0.45,
    paddingVertical: 11,
    borderRadius: 6,
    paddingHorizontal: 9,
    marginBottom: 15,
  },
  title: {
    fontSize: 15,
    fontFamily: 'Poppins-Regular',
    color: '#fff',
    marginLeft: 8,
  },
});
