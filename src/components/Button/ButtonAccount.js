import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import React from 'react';
import {colors} from '../../utils';

export default function ButtonAccount({title = '', icon, onPress}) {
  return (
    <TouchableNativeFeedback useForeground onPress={onPress}>
      <View style={styles.view}>
        {icon}
        <Text style={styles.text} adjustsFontSizeToFit numberOfLines={1}>
          {title}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    color: 'white',
    marginBottom: -3,
    marginHorizontal: 10,
    marginRight: 25,
  },
  view: {
    overflow: 'hidden',
    backgroundColor: colors.primary,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    elevation: 3,
    width: Dimensions.get('screen').width / 2.5,
    maxWidth: 185,
    padding: 10,
    marginBottom: 15,
  },
});
