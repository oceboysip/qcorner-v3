import React from 'react';
import {View, Text, TouchableNativeFeedback, StyleSheet} from 'react-native';
import {colors} from '../../utils';

export default function ButtonAdd({
  onPress,
  title = '',
  showPlus = false,
  width = null,
  maxWidth = 480,
  alignSelf = 'center',
  disabled,
  replacePlus,
}) {
  return (
    <TouchableNativeFeedback
      useForeground
      onPress={onPress}
      disabled={disabled}>
      <View
        style={{
          ...styles.view,
          width: width,
          maxWidth: maxWidth,
          alignSelf: alignSelf,
          borderColor: disabled ? 'grey' : colors.primary,
        }}>
        {showPlus && (
          <Text
            style={{...styles.plus, color: disabled ? 'grey' : colors.primary}}>
            {replacePlus ? replacePlus : '+'}
          </Text>
        )}
        <Text
          style={{...styles.text, color: disabled ? 'grey' : colors.primary}}>
          {title}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  plus: {
    position: 'absolute',
    fontSize: 25,
    left: 10,
    textAlignVertical: 'center',
  },
  text: {
    fontFamily: 'Poppins-Medium',
    textAlign: 'center',
  },
  view: {
    borderWidth: 2,
    borderRadius: 5,
    paddingVertical: 5,
    paddingTop: 8,
    overflow: 'hidden',
    backgroundColor: 'white',
  },
});
