import React from 'react';
import {StyleSheet, Text, Pressable} from 'react-native';

const ButtonPlus = () => {
  return (
    <Pressable style={styles.container}>
      <Text style={styles.plus}>+</Text>
      <Text style={styles.text}>Tambah Bank</Text>
    </Pressable>
  );
};

export default ButtonPlus;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 19,
    paddingHorizontal: 20,
    position: 'relative',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#24B1A8',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 18,
    fontFamily: 'Poppins-Medium',
    color: '#24B1A8',
    textAlign: 'center',
  },
  plus: {
    fontSize: 30,
    fontFamily: 'Poppins-Medium',
    color: '#24B1A8',
    position: 'absolute',
    top: 11,
    left: 20,
  },
});
