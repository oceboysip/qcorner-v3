import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors} from '../../utils';

const CheckCircle = ({value}) => (
  <View style={styles.dotRadius}>
    {value && <View style={styles.dotActive} />}
  </View>
);

const styles = StyleSheet.create({
  dotRadius: {
    width: 20,
    height: 20,
    borderRadius: 20 / 2,
    borderWidth: 2.5,
    borderColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  dotActive: {
    width: 7,
    height: 7,
    borderRadius: 7 / 2,
    backgroundColor: colors.primary,
  },
});

export default CheckCircle;
