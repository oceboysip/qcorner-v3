import React from 'react';
import {View, Text, StyleSheet, Image, Platform, Pressable} from 'react-native';
import {
  IconAccount,
  IconAccountOff,
  IconHistory,
  IconHistoryOff,
  IconHome,
  IconHomeOff,
  IconNotif,
  IconNotifOff,
  IconSearchon,
  IconSearchOff
} from '../../assets';

const TabItem = ({onPress, onLongPress, isFocused, label}) => {
  const Icon = () => {
    switch (label) {
      case 'Home':
        return isFocused ? (
          <IconHome width={20} height={20} />
        ) : (
          <IconHomeOff width={20} height={20} />
        );

        case 'Search':
          return isFocused ? (
            <IconSearchon width={20} height={20} />
          ) : (
            <IconSearchOff width={20} height={20} />
          );
      case 'History':
        return isFocused ? (
          <IconHistory width={35} height={35} />
        ) : (
          <IconHistoryOff width={35} height={35} />
        );
      case 'Profile':
        return isFocused ? (
          <IconAccountOff width={20} height={20} />
        ) : (
          <IconAccount width={20} height={20} />
        );

     case 'Wadidaw':
          return isFocused ? (
            <IconAccountOff width={20} height={20} />
          ) : (
            <IconAccount width={20} height={20} />
          );
      default:
        return <View />;
    }
  };
  return (
    <Pressable
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <Icon />
      <Text style={[styles.text, {color: isFocused ? '#917369' : '#888888'}]}>
        {label}
      </Text>
    </Pressable>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.OS === 'ios' ? 20 : 8,
    flex: 1,
  },
  text: {
    fontSize: 10,
    fontFamily: 'Roboto-Regular',
    marginTop: 0,
    textAlign: 'center',
  },
  icon: {
    width: 35,
    height: 90,
  },
});
