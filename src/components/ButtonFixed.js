import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import Styled from 'styled-components';

import {
  Text,
} from '.';
import { colors as Color } from '../utils';

const CustomButton = Styled(TouchableOpacity)`
  backgroundColor: ${(props) => (props.disabled ? '#E7CE3E' : props.color)};
  flex: 1;
  minHeight: 45px;
  borderRadius: 4px;
  alignItems: center;
  justifyContent: center;
  flexDirection: row;
  borderWidth: 1px;
  borderColor: ${(props) => (props.disabled ? '#E7CE3E' : props.borderColor)};
`;

const CustomImage = Styled(Image)`
  width: 10;
  height: 10;
  marginRight: 3;
`;

const ButtonFixed = (props) => {
  const {
    fontColor,
    fontSize,
    color,
    borderColor,
    onPress,
    children,
    disabled,
    type,
    source,
    outline,
    ...style
  } = props;

  return (
    <CustomButton
      {...style}
      {...props}
      color={color ? color : outline ? Color.white1 : Color.primary}
      onPress={!disabled && onPress}
      borderColor={borderColor ? borderColor : outline ? Color.primary : Color.primary}
    >
      {source && <CustomImage source={source} />}
      <Text
        size={fontSize || 16}
        type={type || 'semibold'}
        color={
          disabled ? '#D2BD3F' : props.fontColor ? props.fontColor : outline ? Color.primary : Color.white1
        }>
        {children}
      </Text>
    </CustomButton>
  );
};

export default ButtonFixed;
