import React from 'react';
import {Pressable, StyleSheet, Text, View} from 'react-native';
import Gap from '../Gap';
import {IconEdit, IconRemove} from '../../assets';

const Item = ({text1, text2, text3}) => {
  return (
    <View>
      <Text style={styles.text}>{text1}</Text>
      <Text style={styles.text}>{text2}</Text>
      <Text style={styles.text}>{text3}</Text>
    </View>
  );
};

const CardAccountBank = ({text1, text2, text3}) => {
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <Item text1="Nama Bank" text2="Nomor Rekening" text3="Nama" />
        <Gap width={18} />
        <Item text1={text1} text2={text2} text3={text3} />
        <Gap width={30} />
      </View>
      <View style={styles.content}>
        <Pressable style={styles.item}>
          <IconRemove />
          <Text style={styles.itemText}>Hapus</Text>
        </Pressable>
        <Pressable style={styles.item}>
          <IconEdit />
          <Text style={styles.itemText}>Edit</Text>
        </Pressable>
      </View>
    </View>
  );
};

export default CardAccountBank;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#24B1A8',
    paddingVertical: 20,
    paddingHorizontal: 10,
    justifyContent: 'space-between',
  },
  text: {
    fontSize: 15,
    fontFamily: 'Poppins-Regular',
    color: '#000',
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    width: 70,
    height: 95,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderLeftWidth: 1,
    borderLeftColor: '#dcdcdc',
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemText: {
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    color: '#646464',
    marginTop: 3,
  },
});
