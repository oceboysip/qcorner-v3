import React, { Component } from 'react';
import { View, Text, TouchableOpacity as NativeTouchable, SafeAreaView } from 'react-native';
import Styled from 'styled-components';
import Foundation from 'react-native-vector-icons/Foundation';

// import Text from '../Text';

const ViewNotification = Styled(NativeTouchable)`
    minHeight: 50;
    width: 100%;
    flexDirection: row;
    backgroundColor: ${props => (props.error ? '#FF0000' : props.warning ? '#917369' : '#917369')};
    alignItems: center;
    justifyContent: flex-start;
    padding: 12px 16px 12px 16px;
`;

const IconView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    marginRight: 16;
`;

const DescriptionView = Styled(View)`
    flex: 1;
    alignItems: flex-start;
`;

const WhiteText = Styled(Text)`
    textAlign: left;
    color: #FFFFFF;
`;

const FoundationIcon = Styled(Foundation)`
    fontSize: 20;
    color: #FFFFFF;
`;

export default class ModalInformation extends Component {

  render() {
    return (
      <SafeAreaView>
        <ViewNotification {...this.props}>
            <IconView><FoundationIcon name='info' /></IconView>
            <DescriptionView>
              <WhiteText type='medium'>{this.props.label}</WhiteText>
            </DescriptionView>
        </ViewNotification>
      </SafeAreaView>
    );
  }
}
