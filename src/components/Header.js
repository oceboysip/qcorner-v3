import React, {memo} from 'react';
import {StyleSheet} from 'react-native';

import {Appbar, Text, Button} from 'react-native-paper';

const HeaderComponent = memo(function Header(props) {
  const {
    title,
    subtitle,
    navigation,
    buttonTextLeft,
    buttonLeftDisabled,
    onPressButtonLeft,
    buttonTextRight,
    iconRight,
    buttonRightDisabled,
    onPressButtonRight,
  } = props;
  return (
    <Appbar.Header style={styles.header}>
      {buttonTextLeft ? (
        <Button
          color="#FFFFFF"
          disabled={buttonLeftDisabled}
          onPress={
            onPressButtonLeft ? onPressButtonLeft : () => navigation.goBack()
          }
          labelStyle={{fontWeight: '700'}}>
          {buttonTextLeft}
        </Button>
      ) : (
        <Appbar.BackAction onPress={() => navigation.goBack()} />
      )}
      <Appbar.Content
        title={title}
        subtitle={subtitle}
        titleStyle={styles.headerText}
      />
      {buttonTextRight ? (
        <Button
          color="#FFFFFF"
          disabled={buttonRightDisabled}
          onPress={onPressButtonRight}
          labelStyle={{fontWeight: '700'}}>
          {buttonTextRight}
        </Button>
      ) : (
        <Appbar.Action icon={iconRight} onPress={onPressButtonRight} />
      )}
    </Appbar.Header>
  );
});

const styles = StyleSheet.create({
  header: {elevation: 0, backgroundColor: '#917369'},
  headerText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
});

export default HeaderComponent;
