import React, {useState, useEffect, memo} from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
//import Animated from 'react-native-reanimated';
import {Appbar} from 'react-native-paper';

import {isIphoneX} from '../utils/isIphoneX';
import GeneralStatusBar from './StatusBar';
import TabItemChat from './TabItemChat';

const TabBar = memo(props => {
  const {position, navigationState, navigation} = props;

  const [desiredWidth, setDesireWidth] = useState(
    Dimensions.get('window').width / navigationState.routes.length,
  );

  const renderTab = (route, index) => {
    return (
      <TabItemChat
        index={index}
        route={route}
        key={index}
        navigationState={navigationState}
        navigation={navigation}
        desiredWidth={desiredWidth}
      />
    );
  };
  const renderHeader = () => {
    return (
      <Appbar.Header style={styles.header}>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="" titleStyle={styles.headerText} />
        {/* <Appbar.Action icon="square-edit-outline" /> */}
      </Appbar.Header>
    );
  };

  useEffect(() => {
    Dimensions.addEventListener('change', e => {
      const {width} = e.window;
      setDesireWidth(width / navigationState.routes.length);
    });
  }, []);

  /** Indicator transition */
  // const indicatorTranslateX = Animated.interpolate(position, {
  //   inputRange: [0, 1],
  //   outputRange: [0, desiredWidth],
  // });

  // /** Indicator width */
  // const indicatorWidth = Animated.interpolate(position, {
  //   inputRange: [0, 1],
  //   outputRange: [desiredWidth, desiredWidth],
  // });
  return (
    <View style={[styles.container]}>
      <View style={styles.main}>
        <GeneralStatusBar backgroundColor="#917369" />
        {renderHeader()}
        <View style={styles.tab}>
          {navigationState.routes.map((route, index) =>
            renderTab(route, index),
          )}
        </View>
        {/* <Animated.View
          style={[
            styles.indicator,
            {
              width: indicatorWidth,
              left: indicatorTranslateX,
            },
          ]}
        /> */}
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#917369',
    position: 'absolute',
    zIndex: 1,
    top: 0,
    left: 0,
    right: 0,
    elevation: 3,
  },
  main: {flex: 1},
  header: {elevation: 0, backgroundColor: '#917369'},
  headerText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  tab: {
    flex: 1,
    backgroundColor: '#917369',
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: 24,
    height: 24,
    marginHorizontal: 12,
  },
  indicator: {
    height: 3,
    backgroundColor: 'white',
    elevation: 2,
    top: 0,
  },
});

export default TabBar;
