import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Appbar, TouchableRipple} from 'react-native-paper';

import ChatText from './chats/ChatText';

const fullWhite = 'rgba(255,255,255,1)';
const dullWhite = 'rgba(255,255,255,0.6)';

const TabItemChat = props => {
  console.log('wew',props)
  const {navigation, route, desiredWidth, navigationState, index} = props;
  const selectedColor = navigationState.index === index ? fullWhite : dullWhite;

  const onTabPress = () => {
    navigation.navigate(route.key);
  };

  return (
    <TouchableRipple
      key={index}
      style={[
        styles.tabBtn,
        {
          width: desiredWidth,
        },
      ]}
      onPress={onTabPress}
      rippleColor="rgba(255, 255, 255, .32)">
      <ChatText
        style={[
          styles.tabText,
          {
            color: selectedColor,
          },
        ]}
        value={route.key.toUpperCase()}
      />
    </TouchableRipple>
  );
};

const styles = StyleSheet.create({
  tabText: {
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  tabBtn: {height: 50, justifyContent: 'center', alignItems: 'center'},
  camera: {width: 48},
});

export default TabItemChat;
