import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const CardPromotion = ({title, desc}) => {
  return (
    <View style={styles.container}>
      <LinearGradient
        start={{x: 0.0, y: 0.5}}
        end={{x: 0.25, y: 1}}
        locations={[0, 0.6, 0.5]}
        colors={['#F6923B', '#F26D06']}
        style={styles.container}>
        <Text style={styles.discount}>{title}</Text>
      </LinearGradient>
      <Text style={styles.text}>{desc}</Text>
    </View>
  );
};

export default CardPromotion;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F26D06',
    flexDirection: 'row',
    alignItems: 'center',
    overflow: 'hidden',
  },
  discount: {
    fontSize: 64,
    paddingHorizontal: 6,
    paddingVertical: 30,
    color: '#fff',
  },
  text: {
    fontSize: 13,
    fontFamily: 'Roboto-Regular',
    color: '#fff',
    paddingRight: 150,
    lineHeight: 18,
  },
});
