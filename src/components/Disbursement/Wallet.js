import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {IconWallet} from '../../assets';
import {colors} from '../../utils';

function Wallet() {
  return (
    <View style={styles.viewSaldo}>
      <IconWallet height={15} width={15} />
      <Text style={styles.textWallet}>Wallet Grosir Juara</Text>
      <Text style={styles.textSaldo}>Rp 10.000.000</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  viewSaldo: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 15,
    margin: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 10,
  },
  textWallet: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    marginBottom: -2,
    marginHorizontal: 5,
  },
  textSaldo: {
    fontFamily: 'Poppins-Medium',
    textAlign: 'right',
    color: colors.primary,
    marginBottom: -3,
    fontSize: 17,
    flex: 1,
  },
});

export default Wallet;
