import React, {useState, useEffect} from 'react';
import database from '@react-native-firebase/database';
//import firebase from 'react-native-firebase';
import utf8 from 'utf8';
import base64 from 'base-64';

export default function useGetChats(userEmail = null, contactEmail = null) {
  const [dataMessages, setDataMessages] = useState(null);
  const [loading, setLoading] = useState(false);

  const fetchAllMessages = (current_email_encode, contact_email_encode) => {
    setLoading(true);
    database()
      .ref(`/messages/${current_email_encode}/${contact_email_encode}`)
      .on('value', snapshot => {
        // set here
        setDataMessages(snapshot.val());
        setLoading(false);
      });
  };
  useEffect(() => {
    if (userEmail !== null && contactEmail !== null) {
      // Convert to base64
      const current_email_bytes = utf8.encode(userEmail);
      const current_email_encode = base64.encode(current_email_bytes);
      const contact_email_bytes = utf8.encode(contactEmail);
      const contact_email_encode = base64.encode(contact_email_bytes);
      fetchAllMessages(current_email_encode, contact_email_encode);
    }
  }, [userEmail, contactEmail]);
  return {
    dataMessages,
    loading,
  };
}
