import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import {IconShelf} from '../../assets';

export default function Storefront({item, onPress}) {
  return (
    <TouchableNativeFeedback useForeground onPress={onPress}>
      <View style={styles.view}>
        <View style={styles.viewImg}>
          {/* <Image source={{uri: item.image}} /> */}
          <IconShelf />
        </View>
        <Text style={styles.text}>{item.title}</Text>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    marginHorizontal: 20,
    fontSize: 16,
  },
  view: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    paddingVertical: 10,
    borderColor: 'grey',
  },
  viewImg: {
    width: 65,
    height: 65,
    elevation: 3,
    backgroundColor: 'white',
    borderColor: 'grey',
    borderRadius: 5,
    borderWidth: StyleSheet.hairlineWidth,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
