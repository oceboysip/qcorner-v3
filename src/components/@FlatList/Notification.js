import {
  Image,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import React from 'react';
import {
  IconBucket,
  IconChevronUp,
  IconInfo,
  IconShop,
  ILOnePiece,
} from '../../assets';
import {colors} from '../../utils';
import {Gap, Line} from '..';

export default function Notification({item}) {
  return (
    <View style={{...styles.container, paddingTop: item.id == 1 ? 15 : 0}}>
      <View style={styles.viewType}>
        {item.type == 'Pembelian' && (
          <IconBucket width={25} height={25} fill={colors.primary} />
        )}
        {item.type == 'Informasi' && (
          <IconInfo width={25} height={25} fill={colors.primary} />
        )}
        {item.type == 'Penjualan' && (
          <IconShop width={25} height={25} fill={colors.primary} />
        )}
        <Gap width={10} />
        <Text style={{...styles.textLabel, flex: 1}}>{item.type}</Text>
        <Text style={styles.textLabel}>{item.created}</Text>
      </View>
      <Text style={styles.textTitle}>{item.title}</Text>
      {item.description && (
        <Text style={styles.textDesc}>{item.description}</Text>
      )}
      {item.type != 'Informasi' && (
        <TouchableNativeFeedback useForeground>
          <View style={styles.viewProduct}>
            <Image source={ILOnePiece} style={styles.imgProduct} />
            <Gap width={10} />
            <Text numberOfLines={1} style={styles.textProduct}>
              {item.product}
            </Text>
            <IconChevronUp
              style={{transform: [{rotate: '90deg'}]}}
              width={50}
              height={10}
            />
          </View>
        </TouchableNativeFeedback>
      )}
      <Line gap={15} />
    </View>
  );
}

const styles = StyleSheet.create({
  imgProduct: {
    width: 85,
    height: 40,
    borderRadius: 3,
    margin: 5,
  },
  textProduct: {
    fontFamily: 'Poppins-Medium',
    flex: 1,
  },
  viewProduct: {
    overflow: 'hidden',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    borderRadius: 3,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    elevation: 3,
  },
  textDesc: {
    fontFamily: 'Poppins-Regular',
    color: colors.text.label,
    marginBottom: -3,
  },
  textTitle: {
    fontFamily: 'Poppins-Medium',
    marginVertical: 10,
  },
  textLabel: {
    fontFamily: 'Poppins-Medium',
    color: colors.text.label,
    marginBottom: -3,
  },
  viewType: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    paddingHorizontal: 15,
    maxWidth: 480,
    alignSelf: 'center',
    width: '100%',
  },
});
