import CheckBox from '@react-native-community/checkbox';
import _ from 'lodash';
import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import {Gap} from '..';
import {IconShelf, ILOnePiece} from '../../assets';
import {colors} from '../../utils';

export default function StorefrontAddProduct({item, onPress}) {
  const idrFormat = price => _.replace(price, /\B(?=(\d{3})+(?!\d))/g, '.');
  return (
    <TouchableNativeFeedback useForeground onPress={onPress}>
      <View style={styles.view}>
        <CheckBox
          value={item.selected}
          onValueChange={onPress}
          tintColors={{true: colors.primary}}
        />
        <Gap width={10} />
        <View style={styles.viewImg}>
          <Image source={ILOnePiece} style={styles.imgProduct} />
        </View>
        <View style={{height: '100%'}}>
          <Text style={styles.text}>{item.name}</Text>
          <Text style={styles.textPrice}>Rp {idrFormat(item.price)}</Text>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  textPrice: {
    fontFamily: 'Poppins-Regular',
    marginHorizontal: 20,
    fontSize: 10,
  },
  text: {
    fontFamily: 'Poppins-Bold',
    marginHorizontal: 20,
  },
  view: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    paddingVertical: 10,
    borderColor: 'grey',
  },
  viewImg: {
    backgroundColor: 'white',
    elevation: 3,
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 5,
  },
  imgProduct: {
    width: 65,
    height: 65,
    borderRadius: 5,
  },
});
