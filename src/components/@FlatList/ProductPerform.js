import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {ILOnePiece} from '../../assets';
import {colors} from '../../utils';
import _ from 'lodash';

export default function ProductPerform({item, filter}) {
  const idrFormat = price => _.replace(price, /\B(?=(\d{3})+(?!\d))/g, '.');
  return (
    <View style={styles.container}>
      <View style={styles.viewImg}>
        <Image source={ILOnePiece} style={styles.img} />
      </View>
      <Text style={styles.textName} numberOfLines={2}>
        {item.name}
      </Text>
      <Text style={styles.textFilter} numberOfLines={2}>
        {filter == 1
          ? `Rp ${idrFormat(item.credit_total)}`
          : filter == 2
          ? item.sold
          : item.views}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  textFilter: {
    fontFamily: 'Poppins-Regular',
    width: 110,
    textAlign: 'right',
  },
  textName: {
    fontFamily: 'Poppins-Medium',
    flex: 1,
    marginHorizontal: 10,
  },
  container: {
    flexDirection: 'row',
    margin: 10,
  },
  viewImg: {
    borderRadius: 5,
    borderWidth: 2,
    borderColor: colors.primary,
    width: 70,
    height: 70,
    elevation: 3,
  },
  img: {
    width: 65,
    height: 65,
    borderRadius: 5,
  },
});
