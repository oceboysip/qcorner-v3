import CheckBox from '@react-native-community/checkbox';
import React from 'react';
import {View, Text, StyleSheet, TouchableNativeFeedback} from 'react-native';
import {IconPencil, IconTrash} from '../../assets';
import {Gap} from '../../components';
import {colors} from '../../utils';

function Addresses({item}) {
  return (
    <TouchableNativeFeedback key={item.id} useForeground>
      <View style={styles.viewAddress}>
        <View style={styles.viewAddressName}>
          <Text style={styles.textAddress}>Alamat {item.city}</Text>
          {item.primary && (
            <View style={styles.viewDefault}>
              <Text style={styles.textDefault}>Utama</Text>
            </View>
          )}
        </View>
        <View style={styles.viewDetail}>
          <View style={{width: '87.5%'}}>
            <Text style={styles.textDetail}>{item.detail}</Text>
          </View>
          <CheckBox value={item.selected} />
        </View>
        <View style={styles.viewOption}>
          <View style={{flex: 1}} />
          <TouchableNativeFeedback useForeground>
            <View style={styles.buttonOption}>
              <IconTrash />
              <Text style={styles.textOption}>Hapus</Text>
            </View>
          </TouchableNativeFeedback>
          <Gap width={5} />
          <TouchableNativeFeedback useForeground>
            <View style={styles.buttonOption}>
              <IconPencil />
              <Text style={styles.textOption}>Edit</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  textOption: {
    fontFamily: 'Poppins-Regular',
    fontSize: 10,
    marginBottom: -3,
    marginLeft: 5,
  },
  buttonOption: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 35,
    paddingHorizontal: 10,
    borderRadius: 3,
    overflow: 'hidden',
  },
  viewOption: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingTop: -10,
  },
  textDetail: {
    fontFamily: 'Poppins-Regular',
    color: colors.text.secondary,
  },
  viewDetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  textDefault: {
    color: colors.text.menuActive,
    fontFamily: 'Poppins-Regular',
    marginBottom: -3,
  },
  viewDefault: {
    backgroundColor: colors.primary,
    padding: 5,
    paddingHorizontal: 15,
    borderRadius: 5,
    elevation: 3,
  },
  textAddress: {
    fontFamily: 'Poppins-SemiBold',
    color: colors.text.secondary,
    fontSize: 15,
    marginBottom: -3,
  },
  viewAddressName: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
    marginHorizontal: 15,
    alignItems: 'center',
    height: 35,
  },
  viewAddress: {
    margin: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 5,
    backgroundColor: 'white',
    elevation: 3,
  },
  viewMenu: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    backgroundColor: colors.primary,
    alignItems: 'center',
    paddingBottom: 5,
  },
  textMenu: {
    fontFamily: 'Poppins-Regular',
    color: colors.text.menuActive,
    flex: 1,
    fontSize: 15,
  },
  buttonMenu: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
});

export default Addresses;
