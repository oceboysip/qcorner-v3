import _ from 'lodash';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableNativeFeedback,
} from 'react-native';
import {Gap} from '..';
import {
  IconAlertCircle,
  IconOption,
  IconOptionGrey,
  ILOnePiece,
} from '../../assets';
import {colors} from '../../utils';

const MyProduct = ({item, filter}) => {
  const idrFormat = price => _.replace(price, /\B(?=(\d{3})+(?!\d))/g, '.');
  return (
    <View style={styles.container}>
      <View style={styles.viewProduct}>
        <View style={styles.viewImg}>
          <Image source={ILOnePiece} style={styles.imgProduct} />
        </View>
        <Gap width={20} />
        <View style={{flex: 1}}>
          <Text style={styles.textProduct}>{item.name}</Text>
          <Text style={styles.text}>Rp {idrFormat(item.price)}</Text>
          <View style={styles.viewStatusProduct}>
            <Text style={styles.text}>Stok: {item.stock}</Text>
            <Gap width={10} />
            {item.status == 'active' && (
              <>
                <Text style={styles.textActive}>Aktif</Text>
                <Gap width={10} />
                <Text style={styles.textVariant}>Varian</Text>
              </>
            )}
            {item.status == 'inactive' && (
              <>
                <IconAlertCircle />
                <Gap width={10} />
                <Text style={styles.textInactive}>Nonaktif</Text>
              </>
            )}
          </View>
        </View>
      </View>
      <View style={styles.viewOptions}>
        <TouchableNativeFeedback useForeground onPress={() => null}>
          <View style={styles.buttonOption}>
            <Text style={styles.text}>Ubah Harga</Text>
          </View>
        </TouchableNativeFeedback>
        <Gap width={10} />
        <TouchableNativeFeedback useForeground onPress={() => null}>
          <View style={styles.buttonOption}>
            <Text style={styles.text}>Ubah Stok</Text>
          </View>
        </TouchableNativeFeedback>
        <View style={{flex: 1}} />
        <TouchableNativeFeedback useForeground onPress={() => null}>
          <View style={styles.buttonMenu}>
            <IconOptionGrey width={17} height={20} />
          </View>
        </TouchableNativeFeedback>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonOption: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: 'grey',
    overflow: 'hidden',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    width: '35%',
    height: 35,
    paddingTop: 3,
    elevation: 3,
  },
  buttonMenu: {
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  viewOptions: {
    flexDirection: 'row',
    padding: 20,
  },
  textVariant: {
    backgroundColor: 'grey',
    fontFamily: 'Poppins-Regular',
    color: 'white',
    paddingHorizontal: 10,
    borderRadius: 3,
    height: 22,
  },
  textInactive: {
    backgroundColor: '#80808054',
    fontFamily: 'Poppins-Regular',
    color: 'grey',
    paddingHorizontal: 10,
    borderRadius: 3,
    height: 22,
  },
  textActive: {
    backgroundColor: 'paleturquoise',
    fontFamily: 'Poppins-Regular',
    color: 'dodgerblue',
    paddingHorizontal: 10,
    borderRadius: 3,
    height: 22,
  },
  viewStatusProduct: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textProduct: {
    fontFamily: 'Poppins-Bold',
    marginBottom: -3,
  },
  text: {
    fontFamily: 'Poppins-Regular',
  },
  viewImg: {
    backgroundColor: 'white',
    elevation: 3,
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 5,
  },
  imgProduct: {
    width: 80,
    height: 80,
    borderRadius: 5,
  },
  viewProduct: {
    padding: 20,
    paddingBottom: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    marginBottom: 10,
    elevation: 1,
    backgroundColor: 'white',
  },
});

export default MyProduct;
