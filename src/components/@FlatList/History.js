import _ from 'lodash';
import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import {Gap, Line} from '..';
import {IconBucket, IconShop, ILOnePiece} from '../../assets';
import {colors} from '../../utils';

export default function History({item}) {
  const idrFormat = price => _.replace(price, /\B(?=(\d{3})+(?!\d))/g, '.');
  return (
    <View style={styles.container}>
      <View style={styles.viewStatus}>
        {item.type == 'Pembelian' ? <IconBucket /> : <IconShop />}
        <View style={{flex: 1, marginLeft: 10}}>
          <Text style={styles.textType}>{item.type}</Text>
          <Text style={styles.textDate}>{item.created}</Text>
        </View>
        <Text style={styles.textStatus}>{item.status}</Text>
      </View>
      <Line width="95%" align="center" />
      <View style={styles.viewProduct}>
        <View style={styles.viewImgProduct}>
          <Image source={ILOnePiece} style={styles.imgProduct} />
        </View>
        <Gap width={15} />
        <View style={{flex: 1}}>
          <Text numberOfLines={2} style={{fontFamily: 'Poppins-Bold'}}>
            {item.name}
          </Text>
          <Text style={{fontFamily: 'Poppins-Regular'}}>{item.qty} Barang</Text>
        </View>
      </View>
      <View style={styles.viewDetail}>
        <View style={{flex: 1}}>
          <Text style={styles.textTotalPayment}>Total Belanja</Text>
          <Text style={styles.textPayment}>
            Rp {idrFormat(item.total_payment)}
          </Text>
        </View>
        {item.status != 'Pesanan Selesai' && item.status != 'Selesai' && (
          <TouchableNativeFeedback useForeground>
            <View style={styles.buttonOption}>
              <Text style={styles.buttonText}>
                {item.status == 'Sampai Tujuan'
                  ? 'Konfirmasi Penerimaan'
                  : 'Pesanan di proses' && 'Lacak'}
              </Text>
            </View>
          </TouchableNativeFeedback>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonText: {
    fontFamily: 'Poppins-Regular',
    color: 'white',
    marginBottom: -3,
  },
  buttonOption: {
    backgroundColor: colors.primary,
    paddingHorizontal: 15,
    height: 32.5,
    justifyContent: 'center',
    borderRadius: 3,
    elevation: 3,
    overflow: 'hidden',
  },
  viewDetail: {
    flexDirection: 'row',
    marginHorizontal: 15,
    marginBottom: 10,
    alignItems: 'center',
  },
  textPayment: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    marginBottom: -3,
  },
  textTotalPayment: {
    fontFamily: 'Poppins-Medium',
    color: colors.text.label,
    fontSize: 10,
    marginBottom: -3,
  },
  viewImgProduct: {
    width: 90,
    height: 90,
    elevation: 3,
    backgroundColor: 'white',
    borderColor: colors.primary,
    borderWidth: 1,
    borderRadius: 3,
  },
  imgProduct: {
    width: 88,
    height: 88,
    borderRadius: 3,
  },
  viewProduct: {
    flexDirection: 'row',
    margin: 15,
  },
  textStatus: {
    backgroundColor: '#8EBE8D',
    fontFamily: 'Poppins-Medium',
    height: 21,
    color: 'white',
    paddingHorizontal: 10,
    borderRadius: 3,
    fontSize: 12,
  },
  textDate: {
    fontFamily: 'Poppins-Regular',
    color: colors.text.label,
    fontSize: 10,
    marginBottom: -3,
  },
  textType: {
    fontFamily: 'Poppins-Medium',
    color: colors.text.label,
    marginBottom: -5,
  },
  viewStatus: {
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 7,
    alignItems: 'center',
  },
  container: {
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
    marginBottom: 10,
    backgroundColor: 'white',
    maxWidth: 480,
    alignSelf: 'center',
    width: '100%',
  },
});
