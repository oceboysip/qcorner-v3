const initLogin = {
  email: '',
  password: '',
};

const initRegister = {
  shopname: '',
  firstname: '',
  lastname: '',
  email: '',
  phone: '',
  address: '',
};

const SessionLogin = {
  token: '',
  name: '',
  email: '',
  pin:'',
  roles:'',
  role:'',
};
const SessionPermohonan = {
  list_data_aju:'',

}

export const reducerSession = (state = SessionLogin, action) => {
  if (action.type === 'SET_USER') {
    return {
      ...state,
      token: action.value.token,
      name: action.value.name,
      email: action.value.email,
      roles: action.value.roles,
      role: action.value.role,
    };
  }  
  if (action.type === 'UPDATE_PIN') {
    return {
      ...state,
      pin: action.value.pin,
    };
  }
  if (action.type === 'UPDATE_PHONE') {
    return {
      ...state,
      pin: action.value.pin,
    };
  }
  if (action.type === 'UPDATE_IP') {
    return {
      ...state,
      ip: action.value.ip,
    };
  }
  if (action.type === 'USER.CLEAR_AUTH') {
    return SessionLogin;
  }
  
  return state;
};

export const reducerPermohonan = (state = SessionPermohonan, action) => {
  return {
    ...state,
    list_data_aju:action.value.data.data,
  }
 };


export const reducerLogin = (state = initLogin, action) => {
  if (action.type === 'SET_LOGIN') {
    return {
      ...state,
      email: action.value.email,
      password: action.value.password,
    };
  }
  if (action.type === 'SET_RESET_LOGIN') {
    return {
      ...state,
      email: action.value.email,
      password: action.value.password,
    };
  }
  return state;
};

export const reducerSignUp = (state = initRegister, action) => {
  if (action.type === 'SET_REGISTER') {
    return {
      ...state,
      shopname: action.value.shopname,
      firstname: action.value.firstname,
      lastname: action.value.lastname,
      email: action.value.email,
      phone: action.value.phone,
      address: action.value.address,
    };
  }
  if (action.type === 'SET_RESET_REGISTER') {
    return {
      ...state,
      shopname: action.value.shopname,
      firstname: action.value.firstname,
      lastname: action.value.lastname,
      email: action.value.email,
      phone: action.value.phone,
      address: action.value.address,
    };
  }
  return state;
};
