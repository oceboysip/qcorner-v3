import { persistCombineReducers } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import {
    reducerLogin,
    reducerSignUp,
    reducerSession,
} from './auth';

import { reducerForgot } from './forgot';
import { loading } from './loading';

const config = {
    key: 'root',
    storage: AsyncStorage,
};

const reducer = persistCombineReducers(config, {
    reducerLogin,
    reducerSignUp,
    reducerSession,
    loading,
    reducerForgot,
});

export default reducer;