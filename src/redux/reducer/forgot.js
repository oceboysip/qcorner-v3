const initpassword = {
  otp_number: '',
  ip: '',
  new_password: '',
  confirm_password: '',
  phone: '',
};

export const reducerForgot = (state = initpassword, action) => {
  if (action.type === 'SET_FORGOT') {
    return {
      ...state,
      phone: action.value.phone,
      ip: action.value.ip,
    };
  }
  if (action.type === 'SET_OTP_NUMBER') {
    return {
      ...state,
      otp_number: action.value.otp_number,
    };
  }
  if (action.type === 'SET_PASSWORDNEW') {
    return {
      ...state,
      otp_number: action.value.otp_number,
      ip: action.value.ip,
      new_password: action.value.new_password,
      confirm_password: action.value.confirm_password,
      phone: action.value.phone,
    };
  }
  return state;
};
