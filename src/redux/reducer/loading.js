const initState = {
  loading: false,
};

export const loading = (state = initState, action) => {
  if (action.type === 'SET_LOADING') {
    return {
      ...state,
      loading: action.value,
    };
  }
  return state;
};
