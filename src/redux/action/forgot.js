import { navigationRef } from '../../App';
import {showMessage} from '../../utils';
import { postAPI, postHttpBasic } from '../../utils/httpService';
import store from '../store';

export const forgotPassAction = (params) => (dispatch) => {
    store.dispatch({type: 'SET_LOADING', value: true});
    let body = {
        phone: params.phone
    };
    console.log(body);

    postHttpBasic('forgot_password/request_by_phone', body).then((res) => {
      console.log('res', res);

      if (res.status && res.content) {
        navigationRef.current.navigate('OTP', { phone: params.phone, ip: res.content.ipaddress });
      } else {
        showMessage({
          message: res.message,
        });
      }

      store.dispatch({type: 'SET_LOADING', value: false});
    })
    .catch((err) => {
      console.log('err', err);
      store.dispatch({type: 'SET_LOADING', value: false});
      showMessage({
        message: err.message,
      });
    });
};
