import { navigationRef } from '../../App';
import {showMessage} from '../../utils';
import { baseURL, postLOGIN, postHttpBasic } from '../../utils/httpService';
import store from '../store';
import AsyncStorage from '@react-native-community/async-storage';
import utf8 from 'utf8';
import base64 from 'base-64';
import _ from 'lodash';
import database from '@react-native-firebase/database';

const SegmentOperator=(params,dispatch)=>{
  console.log('masuk sini2')
  let body = {
    username: params.email,
    password: params.password,
    device_token: params.fcm_token,
  };
  console.log(body)
  postLOGIN('/auth/operator/login',body).then(response => {
    var status = response.status;
    if (status === true) {
      var mysess = response.data;
      AsyncStorage.setItem('mySess', JSON.stringify(mysess));
      AsyncStorage.setItem('myToken', response.data.token); 
      AsyncStorage.setItem('myName', response.data.user_data.name);
      AsyncStorage.setItem('myEmail', response.data.user_data.email);
      const bytesEmail = utf8.encode(response.data.user_data.email);
      const encodedEmail = base64.encode(bytesEmail);
      const reference = database().ref('users/' + encodedEmail);
      reference.once('value').then(users => {
        if (_.isNil(users.val()) || users.val().email !== response.data.data.user_data.email) {
          reference.set({
            name: response.data.user_data.name,
            email: response.data.user_data.email,
            logInTime: firebase.database.ServerValue.TIMESTAMP,
          });
        } else {
          reference.update({
            name: response.data.user_data.name,
            email: response.data.user_data.email,
            logInTime: firebase.database.ServerValue.TIMESTAMP,
          });
        }
      });
      let token =   response.data.token;
      if(token){
        dispatch({type: 'SET_LOADING', value: false});
        response.data.user_data.token=token;
        dispatch({type: 'SET_USER', value: response.data.user_data});

      

        return token;
      }
    } 
  }).catch((err)=>{
    showMessage({
      message: 'Periksa Kembali Jaringan Anda.' + err.message,
    });
  })
}

export const signInAction = (params) => (dispatch) => {

  
 

  
  dispatch({type: 'SET_LOADING', value: true});
  let body = {
    username: params.email,
    password: params.password,
    device_token: params.fcm_token,
  };

 postLOGIN('/auth/login',body).then(response => {
    var status = response.status;
    if (status === true) {
      var mysess = response.data;
      AsyncStorage.setItem('mySess', JSON.stringify(mysess));
      AsyncStorage.setItem('myToken', response.data.token); 
      AsyncStorage.setItem('myName', response.data.user_data.name);
      AsyncStorage.setItem('myEmail', response.data.user_data.email);
      const bytesEmail = utf8.encode(response.data.user_data.email);
      const encodedEmail = base64.encode(bytesEmail);
      const reference = database().ref('users/' + encodedEmail);
      reference.once('value').then(users => {
        if (_.isNil(users.val()) || users.val().email !== response.data.data.user_data.email) {
          reference.set({
            name: response.data.user_data.name,
            email: response.data.user_data.email,
            logInTime: firebase.database.ServerValue.TIMESTAMP,
          });
        } else {
          reference.update({
            name: response.data.user_data.name,
            email: response.data.user_data.email,
            logInTime: firebase.database.ServerValue.TIMESTAMP,
          });
        }
      });

      let token =   response.data.token;
      if(token){
        dispatch({type: 'SET_LOADING', value: false});
        response.data.user_data.token=token;
        dispatch({type: 'SET_USER', value: response.data.user_data});
      }
    } 
  }).catch((err) => {
    dispatch({type: 'SET_LOADING', value: false});
    SegmentOperator(params,dispatch)
    
  });



};


