import { navigationRef } from '../../App';
import {showMessage} from '../../utils';
import { baseURL, postLOGIN, postHttpBasic, getAPI } from '../../utils/httpService';
import store from '../store';
import AsyncStorage from '@react-native-community/async-storage';
import utf8 from 'utf8';
import base64 from 'base-64';

export const GetListAction = (params,async) => (dispatch) => {
  dispatch({type: 'SET_LOADING', value: true});
  getAPI('applicant/inquiry-list').then(response => {
    dispatch({type: 'SET_LOADING', value: false});
  }).catch((err) => {
    console.log('err', err);
    dispatch({type: 'SET_LOADING', value: false});
    showMessage({
      message: 'Periksa Kembali Jaringan Anda.' + err.message,
    });
  });
};
