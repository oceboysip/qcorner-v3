import {showMessage} from '../../utils';
import {postAPI} from '../../utils/httpService';

export const updateProfile = (profile, navigation) => dispatch => {
  dispatch({type: 'SET_LOADING', value: true});
  postAPI('update_profile', profile)
    .then(res => {
      dispatch({type: 'SET_LOADING', value: false});
      if (!res.status) {
        showMessage({
          message: res.message,
        });
      } else {
        dispatch({type: 'UPDATE_PROFILE', value: res.content});
        navigation.push('MainApp', {screen: 'Home'});
      }
    })
    .catch(err => {
      dispatch({type: 'SET_LOADING', value: false});
      showMessage({
        message: err.message,
      });
    });
};
