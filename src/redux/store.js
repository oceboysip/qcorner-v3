import { createStore, applyMiddleware } from 'redux';
import { persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

import reducer from './reducer';

export let store = createStore(
    reducer,
    applyMiddleware(thunk)
);

export let persistor = persistStore(store);

export default store;