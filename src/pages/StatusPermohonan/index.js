import {CommonActions} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {ImageBackground, StyleSheet, View, Text,ScrollView,RefreshControl,TextInput,TouchableOpacity,Image,FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {background, cari,bgadacewek} from '../../assets';
import {Header} from '../../components';
import {GetListAction} from '../../redux/action/permohonan';
import axios from 'axios';



const StatusPermohonan = ({navigation, route}) => {
  const [no_aju,setNoAju] = useState([]);
  const [data,setData] = useState([]);
  const {reducerSession} = useSelector(state => state);
  const {reducerPermohonan} = useSelector(state => state);
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch();
  const [jenispermohonan, setJenisPermohonan] = useState([]);
  useEffect(() => {
    // console.log('ini session');
    // console.log('reducer:', reducerSession);
    // console.log('permohonan:',reducerPermohonan);
    cariDetailAju();
    listData();
    LoadMaster();
    // RenderDetailData();
  }, []);
  const LoadMaster = () => {
    setJenisPermohonan(dispatch(GetListAction()));

  };

  const redirectTo = (name, params) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name, params}],
      }),
    );
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      cariDetailAju();
      listData();
      LoadMaster();
      setRefreshing(false);
    }, 1000);
  };

   const listData = (no_aju) => {
    const token = reducerSession.token;
    console.log('tokenss:',token)
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    //console.log("tokennya:"+token)
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/applicant/inquiry-list-new',
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      // setData(+JSON.stringify(response.data);
      // console.log("==============DATA_LIST=============")

      // console.log("the size => "+JSON.stringify(response.data.data));
      const status = response.data.code
      const data = response.data.data
      // const no_aju = response.data.data.aju_number;
      // console.log('ajuin:',no_aju)
      const ajuaju = data.filter(item => item.aju_number === setNoAju) 
      console.log('statuses:',status)
      if (status == 200){
       setData(response.data.data);
       setNoAju(response.data.data.aju_number)
      } else {
        // alert('no')
      }

    })
    .catch(function (error) {
      console.log('yah error token keqnya',+error);
    });
    
  }

  cariData  =  () => {
    const aju = no_aju
    const token = reducerSession.token;
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }

    axios.post('http://apiv2.bbkpsoetta.com/applicant/inquiry/search', 
    {
      search: aju,
    },{
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' +token
      },
    })
    .then( response => {

    

      var status = response.data.code
      if (status == 200){
        setData(response.data.data)
      } else {
        // alert('no')
      }
    })
    .catch(error => {
      alert('error')
      console.log('error : ',+error);
    });
  }


  const cariDetailAju =  () => {
    const token = reducerSession.token
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/applicant/inquiry/'+no_aju,
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      // console.log("==============DATA_AJU_DETAIL=============")
      const data = response.data
      window.dataAjuDetail = data
    })
    .catch(error => {
      console.log('error : ',+error);
      window.dataAjuDetail = false
    });

    return window.dataAjuDetail
}

const detailAju = (no_aju,status,quarantine_status) => {
  // alert(status)
  const  dataAju = cariDetailAju.no_aju;
  const token = reducerSession.token
  if (dataAju == false){
    alert('Mohon Menunggu, Request sedang kami proses ..  ')
    return
  }
  
  if (status == 'Draft Sertifikat'){
    
      navigation.navigate('PengajuanVerifikasi',{no_aju: no_aju})
 
  
  }else if(status == 'Pending'){
    navigation.navigate('PengajuanPerbaikan',{no_aju: no_aju,data:dataAju})
  
  }else if(status == 'Proses Verifikasi'){
    navigation.navigate('ProsesVerifikasi',{no_aju: no_aju,data:dataAju})
  
  }else if(status == 'persetujuan'){
    alert('Status Sudah Di setujui !!')
  
  }else if(status == 'Proses Penerbitan Biling'){  
    navigation.navigate('PenerbitanBilling',{no_aju: no_aju,data:dataAju,token:token})
  
  }else if(status == 'Draft Permohonan Perbaikan'){  
    navigation.navigate('DraftPermohonanPerbaikan',{no_aju: no_aju,data:dataAju,token:token})
  
  }else if(status === 'Penerbitan Biling'){
    navigation.navigate('PengajuanPembayaran',{no_aju: no_aju,data:dataAju,token:token})
  
  }else if(status == 'Penerbitan Sertifikat' ){
    navigation.navigate('Esertifikat',{no_aju: no_aju,data:dataAju})      
  
  }else if(status == 'serah_terima'){  
    navigation.navigate('Esertifikat',{no_aju: no_aju,data:dataAju})            
  
  }else if(status == 'Ditolak'){
    navigation.navigate('PengajuanPpkDitolak',{no_aju: no_aju,data:dataAju})
  
  }else if(status == 'Menunggu verifikasi'){
    navigation.navigate('PengajuanSetelahSubmit',{no_aju: no_aju,data:dataAju})
  
  }else if(status == 'Pembayaran Diterima'){
    navigation.navigate('PembayaranDiterima',{no_aju: no_aju,data:dataAju})
  
  }else if(status == 'Tindak Lanjut Hasil Lab'){
    navigation.navigate('TindakLanjutHasilLab',{no_aju: no_aju,data:dataAju})
  }else if(status == 'Diterima'){
    navigation.navigate('DiTerima',{no_aju: no_aju,data:dataAju})
  }else{
    // if (quarantine_status == 'Tindak Lanjut Hasil Lab'){
    //   this.props.navigation.navigate('TindakLanjutHasilLab',{no_aju: no_aju,data:dataAju})
    // } else {
  
      alert('Status Masih Sedang dalam Prosess !!')
    // }
  }
}

   const _RenderDetailData = (no_aju,status,namaperusahaan,status_detail,id,quarantine_status,destinasi) => {
    if(status != 'Tutup (Selesai)'){
      var hit = id % 2
      if(hit != 0){
        var warna = '#EBE5DF'
      }else{
        var warna = '#ffffff'
      }

      if (quarantine_status == 'Tindak Lanjut Hasil Lab'){
        status_detail = 'Tindak Lanjut Hasil Lab'
      }
      
      return (
        // <View style={{flex:1,backgroundColor:'#EBE5DF'}}>
        <View style={{flex:1,backgroundColor:warna}}>
          <View style={{paddingHorizontal:10,paddingVertical:10,flex:1,flexDirection:'row',borderColor:'#ffff',borderBottomWidth:2}}>
              <View style={{flex:1.6,backgroundColor:''}}>
                <Text style={{fontSize:12}}>{no_aju}</Text>
              </View>
              <View style={{flex:1.6,backgroundColor:'',padding:5}}>
                <Text style={{fontSize:12}}>{destinasi && ContentSnippet(destinasi)}</Text>
              </View>
              <View style={{flex:1.4,backgroundColor:'',padding:5}}>
                <TouchableOpacity onPress={() => detailAju(no_aju,status_detail,quarantine_status)}>
                  <Text style={{fontSize:12}}>{status_detail}</Text>   
                </TouchableOpacity>                 
              </View>
            </View>
        </View>
      );
    }
  }
const ContentSnippet = (content, count) => {
  return content.substring(0,50).split(/\s+/).slice(0, 30).join(" ")+" \n\n";
}
  return (
    <ImageBackground
      source={background}
      style={{width: '100%', height: '100%'}}>
        <Header onPress={() => navigation.pop()}  />
      <View style={{flex: 1}}>
        <Header onPress={() => navigation.pop()} title={'Status Permohonan'} />
        <ImageBackground source={bgadacewek} style={{width: '100%', height: '100%', position: 'absolute'}}>
      
<View >
<ScrollView 
             refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
      <View style={{flex:1,flexDirection:'row'}}>
        <View style={{flex:1,alignItems:'flex-start',marginLeft:30,marginTop:50,backgroundColor:''}}> 
          <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>List</Text>
          <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Permohonan</Text>
        </View>
      </View>
     
      {/* START SEARCH */}
      <View style={{flex:1,marginHorizontal:30,marginTop:10}}>
      

        {/* <View style={{backgroundColor:'white',paddingLeft:10,borderRadius:25,borderColor:"grey",borderWidth:1, flexDirection: 'row'}}>          
          <View style={{flex:5}}>
            <TextInput
                placeholder="Masukan No Aju"
                style={{ height: 40, borderColor: '#FFFFFF'}}
                onChangeText={ (noaju) => setData({ noaju }) }
                // value={this.state.password}
              />
          </View>
          <View style={{flex:1,marginRight:10}}>
            <TouchableOpacity onPress={() => cariData()} style={{alignItems:'flex-end',paddingTop:10}}>
              <Image
                source={cari}
                style={{ height: 20, width: 20 }}
                resizeMode={'contain'}
              />
            </TouchableOpacity>
          </View>
        </View> */}
      </View>
      {/* END SEARCH */}

    
    <View style={{flex:1,marginHorizontal:30,marginTop:40}}>
      <View style={{flex:1,backgroundColor:'#917369',borderTopLeftRadius:20,borderTopRightRadius:20,borderColor:"grey",borderWidth:1}}>          
        <View style={{paddingVertical:10,flex:1,flexDirection:'row'}}>
          <View style={{flex:1.5,alignItems:'flex-start',paddingLeft:8}}>
            
            <Text style={{color:'white',fontSize:13}}>No.Aju</Text>
          </View>
          <View style={{flex:1.5,alignItems:'flex-start',paddingLeft:8}}>
            <Text style={{color:'white',fontSize:13}}>Destinasi</Text> 
          </View>
          <View style={{flex:1.5,alignItems:'flex-start',paddingLeft:8}}>

           <Text style={{color:'white',fontSize:13}}>Status</Text>                    
          </View>
        </View>
      </View>
      {/* <FlatList 
        data={data}
        renderItem={({item})=>{
          <View>
            <Text>ssss</Text>
            </View>
        }}/> */}
      <FlatList
        style={{marginBottom:30}}
        data={data}
        renderItem=
        {
          ({item,index}) => 
                _RenderDetailData(item.aju_number,item.status,item.nama_perusahaan,item.inquery_status,index,item.quarantine_status,item.port_destination)
        }
      />
      </View>
    
  </ScrollView>
  {/* <FAB
    style={styless.fab}
    icon="refresh"
    onPress={() => this.refresh()}
  /> */}
   

</View>
</ImageBackground>
        {/* <View style={{flex:1,backgroundColor:'fff'}}>
        <View style={{paddingHorizontal:10,paddingVertical:10,flex:1,flexDirection:'row',borderColor:'#ffff',borderBottomWidth:2}}>
            <View style={{flex:1.6,backgroundColor:''}}>
              <Text style={{fontSize:12}}>{no_aju}</Text>
            </View>
            <View style={{flex:1.6,backgroundColor:'',padding:5}}>
              <Text style={{fontSize:12}}>{destinasi && ContentSnippet(destinasi)}</Text>
            </View>
            <View style={{flex:1.4,backgroundColor:'',padding:5}}>
              <TouchableOpacity onPress={() => detailAju(no_aju,status_detail,quarantine_status)}>
                <Text style={{fontSize:12}}>{status_detail}</Text>   
              </TouchableOpacity>                 
            </View>
          </View>
      </View> */}
      </View>
    </ImageBackground>
  )



};

export default StatusPermohonan;

const styles = StyleSheet.create({});
