import { StyleSheet, Text, View ,ImageBackground,ScrollView} from 'react-native'
import React, {useState,useEffect} from 'react'
import { background, dua, pengajuansertifikat } from '../../assets'
import { Header } from '../../components'
import FitImage from 'react-native-fit-image';
import {useDispatch, useSelector} from 'react-redux';
import axios from 'axios';

const PengajuanPerbaikan = ({navigation,route,params}) => {
    const [uri_path,setUriPath]=useState(false);
    const [keterangan,setKeterangan]=useState("");
    const {no_aju} = route.params;
    const {data} =route.params;
    const {reducerSession} = useSelector(state => state);



    useEffect(() => {
      _getData();
      cariDetailAju();
      console.log('noajuhhhhhh:',no_aju)
      console.log('inidataajuh:',data)
      }, []);


      const _getData = async () => {
        var dataState = await cariDetailAju(no_aju);
        console.log('datasea:',dataState)
        var data = JSON.stringify(dataState)
        console.log("dataku_str: "+data)
        var data_parse = JSON.parse(data)
        var dataku = data_parse.data.current_requirement
        var data_doc = JSON.stringify(dataku)
        console.log("dataku_current: "+data_doc)
    
        if(data_doc != "null"){
          var data_doc_2 = JSON.parse(data_doc)
          var data_doc_uri = data_doc_2.file_path
          console.log("dataku_oy: "+data_doc_2)
          if(data_doc_uri){
           setUriPath(data_doc_uri)
          }
        }else{
          alert("Dokument tidak ditemukan silahkan hubungi administrator")
        }
        
      }
      const cariDetailAju = async () => {
        const token = reducerSession.token;
        console.log('tokenss:',token)
        if (token == false){
          alert(constant.pesan_relog)
          navigation.navigate('Login')
        }
        //console.log("tokennya:"+token)
        axios({
          method: 'get',
          url: 'http://apiv2.bbkpsoetta.com/applicant/inquiry-list-new',
          headers: {
            'Authorization': 'Bearer ' +token
          }
        })
        .then(response => {
          // setData(+JSON.stringify(response.data);
          // console.log("==============DATA_LIST=============")
    
          // console.log("the size => "+JSON.stringify(response.data.data));
          const status = response.data.code
          const data = response.data.data
          console.log('inidata:',data)
          const no_aju = data.filter(item => item.aju_number === no_aju) 
          console.log('noajuh:',no_aju)
          console.log('statuses:',status)
          if (status == 200){
           setData(response.data.data);
          } else {
            // alert('no')
          }
    
        })
        .catch(function (error) {
          console.log('yah error token keqnya',+error);
        });
        
      }

  return (
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
    <Header onPress={() => navigation.pop()} title={'Pengajuan Verifikasi'} />
    <View style={{ flex: 1}}>
    {/* <HeaderLengkap is_GoTo_hidden={true} is_textHeader_hidden={true}/> */}
      <ScrollView>
        <View style={{flex:1,backgroundColor:'',marginBottom:20}}>
          <FitImage
            resizeMode="cover"
            originalWidth={400}
            originalHeight={50}
            source={dua}
            />
        </View>
        <View style={{flex:1,alignItems:'center',marginTop:0}}>
          <Text style={{fontSize:30,color:'#917369',fontWeight:'bold'}}>Verifikasi Draft </Text>
          <Text style={{fontSize:30,color:'#917369',fontWeight:'bold'}}>Permohonan</Text>
          <Text style={{fontSize:15,marginTop:10}}>
            Mohon periksa kembali Draft Sertifikat 
          </Text>
          <Text style={{fontSize:15}}>
            dibawah ini, pastikan dokumen - dokumen Anda
          </Text>
          <Text style={{fontSize:15}}>
            sudah benar sebelum melanjutkan 
          </Text>
          <Text style={{fontSize:15}}>
            ke proses berikutnya 
          </Text>
        </View> 
        <View style={{flex:3,marginHorizontal:30,marginTop:20}}>
          {uri_path &&
          <View>
            <TouchableHighlight onPress={() => navigation.navigate('PDF',{uri:uri_path,link_back:'PengajuanVerifikasi',text_header:'Dokumen'})} >
              <View style={styles.shadowView}>          
                <Image
                  style={{height:50,width:300}}
                  source={pengajuansertifikat}
                />
              </View>
            </TouchableHighlight>
              <View style={{flex:1,flexDirection:'row',marginTop:20}}>
                <View style={{flex:1,backgroundColor:'',paddingHorizontal:10}}>
                  <TouchableOpacity onPress={() => this.download("Draft Sertifikat",this.GetUrl(this.state.uri_path.toString()))}>
                    <View style={{backgroundColor:'#917369',width:"100%",height:40,
                                  alignContent:'center',justifyContent:'center',alignItems:'center',
                                  borderRadius:25,borderColor:"grey",borderWidth:1}}>          
                        <View><Text style={{color:'white',fontSize:12}}>Download</Text></View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1,backgroundColor:'',paddingHorizontal:10}}>
                  <TouchableOpacity onPress={this.clickSetuju}>
                    <View style={{backgroundColor:'#917369',width:"100%",height:40,
                                  alignContent:'center',justifyContent:'center',alignItems:'center',
                                  borderRadius:25,borderColor:"grey",borderWidth:1}}>          
                        <View><Text style={{color:'white',fontSize:12}}>Setuju</Text></View>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{flex:1,backgroundColor:'',paddingHorizontal:10}}>
                  <TouchableOpacity onPress={this.clickModal}>
                    <View style={{backgroundColor:'white',width:"100%",height:40,
                                  alignContent:'center',justifyContent:'center',alignItems:'center',
                                  borderRadius:25,borderColor:"grey",borderWidth:1}}>          
                        <View><Text style={{color:'#917369',fontSize:12}}> Perbaikan</Text></View>
                    </View>
                  </TouchableOpacity>

                  {/* modal */}
                  <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    // visible={true}
                    >
                    <View style={{flex:1}}>
                      <View style={{flex:3}}></View>
                      <View style={{backgroundColor:'white',
                                    height:150,
                                    flex:1,paddingHorizontal:10}}>
                        {/* content */}
                        <ScrollView>
                        <View style={{flex:1,marginVertical:10}}>
                          <Text>Silahkan masukkan keterangan perbaikan</Text>
                        </View>
                        <View style={{flex:2}}>
                          <View style={{backgroundColor:'white',paddingLeft:10,marginBottom:10,
                                            borderRadius:25,borderColor:"grey",borderWidth:1}}>

                            <TextInput
                              style={{ height: 40,borderColor: "#FFFFFF"}}
                              onChangeText={(keterangan) => this.setState({keterangan: keterangan})}
                          
                            />
                          </View>
                        </View>
                        <View style={{flex:2}}>
                          <View style={{flex:1,backgroundColor:'',paddingHorizontal:10}}>
                              <TouchableOpacity onPress={this.clickPerbaiki}>
                                <View style={{backgroundColor:'#917369',width:"100%",height:40,
                                              alignContent:'center',justifyContent:'center',
                                              alignItems:'center',
                                              borderRadius:25,borderColor:"grey",borderWidth:1}}>          
                                  <View><Text style={{color:'white',fontSize:20}}>Perbaiki</Text></View>
                              </View>
                            </TouchableOpacity>
                          </View>
                        </View>
                        <View style={{flex:1,alignItems:'center'}}>
                          <TouchableHighlight
                            onPress={() => {
                              this.setModalVisible(!this.state.modalVisible);
                            }}>
                            <Text>Tutup Modal</Text>
                          </TouchableHighlight>
                        </View>
                        </ScrollView>

                        {/* end content */}
                        
                      </View>
                    </View>
                  </Modal>

                  {/* end modal */}

                </View>
                
              </View>
              { this.state.loaded &&
                <View style={{flex:1,flexDirection:'row',marginTop:20,alignSelf:'center'}}>
                  <ActivityIndicator
                          size="large"
                          color="#917369"
                          style={{marginRight:10}}
                        />
                    <View><Text style={{color:'#917369',fontSize:20}}>Harap Tunggu ...</Text></View>
                </View>
              }
              
          </View>
          }

        </View>
      </ScrollView>

    </View>
    </ImageBackground>
  )
}

export default PengajuanPerbaikan

const styles = StyleSheet.create({})