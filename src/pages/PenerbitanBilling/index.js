import { StyleSheet, Text, View ,ImageBackground,ScrollView} from 'react-native'
import React,{useState} from 'react'
import { background, dua } from '../../assets'
import FitImage from 'react-native-fit-image';
import { Header } from '../../components';

export default function PenerbitanBilling({route}) {
    const {noaju}=route.params
    const [load_aju_error,setLoadAjuError]=useState(false);
    const [uri_path,setUriPath]=useState(false);

    const CariAju = () => {
        setLoadAjuError(true);
        
        // this.props.navigation.navigate('ResetPassword')    
      }
    
      const GetUrl=(param)=>{
        var url=param;
        console.log(url);
        return url;
      }
      download=(judul,url)=>{
    
        const android = RNFetchBlob.android
     
        RNFetchBlob.config({
            addAndroidDownloads : {
              useDownloadManager : true,
              title : 'Qcorner Download',
              description : 'Download file'+judul,
            
              mediaScannable : true,
              notification : true,
            }
          })
          .fetch('GET', url)
          .then((res) => {
              android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
          })
       
      }
      useEffect(() => {
  CariAju();
  GetUrl();
      }, []);
  return (
      
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>

    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} title={'Penerbitan Billing'} />
      <ScrollView>
        <View style={{flex:1,backgroundColor:'',marginBottom:20}}>
          
        </View>
        <View style={{flex:1,alignItems:'center',marginTop:0}}>
          <Text style={{fontSize:30,color:'#917369',fontWeight:'bold', alignItems:'center', textAlign:'center'}}>Proses Penerbitan Billing</Text>
          <Text style={{fontSize:15,marginTop:10}}>
            Permohonan Anda dengan Nomor Aju
          </Text>
          <Text style={{fontSize:15,fontWeight:'bold'}}>
            {noaju}
          </Text>
          
          <Text style={{fontSize:15}}>
            Sedang Dalam Proses Pembuatan Billing
          </Text>
          <Text style={{fontSize:15}}>
          </Text>
          <Text style={{fontSize:15}}>
            Anda dapat menghubungi xxxxx
          </Text>
          <Text style={{fontSize:15}}>
            untuk pertanyaan dan informasi lebih lanjut
          </Text>
          <Text style={{fontSize:15}}>
          </Text>
        </View> 
      </ScrollView>

    </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
    txt_head: {
        color: '#999ca0'
    },
    txt_val: {
      color: '#333a42'
    }
})