import { StyleSheet, Text, View ,ImageBackground,ScrollView,TouchableOpacity,Image,AsyncStorage} from 'react-native'
import React ,{useEffect,useState} from 'react'
import { background, banktransfer, icondown, tiga } from '../../assets'
import FitImage from 'react-native-fit-image';
import { Header } from '../../components';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';

const DraftPermohonanPerbaikan = () => {
    const {noaju}=route.params
    const [load_aju_error,setLoadAjuError]=useState(false);
    const [uri_path,setUriPath]=useState(false);
  return (
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>

    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} title={'Draft Permohonan Perbaikan'} />
      <ScrollView>
        <View style={{flex:1,backgroundColor:'',marginBottom:20}}>
          
        </View>
        <View style={{flex:1,alignItems:'center',marginTop:0}}>
          <Text style={{fontSize:30,color:'#917369',fontWeight:'bold', alignItems:'center', textAlign:'center'}}>Permohonan Perbaikan</Text>
          <Text style={{fontSize:15,marginTop:10}}>
            Permohonan Anda dengan Nomor Aju
          </Text>
          <Text style={{fontSize:15,fontWeight:'bold'}}>
            {noaju}
          </Text>
          
          <Text style={{fontSize:15}}>
            Sedang Dalam Proses Perbaikan Draft Sertifikat
          </Text>
          <Text style={{fontSize:15}}>
          </Text>
          <Text style={{fontSize:15}}>
            Anda dapat menghubungi Admin
          </Text>
          <Text style={{fontSize:15}}>
            untuk pertanyaan dan informasi lebih lanjut
          </Text>
          <Text style={{fontSize:15}}>
          </Text>
        </View> 
        
        <FooterBgAjuCweDitolak />
      </ScrollView>
      <FooterNavBar />

    </View>
    </ImageBackground>
  )
}

export default DraftPermohonanPerbaikan

const styles = StyleSheet.create({
    txt_head: {
        color: '#999ca0'
    },
    txt_val: {
      color: '#333a42'
    }
})