import { StyleSheet, Text, View ,ImageBackground,ScrollView,TouchableOpacity,Image,AsyncStorage} from 'react-native'
import React ,{useEffect,useState} from 'react'
import { background, banktransfer, icondown, tiga,dua } from '../../assets'
import FitImage from 'react-native-fit-image';
import { Header } from '../../components';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';

const PengajuanPpkDitolak = () => {
  return (
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>

    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} title={'Pengajuan PPK'} />
      <ScrollView>
        <View style={{flex:1,backgroundColor:'',marginBottom:20}}>
          <FitImage
            resizeMode="cover"
            originalWidth={400}
            originalHeight={50}
            source={dua}
            />
        </View>
        <View style={{flex:1,alignItems:'center',marginTop:0}}>
          <Text style={{fontSize:30,color:'#917369',fontWeight:'bold'}}>Sayang Sekali..</Text>
          <Text style={{fontSize:15,marginTop:10}}>
            Mohon maaf pengajuan permohonan anda
          </Text>
          <Text style={{fontSize:15}}>
            dengan Nomor Aju
          </Text>
          <Text style={{fontSize:15,fontWeight:'bold'}}>
             {noaju}
          </Text>
          <View style={{backgroundColor:'#e02020',width:'50%',alignItems:'center',borderRadius:5}}>
            <Text style={{fontSize:15,color:'white',fontWeight:'bold'}}>
               {detail_aju.inquery_status}
            </Text>
          </View>
          <View style={{flex:1,width:'80%',alignItems:'center',marginTop:0}}>
              <Text style={{fontSize:15,alignItems:'center'}}>
              Karena {detail_aju.last_note}
              </Text>
          </View>
          <Text style={{fontSize:15}}>
         
          </Text>
        </View> 
      </ScrollView>

    </View>
    </ImageBackground>
  )
}

export default PengajuanPpkDitolak

const styles = StyleSheet.create({})