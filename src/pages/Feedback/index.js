import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { cetakpermohonan } from "../../assets";
import { AirbnbRating } from "react-native-ratings";
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import { Header } from "../../components";
import { postAPI } from "../../utils/httpService";

export default function Feedback({navigation},props) {
    const [answer, setAnswer] = useState([]);
    const [jawab, setJawab] = useState([]);
    const {reducerSession} = useSelector(state => state);
    const [pertanyaaan_id, setPertanyaaan_id] = useState(0);
    const [user_id, setUser_id] = useState("");
    const [ratting, setRatting] = useState("");
    const [tokens, setToken] = useState("");
  
    // function onCreatePost(e){
    //   e.preventDefault();
    //   const postData = {
    //     pertanyaaan_id,
    //     user_id,
    //     ratting,
    //     value,
    //   };
    //   Axios.post(`${constant.api_corner}quisioner/answare`, postData ).then(response => {
    //     console.log(response);
    //   })
    // }
  
    useEffect(() => {
      getQuisioner();
    }, []);
  
    const getQuisioner =  () => {
      const token = reducerSession.token;
    axios({
        method: "get",
        url: "http://apiv2.bbkpsoetta.com/quisioner/",
        headers: {
          Authorization: "Bearer " + token,
        },
      })
        .then((res) => {
          console.log('pertanyaan',res.data);
          setAnswer(res.data.data.pertanyaan_list);
          setPertanyaaan_id(res.data.data.id_status_quisioner)
  
          let newJawaban = [];
          res.data.data.pertanyaan_list.map((item, index) => {
            newJawaban.push({
              id: item.pertanyaan_id,
              rating: 0,
              jawaban: "",
            });
          });
          setJawab(newJawaban);
        })
        .catch((err) => {
          console.log(err.message);
        });
    };
  
    const submit = () => {
      let myObjStr = JSON.stringify(jawab);
      var gabung="";
      console.log(jawab,'rif');
  
  
      jawab.map((obj,index) => 
      { 
        paramplus=index+1;
        gabung=gabung+'jawaban_'+obj.id+'='+obj.jawaban+'|rating_'+obj.id+"="+obj.rating+"|" } );
  
      let wadidaw=gabung.split("|");
      let uploaddata = [];
  
      wadidaw.map((obj,index) =>  
      {
        if(obj){
          let splitsamadengan=obj.split("=");
          // uploaddata.append(splitsamadengan[0].form,splitsamadengan[1]);
          uploaddata.push({form: {[splitsamadengan[0]]: splitsamadengan[1]}})
        }
  
      });
      console.log(uploaddata,'rif');
      const sendData = {
        ...uploaddata[0].form,
        ...uploaddata[1].form,
        ...uploaddata[2].form,
        ...uploaddata[3].form,
        ...uploaddata[4].form,
        ...uploaddata[5].form,
        ...uploaddata[6].form,
        ...uploaddata[7].form,
        id_status_quisioner: pertanyaaan_id
      }
      
      postAPI('quisioner/answare',sendData).then((response) => {
        console.log('answer : ', response);
        const body = {
          status: 1,
          id_status_quisioner: sendData.pertanyaaan_id
        }

        postAPI('quisioner/answare',body).then((response2) => {
           if (response2.message === "SUCCESS") {
              navigation.navigate("MainApp") 
            }
        })
  
      })
      .catch(error => {
        console.log('error : ',+error);
      });
      
    };
  
    const ratingCompleted = (obj) => {
      console.log("Rating is: " + obj.rating)
        let newJawaban = [...jawab];
        newJawaban[obj.index].rating =obj.rating;
        setJawab(newJawaban);
    };
  
    return (
      <ImageBackground source={cetakpermohonan}  style={styles.page}>
       
        <View style={styles.container}>
        <Header onPress={() => navigation.pop()} title={'Feedback'}/>
        <ScrollView showsVerticalScrollIndicator={false}>
          
        {/* <Text style={styles.title1}>Give Feedback</Text> */}
          {answer.map((item, index) => (
            <View key={index}>
              <Text style={styles.welcome}>{item.pertanyaan}</Text>
              {item.pertanyaan !== 'Aduan' ?
              (<View style={styles.bintang}>
              <AirbnbRating
                count={5}
                reviews={[
                  "Sangat Tidak Puas",
                  "Tidak Puas",
                  "Puas",
                  "Cukup Puas",
                  "Sangat Puas",
                ]}
                defaultRating={jawab.length > 0 ? jawab[index].rating : 5}
                onFinishRating={(rating)=>ratingCompleted({rating,index})}
                size={17}
                reviewSize={0}
                
              />
              </View>):(  <View style={styles.bintang2}></View>)}
              <View style={styles.border}>
            
              <TextInput
                placeholder="berikan tanggapan..."
                onChangeText={(value) => {
                  let newJawaban = [...jawab];
                  newJawaban[index].jawaban = value;
                  setJawab(newJawaban);
                }}
              /></View>
            </View>
          ))}
  
          <TouchableOpacity style={styles.button} onPress={submit}>
            <Text style={{ color: "white" }}>PUBLISH FEEDBACK</Text>
          </TouchableOpacity>
          <Text style={styles.text2}>
            Your review will be posted to the qcorner{" "}
          </Text>
          </ScrollView>
        </View>
      </ImageBackground>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex:1,
      justifyContent: "center",
      top: 15,
    },
    text: {
      color: "#ff0000",
      paddingLeft: 70,
      fontSize: 20,
      maxWidth: 300,
    },
    welcome: {
      fontSize: 15,
      marginTop: 20,
      marginBottom:-15,
      justifyContent: "center",
      fontWeight: "bold",
      marginLeft:2,
    },
    title: {},
    page: {
      padding: 10,
      justifyContent: 'space-between',
      flex: 1,
      backgroundColor:'white',
    },
    title1: {
      textAlign: "center",
      fontSize: 25,
      marginBottom:20,
    },
    content: {
      bottom: 60,
    },
    textAreaContainer: {
      borderColor: "#000000",
      borderWidth: 0.4,
      height: 70,
    },
    textArea: {
      height: 80,
      justifyContent: "flex-start",
      borderWidth: 1,
      borderColor: "#000",
      marginTop: 10,
    },
    textArea2: {
      height: 80,
      justifyContent: "flex-start",
      textAlignVertical: "center",
      borderWidth: 1,
      borderColor: "#000",
      marginTop: 10,
    },
    rate: {
      top: -60,
      marginRight: 50,
    },
    text2: {
      color: "#666666",
      textAlign: "center",
    },
    button: {
      alignItems: "center",
      backgroundColor: "#996633",
      padding: 10,
      marginTop: 40,
    },
    welcome1: {
      fontSize: 15,
      marginTop: 10,
      justifyContent: "center",
      alignItems: "center",
      fontWeight: "bold",
      textAlign: "center",
    },
    border: {
      borderColor: "#000000",
      borderWidth: 0.5,
    },
    bintang: {
      marginRight:220,
      marginBottom:10,
    },
    bintang2: {
      marginRight:220,
      marginBottom:40,
    },
  });
  