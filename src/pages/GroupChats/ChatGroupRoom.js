import React, {useState, useEffect} from 'react';
import {
  ActivityIndicator,
  Alert,
  Animated,
  Dimensions,
  FlatList,
  ImageBackground,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
  ToastAndroid,
  Modal,
} from 'react-native';
import {Left, Body, Right, Title} from 'native-base';
import {Appbar, Button, IconButton} from 'react-native-paper';
// import database from '@react-native-firebase/database';
import firebase from 'react-native-firebase';
import _, {wrap} from 'lodash';
import moment from 'moment';
import utf8 from 'utf8';
import base64 from 'base-64';

import Header from '../../components/Header';
import GeneralStatusBar from '../../components/StatusBar';
import useGetGroupMessages from '../../components/useGetGroupMessages';

const {height, width} = Dimensions.get('window');
const MIN_COMPOSER_HEIGHT = 41;
const MAX_COMPOSER_HEIGHT = 80;
const MIN_INPUT_TOOLBAR_HEIGHT = 56 + StatusBar.currentHeight;

const ChatGroupRoom = props => {
  const {navigation} = props;
  const {groupId, groupName, members, sessionLogin} = navigation.state.params;
  const {dataMessages, loading} = useGetGroupMessages({
    groupName,
    email: sessionLogin.data.user_data.email,
  });
  const [textMassage, setTextMassage] = useState('');
  const [messageList, setMessageList] = useState([]);
  const [participants, setParticipants] = useState([]);
  const [keyboardHeight, setKeyboadHeight] = useState(0);
  const [maxHeight, setMaxHeight] = useState(0);
  const [isFirstLayout, setIsFirstLayout] = useState(true);
  const [messagesContainerHeight, setMessageContainerHeight] = useState(
    new Animated.Value(0),
  );
  const [composerHeight, setComposerHeight] = useState(MIN_COMPOSER_HEIGHT);
  const [loadingLeaveGroup, setLoadingLeaveGroup] = useState(false);

  const onPressButtonLeft = () => {
    navigation.popToTop();
  };

  const onLeaveGroup = async () => {
    setLoadingLeaveGroup(true);
    // Convert to base64
    const member_encode_base64 = base64.encode(
      sessionLogin.data.user_data.email,
    );
    const member_encode_bytes = utf8.encode(member_encode_base64);
    try {
      await firebase
        .database()
        .ref(`/groups/${groupId}/members/${member_encode_bytes}`)
        .remove();
      await firebase
        .database()
        .ref(`/users/${member_encode_bytes}/groups/${groupId}`)
        .remove();
      setLoadingLeaveGroup(false);
      ToastAndroid.showWithGravity(
        'Berhasil keluar dari group',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      navigation.state.params.refresh();
      navigation.goBack();
    } catch (err) {
      console.log(err, 'err');
      setLoadingLeaveGroup(false);
    }
  };

  const onPressButtonRight = () => {
    Alert.alert(
      `Keluar dari group ${groupName}`,
      'Anda yakin ingin keluar dari group ini ?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: onLeaveGroup},
      ],
      {cancelable: false},
    );
  };

  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          alignItems:
            item.email === sessionLogin.data.user_data.email
              ? 'flex-end'
              : 'flex-start',
          marginTop: 5,
          paddingLeft: '5%',
          paddingRight: '5%',
        }}>
        <View
          style={{
            backgroundColor:
              item.email === sessionLogin.data.user_data.email
                ? '#dbf5b4'
                : '#bfbfbf',
            borderRadius: 8,
            maxWidth: '95%',
            paddingTop: 6,
            paddingLeft: 7,
            paddingBottom: 8,
            paddingRight: 9,
            position: 'relative',
          }}>
          {item.email !== sessionLogin.data.user_data.email && (
            <Text
              style={{
                fontSize: 13,
                color: '#145c94',
                marginTop: 8,
              }}>
              {item.name}
            </Text>
          )}
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-end',
            }}>
            <Text
              style={{
                fontSize: 16,
                color: '#0d0d0d',
                textAlign: 'left',
                alignSelf: 'flex-start',
                marginRight: 4,
                marginBottom: 8,
              }}>
              {item.message}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: '#999999',
                textAlign: 'right',
                alignSelf: 'flex-end',
                marginBottom: 5,
              }}>
              {moment(item.time).format('HH:mm')}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  const sendMessage = () => {
    const newTextMessage = textMassage;
    if (sessionLogin && newTextMessage !== '') {
      setTextMassage('');
      setComposerHeight(MIN_COMPOSER_HEIGHT);
      // Convert to base64
      const member_encode_base64 = base64.encode(
        sessionLogin.data.user_data.email,
      );
      const member_encode_bytes = utf8.encode(member_encode_base64);

      //SendNotif(sessionLogin.data.user_data.email,person.email,newTextMessage,token)
      // Push to firebase (send and receive)
      firebase
        .database()
        .ref(`/messages/${groupId}/${member_encode_bytes}`)
        .push({
          name: sessionLogin.data.user_data.name,
          email: sessionLogin.data.user_data.email,
          message: newTextMessage,
          time: firebase.database.ServerValue.TIMESTAMP,
        })
        .then(() => {
          
         
         // Store header user conversations
         
         firebase
            .database()
            .collection(`/user_conversations/${groupId}`)
            .forEach((doc) => {
              console.log(doc,'wew');
           });

          firebase
            .database()
            .ref(`/user_conversations/${groupId}`)
            .set({
              name: sessionLogin.data.user_data.name,
              email: sessionLogin.data.user_data.email,
              title: groupName,
              lastMessage: newTextMessage,
              time: firebase.database.ServerValue.TIMESTAMP,
            });
        });
    }
  };
  const SendNotif=(from,to,message,token) => {
  
    console.log(token);
     if (token) {
 
             let _Param = new FormData();
             _Param.append('from',from);
             _Param.append('to', to);
             _Param.append('message', message);
             console.log(_Param,'paramnya')
             axios({
               method: 'POST',
               url: constant.api_corner + '/sendmsg',
               data: _Param,
               headers: {
                 'Authorization': 'Bearer ' +token
               }
             })
             .then(response => {
               console.log('Success');
               this.setState({ loadingSubmit: false });
             })
             .catch(function (error) {
               console.log('Error' + error.message);
               //Alert.alert(error.message);
               this.setState({ loadingSubmit: false });
             });
     }
 
   }
  const prepareMessagesContainerHeight = value => {
    if (props.isAnimated === true) {
      return new Animated.Value(value);
    }
    return value;
  };

  const getMinInputToolbarHeight = () => {
    return MIN_INPUT_TOOLBAR_HEIGHT;
  };

  const getMaxHeight = () => {
    return maxHeight;
  };

  const _keyboardDidShow = e => {
    const newKeyboardHeight = e.endCoordinates
      ? e.endCoordinates.height
      : e.end.height;
    setKeyboadHeight(newKeyboardHeight);
    const newMessagesContainerHeight =
      getMaxHeight() -
      (composerHeight + (getMinInputToolbarHeight() - MIN_COMPOSER_HEIGHT)) -
      newKeyboardHeight;
    if (props.isAnimated === true) {
      Animated.timing(messagesContainerHeight, {
        toValue: newMessagesContainerHeight,
        duration: 100,
      }).start();
    } else {
      setMessageContainerHeight(newMessagesContainerHeight);
    }
  };

  const _keyboardDidHide = e => {
    setKeyboadHeight(0);
    const newMessagesContainerHeight =
      getMaxHeight() -
      (composerHeight + (getMinInputToolbarHeight() - MIN_COMPOSER_HEIGHT));
    if (props.isAnimated === true) {
      Animated.timing(messagesContainerHeight, {
        toValue: newMessagesContainerHeight,
        duration: 210,
      }).start();
    } else {
      setMessageContainerHeight(newMessagesContainerHeight);
    }
  };

  const onMainViewLayout = e => {
    // fix an issue when keyboard is dismissing during the initialization
    const layout = e.nativeEvent.layout;
    if (getMaxHeight() !== layout.height && isFirstLayout === true) {
      setMaxHeight(layout.height);
      setMessageContainerHeight(
        prepareMessagesContainerHeight(
          layout.height - getMinInputToolbarHeight(),
        ),
      );
    }
    if (isFirstLayout === true) {
      setIsFirstLayout(false);
    }
  };

  const calculateInputToolbarHeight = newComposerHeight => {
    return (
      newComposerHeight + (getMinInputToolbarHeight() - MIN_COMPOSER_HEIGHT)
    );
  };

  const onInputSizeChanged = size => {
    const newComposerHeight = Math.max(
      MIN_COMPOSER_HEIGHT,
      Math.min(MAX_COMPOSER_HEIGHT, size.height),
    );
    const newMessagesContainerHeight =
      getMaxHeight() -
      calculateInputToolbarHeight(newComposerHeight) -
      keyboardHeight;
    setComposerHeight(newComposerHeight);
    setMessageContainerHeight(
      prepareMessagesContainerHeight(newMessagesContainerHeight),
    );
  };

  useEffect(() => {
    const _keyboardDidShowSubscription = Keyboard.addListener(
      'keyboardDidShow',
      e => _keyboardDidShow(e),
    );
    const _keyboardDidHideSubscription = Keyboard.addListener(
      'keyboardDidHide',
      e => _keyboardDidHide(e),
    );
    return () => {
      _keyboardDidShowSubscription.remove();
      _keyboardDidHideSubscription.remove();
    };
  });

  useEffect(() => {
    if (dataMessages !== null) {
      const mapMessages = _.map(dataMessages, (messages, key) => {
        return _.map(messages, (message, uid) => {
          return {...message, userKey: key, uid};
        });
      });
      const flattenMapMessages = _.flatten(mapMessages);
      flattenMapMessages.sort((a, b) => {
        return new Date(b.time) - new Date(a.time);
      });
      setMessageList(flattenMapMessages);
    }
  }, [dataMessages]);

  useEffect(() => {
    if (members.length > 0) {
      for (let i = 0; i < members.length; i++) {
        firebase
          .database()
          .ref('users/' + members[i])
          .on('value', snapshot => {
            setParticipants(prev => {
              return [...prev, snapshot.val()];
            });
          });
      }
    }
  }, [members]);

  const subtitle = participants.map((participant, index) => {
    return (
      <Text key={index}>
        {index > 0 && ', '}
        {participant.name}
      </Text>
    );
  });

  return (
    <View style={{flex: 1}}>
      <GeneralStatusBar />
      <Header
        title={groupName}
        subtitle={subtitle}
        navigation={navigation}
        buttonTextRight="Leave"
        onPressButtonLeft={onPressButtonLeft}
        onPressButtonRight={onPressButtonRight}
      />
      <View style={{flex: 1}} onLayout={onMainViewLayout}>
        <KeyboardAvoidingView style={styles.keyboardAvoidContainer}>
          {loading ? (
            <View style={styles.bodyLoading}>
              <ActivityIndicator />
            </View>
          ) : (
            <>
              <Animated.View
                style={{
                  height: messagesContainerHeight,
                }}>
                <View style={{flex: 1}}>
                  <FlatList
                    style={styles.flatlist}
                    inverted
                    data={messageList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderItem}
                  />
                </View>
              </Animated.View>
              <View
                style={{
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  right: 0,
                }}>
                <View style={styles.parentSectionMessage}>
                  <View style={styles.sectionSendMessage}>
                    <TextInput
                      value={textMassage}
                      multiline
                      onChangeText={text => setTextMassage(text)}
                      onContentSizeChange={event =>
                        onInputSizeChanged(event.nativeEvent.contentSize)
                      }
                      style={[
                        styles.textInput,
                        {
                          height: Math.min(
                            MAX_COMPOSER_HEIGHT,
                            Math.max(MIN_COMPOSER_HEIGHT, composerHeight),
                          ),
                          backgroundColor: props.textInputBgColor,
                          color: props.messageTextColor,
                        },
                      ]}
                    />
                  </View>
                  <IconButton
                    icon="send"
                    size={18}
                    disabled={textMassage === ''}
                    color="#FFF"
                    style={styles.buttonSend}
                    onPress={sendMessage}
                  />
                </View>
              </View>
            </>
          )}
        </KeyboardAvoidingView>
      </View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={loadingLeaveGroup}
        onRequestClose={() => {
          setLoadingLeaveGroup(false);
        }}>
        <View style={styles.containerLoading}>
          <View style={styles.modalView}>
            <ActivityIndicator />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  keyboardAvoidContainer: {
    flex: 1,
  },
  bodyLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatlist: {
    flex: 1,
  },
  parentSectionMessage: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderTopWidth: 1,
    paddingVertical: 5,
    borderTopColor: '#DDDDDD',
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  },
  sectionSendMessage: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    flex: 1,
  },
  textInput: {
    fontSize: 14,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    paddingLeft: 10,
    paddingTop: 8,
    textAlign: 'left',
    borderRadius: 5,
    maxHeight: 80,
  },
  buttonSend: {
    borderRadius: 50,
    marginRight: 0,
    alignItems: 'center',
    backgroundColor: '#917369',
  },
  buttonAdd: {
    marginLeft: 0,
    alignItems: 'center',
  },
  containerLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: 16,
  },
  modalView: {
    backgroundColor: '#FFF',
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    elevation: 4,
  },
});

ChatGroupRoom.defaultProps = {
  isAnimated: Platform.select({
    ios: true,
    android: false,
  }),
  textInputBgColor: '#f5f5f5',
  messageTextColor: '#000',
};

export default ChatGroupRoom;
