import React, {useState, useEffect} from 'react';
import {FlatList, View, ScrollView, Text, Image} from 'react-native';
import {
  Avatar,
  IconButton,
  Searchbar,
  Surface,
  TouchableRipple,
} from 'react-native-paper';
import _ from 'lodash';
import utf8 from 'utf8';
import base64 from 'base-64';

import useGetSessionLogin from '../../components/useGetSessionLogin';
import useGetUsers from '../../components/useGetUsers';
import Header from '../../components/Header';
import GeneralStatusBar from '../../components/StatusBar';

const defaultAvatar = require('../../image/coklatprofil.png');

const AddParticipants = props => {
  const {navigation} = props;
  const {sessionLogin} = useGetSessionLogin();
  const {users, loading} = useGetUsers(
    sessionLogin !== null ? sessionLogin.data.user_data.email : null,
  );
  const [contacts, setContacts] = useState([]);
  const [contactsHolder, setContactsHolder] = useState([]);
  const [selectedParticipants, setSelectedParticipants] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [onNext, setOnNext] = useState(false);

  const searchFilter = text => {
    setSearchQuery(text);
    const newData = contactsHolder.filter(item => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    setContacts(newData);
  };

  const toggleParticipant = item => {
    setSelectedParticipants(selected => {
      const newSelected = [...selected];
      const indexParticipant = newSelected.findIndex(
        tempart => tempart.email === item.email,
      );
      if (indexParticipant === -1) {
        newSelected.push(item);
      } else {
        newSelected.splice(indexParticipant, 1);
      }
      return newSelected;
    });
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableRipple onPress={() => toggleParticipant(item)}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            padding: 15,
            borderBottomWidth: 1,
            borderColor: '#b7b7b7',
          }}>
          <Image
            source={defaultAvatar}
            style={{width: 50, height: 50, borderRadius: 50}}
          />
          <View style={{marginLeft: 15}}>
            <Text style={{fontSize: 23, fontWeight: 'bold'}}>{item.name}</Text>
            <Text style={{fontSize: 13}}>{item.email}</Text>
          </View>
        </View>
      </TouchableRipple>
    );
  };

  useEffect(() => {
    if (users !== null) {
      const mapUsers = _.map(users, (value, uid) => {
        return {...value, uid};
      });
      setContacts(mapUsers);
      setContactsHolder(mapUsers);
    }
  }, [users]);

  return (
    <View style={{flex: 1}}>
      <GeneralStatusBar />
      <Header
        title="Add participants"
        navigation={navigation}
        buttonTextRight="next"
        buttonTextLeft="cancel"
        buttonRightDisabled={selectedParticipants.length <= 0}
        onPressButtonRight={() =>
          navigation.navigate('NewGroup', {selectedParticipants, sessionLogin})
        }
      />
      <Searchbar
        placeholder="Search"
        onChangeText={searchFilter}
        value={searchQuery}
      />
      {selectedParticipants.length > 0 && (
        <Surface
          style={{
            paddingVertical: 10,
            paddingHorizontal: 16,
            elevation: 4,
          }}>
          <FlatList
            data={selectedParticipants}
            horizontal
            keyExtractor={(item, index) => item.uid}
            renderItem={({item, index}) => (
              <View
                style={{
                  marginRight: 8,
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'relative',
                  width: 80,
                  height: 90,
                }}>
                <IconButton
                  icon="close"
                  size={18}
                  style={{
                    backgroundColor: '#A8A699',
                    position: 'absolute',
                    top: -4,
                    right: 0,
                    zIndex: 1,
                  }}
                  onPress={() => toggleParticipant(item)}
                />
                <Avatar.Icon
                  size={50}
                  icon="account"
                  style={{marginBottom: 4}}
                />
                <Text style={{fontSize: 16}}>{item.name}</Text>
              </View>
            )}
          />
        </Surface>
      )}

      <FlatList
        style={{flex: 1}}
        data={contacts}
        renderItem={renderItem}
        keyExtractor={(item, index) => item.uid}
      />
    </View>
  );
};

export default AddParticipants;
