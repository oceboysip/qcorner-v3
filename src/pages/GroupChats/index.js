import React from 'react';
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  Image,
  View,
  StyleSheet,
  Text,
} from 'react-native';
import {TouchableRipple} from 'react-native-paper';

import useGetSessionLogin from '../../components/useGetSessionLogin';
import useGetChats from '../../components/useGetChats';
import useGetUserGroups from '../../components/useGetUserGroups';
import TabPadding from '../../components/TabPadding';

const {width, height} = Dimensions.get('window');

const defaultAvatar = require('../../image/coklatprofil.png');

const GroupChats = ({navigation}) => {
  const {sessionLogin} = useGetSessionLogin();
  const {dataGroups, loading, refresh} = useGetUserGroups(
    sessionLogin !== null ? sessionLogin.data.user_data.email : null,
  );
  const keyExtractor = item => item.groupId;

  const renderHeader = () => {
    return <TabPadding />;
  };

  const renderEmptyComponent = () => {
    return (
      <View style={styles.containerEmptyComponent}>
        <Text>Chat group tidak ada</Text>
      </View>
    );
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableRipple
        onPress={() =>
          navigation.navigate('ChatGroupRoom', {...item, sessionLogin, refresh})
        }>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingHorizontal: 16,
            paddingVertical: 8,
            maxHeight: 100,
            minHeight: 80,
            borderBottomWidth: 1,
            borderColor: '#b7b7b7',
          }}>
          <Image
            source={item.profileImage ? {uri: item.groupImage} : defaultAvatar}
            style={{width: 50, height: 50, borderRadius: 50}}
          />
          <View style={{marginLeft: 15, width: width - 100}}>
            <Text style={{fontSize: 23, fontWeight: 'bold'}}>
              {item.groupName}
            </Text>
            {item.name && item.lastMessage && (
              <Text numberOfLines={2} style={{fontSize: 13, flex: 1}}>
                {item.name}: {item.lastMessage}
              </Text>
            )}
          </View>
        </View>
      </TouchableRipple>
    );
  };
  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" />
        </View>
      ) : (
        <FlatList
          style={{flex: 1}}
          data={dataGroups}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
          ListHeaderComponent={renderHeader}
          ListEmptyComponent={renderEmptyComponent}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerLoading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmptyComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: height / 3,
  },
});

export default GroupChats;
