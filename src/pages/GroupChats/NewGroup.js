import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  Modal,
  StyleSheet,
  View,
} from 'react-native';
import {Avatar, HelperText, List, TextInput} from 'react-native-paper';
import database from '@react-native-firebase/database';
//import firebase from 'react-native-firebase';
import {NavigationActions, StackActions} from 'react-navigation';
import utf8 from 'utf8';
import base64 from 'base-64';
import _ from 'lodash';

import Header from '../../components/Header';
import GeneralStatusBar from '../../components/StatusBar';

const NewGroup = props => {
  const {navigation} = props;
  const selectedParticipants = navigation.getParam('selectedParticipants');
  const sessionLogin = navigation.getParam('sessionLogin');
  const [groupSubject, setGroupSubject] = useState('');
  const [loadingCreateGroup, setLoadingCreateGroup] = useState(false);

  const onCreateGroup = async () => {
    setLoadingCreateGroup(true);
    const bytesGroupName = utf8.encode(groupSubject);
    const encodeGroupName = base64.encode(bytesGroupName);
    const arrayParticipants = [
      ...selectedParticipants,
      ...[
        {
          email: sessionLogin.data.user_data.email,
          name: sessionLogin.data.user_data.name,
        },
      ],
    ];
    let members = arrayParticipants.reduce((accumulator, currentValue) => {
      const participant_email_bytes = utf8.encode(currentValue.email);
      const participant_email_encode = base64.encode(participant_email_bytes);
      let obj = {[participant_email_encode]: true};
      accumulator = Object.assign(accumulator, obj);
      return accumulator;
    }, {});

   database()
      .ref('groups/' + encodeGroupName)
      .once('value', snapshot => {
        const val = snapshot.val();
        if (val === null) {
         database()
            .ref('groups/' + encodeGroupName)
            .set({
              group_name: groupSubject,
              group_description: '',
              members,
            })
            .then(() => {
              _.mapKeys(members, (value, key) => {
                firebase
                  .database()
                  .ref('users/' + key)
                  .once('value')
                  .then(snapshot => {
                    const val = snapshot.val();
                    database()
                      .ref('users/' + key)
                      .update({
                        groups: {
                          ...val.groups,
                          [encodeGroupName]: true,
                        },
                      });
                  });
              });
              setLoadingCreateGroup(false);
              const navigateAction = NavigationActions.navigate({
                routeName: 'MainChat',

                action: NavigationActions.navigate({routeName: 'Group'}),
              });

              navigation.dispatch(navigateAction);
              // navigation.navigate('ChatGroupRoom', {
              //   participants: arrayParticipants,
              //   groupName: groupSubject,
              // });
            });
        } else {
          setLoadingCreateGroup(false);
          Alert.alert(
            `Gagal`,
            'Grup ini sudah ada',
            [{text: 'OK', onPress: () => {}}],
            {cancelable: false},
          );
        }
      });
  };

  return (
    <View style={styles.container}>
      <GeneralStatusBar />
      <Header
        title="New Group"
        navigation={navigation}
        buttonTextRight="create"
        buttonRightDisabled={
          selectedParticipants.length <= 0 || groupSubject === ''
        }
        onPressButtonRight={onCreateGroup}
      />
      <View style={styles.wrapperInput}>
        <Avatar.Icon size={50} icon="account" />
        <View style={styles.textInput}>
          <TextInput
            placeholder="Group Subject"
            value={groupSubject}
            onChangeText={text => setGroupSubject(text)}
          />
          <HelperText visible={true}>Must provied group subject</HelperText>
        </View>
      </View>
      <List.Section>
        <List.Subheader>Participants</List.Subheader>
        <FlatList
          data={selectedParticipants}
          keyExtractor={(item, index) => item.uid}
          renderItem={({item, index}) => (
            <List.Item
              title={item.name}
              left={() => <List.Icon icon="account" />}
            />
          )}
        />
      </List.Section>
      <Modal
        animationType="fade"
        transparent={true}
        visible={loadingCreateGroup}
        onRequestClose={() => {
          setLoadingCreateGroup(false);
        }}>
        <View style={styles.containerLoading}>
          <View style={styles.modalView}>
            <ActivityIndicator />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapperInput: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textInput: {
    flex: 1,
    marginLeft: 8,
    backgroundColor: '#FFFFFF',
  },
  containerLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: 16,
  },
  modalView: {
    backgroundColor: '#FFF',
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    elevation: 4,
  },
});

export default NewGroup;
