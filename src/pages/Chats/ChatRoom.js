import React, {useEffect, useState} from 'react';
import {

  ActivityIndicator,
  Animated,
  Dimensions,
  FlatList,

  Keyboard,
  KeyboardAvoidingView,
 
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import {Left, Body, Right, Title} from 'native-base';
import {Appbar, Button, IconButton} from 'react-native-paper';
import database from '@react-native-firebase/database';
import moment from 'moment';
import utf8 from 'utf8';
import base64 from 'base-64';
import _ from 'lodash';
import axios from 'axios';

import Header from '../../components/Header';
import GeneralStatusBar from '../../components/StatusBar';
import useGetSessionLogin from '../../components/useGetSessionLogin';
import useGetMessages from '../../components/useGetMessages';
import { useSelector } from 'react-redux';
//import {getToken} from '../helpers/MyHelper';


const {width} = Dimensions.get('window');

const MIN_COMPOSER_HEIGHT = 41;
const MAX_COMPOSER_HEIGHT = 80;
const MIN_INPUT_TOOLBAR_HEIGHT = 50 + StatusBar.currentHeight;

const ChatRoom = ({ route, navigation })  => {
  const {name,email} = route.params;
 
  const {sessionLogin} = useGetSessionLogin();
  const {reducerSession} = useSelector(state => state); 
  const [person, setPerson] = useState({
    name: name,
    email: email
  });
 
  const {dataMessages, loading} = useGetMessages(
    sessionLogin !== null ? reducerSession.email : null,
    person.email,
  );
  const [textMassage, setTextMassage] = useState('');
  const [messageList, setMessageList] = useState([]);
  const [keyboardHeight, setKeyboadHeight] = useState(0);
  const [maxHeight, setMaxHeight] = useState(0);
  const [isFirstLayout, setIsFirstLayout] = useState(true);
  const [messagesContainerHeight, setMessageContainerHeight] = useState(
    new Animated.Value(0),
  );
  const [composerHeight, setComposerHeight] = useState(MIN_COMPOSER_HEIGHT);

  const sendMessage = async () => {
    if (sessionLogin) {
      // Convert to base64
      const current_email_bytes = utf8.encode(
        reducerSession.email,
      );
      const current_email_encode = base64.encode(current_email_bytes);
      const contact_email_bytes = utf8.encode(person.email);
      const contact_email_encode = base64.encode(contact_email_bytes);
      const newTextMessage = textMassage;
      // const token=await getToken();
      if (newTextMessage.length > 0) {

        //console.log('yang dikirim '+person.email+' message '+newTextMessage)

       // SendNotif(sessionLogin.data.user_data.email,person.email,newTextMessage,token)
        setTextMassage('');
        setComposerHeight(MIN_COMPOSER_HEIGHT);
        // Push to firebase (send and receive)
        await database()
          .ref(`/messages/${current_email_encode}/${contact_email_encode}`)
          .push({
            message: newTextMessage,
            time: database.ServerValue.TIMESTAMP,
            type: 'send',
          });
        await database()
          .ref(`/messages/${contact_email_encode}/${current_email_encode}`)
          .push({
            message: newTextMessage,
            time: database.ServerValue.TIMESTAMP,
            type: 'receive',
          });
        // Store header user conversations
        await database()
          .ref(
            `/user_conversations/${current_email_encode}/${contact_email_encode}`,
          )
          .set({
            name: person.name,
            email: person.email,
            lastMessage: newTextMessage,
          });
        // Store header contact conversations
        console.log('wew', `/user_conversations/${contact_email_encode}/${current_email_encode}`)
        await database()
          .ref(
            `/user_conversations/${contact_email_encode}/${current_email_encode}`,
          )
          .set({
            name: reducerSession.name,
            email: reducerSession.email,
            lastMessage: newTextMessage,
          });
      }
    }
  };

  const prepareMessagesContainerHeight = value => {
    if (navigation.isAnimated === true) {
      return new Animated.Value(value);
    }
    return value;
  };

  const SendNotif=(from,to,message,token) => {
  
   console.log(token);
    if (token) {

            let _Param = new FormData();
            _Param.append('from',from);
            _Param.append('to', to);
            _Param.append('message', message);
            console.log(_Param,'paramnya')
            // axios({
            //   method: 'POST',
            //   url: constant.api_corner + '/sendmsg',
            //   data: _Param,
            //   headers: {
            //     'Authorization': 'Bearer ' +token
            //   }
            // })
            // .then(response => {
            //   console.log('Success');
            //   this.setState({ loadingSubmit: false });
            // })
            // .catch(function (error) {
            //   console.log('Error' + error.message);
            //   //Alert.alert(error.message);
            //   this.setState({ loadingSubmit: false });
            // });
    }

  }
  const getMinInputToolbarHeight = () => {
    return MIN_INPUT_TOOLBAR_HEIGHT;
  };

  const getMaxHeight = () => {
    return maxHeight;
  };

  const _keyboardDidShow = e => {
    const newKeyboardHeight = e.endCoordinates
      ? e.endCoordinates.height
      : e.end.height;
    setKeyboadHeight(newKeyboardHeight);
    const newMessagesContainerHeight =
      getMaxHeight() -
      (composerHeight + (getMinInputToolbarHeight() - MIN_COMPOSER_HEIGHT)) -
      newKeyboardHeight;
    if (navigation.isAnimated === true) {
      Animated.timing(messagesContainerHeight, {
        toValue: newMessagesContainerHeight,
        duration: 100,
      }).start();
    } else {
      setMessageContainerHeight(newMessagesContainerHeight);
    }
  };

  const _keyboardDidHide = e => {
    setKeyboadHeight(0);
    const newMessagesContainerHeight =
      getMaxHeight() -
      (composerHeight + (getMinInputToolbarHeight() - MIN_COMPOSER_HEIGHT));
    if (navigation.isAnimated === true) {
      Animated.timing(messagesContainerHeight, {
        toValue: newMessagesContainerHeight,
        duration: 210,
      }).start();
    } else {
      setMessageContainerHeight(newMessagesContainerHeight);
    }
  };

  const onMainViewLayout = e => {
    // fix an issue when keyboard is dismissing during the initialization
    const layout = e.nativeEvent.layout;
    if (getMaxHeight() !== layout.height && isFirstLayout === true) {
      setMaxHeight(layout.height);
      setMessageContainerHeight(
        prepareMessagesContainerHeight(
          layout.height - getMinInputToolbarHeight(),
        ),
      );
    }
    if (isFirstLayout === true) {
      setIsFirstLayout(false);
    }
  };

  const calculateInputToolbarHeight = newComposerHeight => {
    return (
      newComposerHeight + (getMinInputToolbarHeight() - MIN_COMPOSER_HEIGHT)
    );
  };

  const onInputSizeChanged = size => {
    const newComposerHeight = Math.max(
      MIN_COMPOSER_HEIGHT,
      Math.min(MAX_COMPOSER_HEIGHT, size.height),
    );
    const newMessagesContainerHeight =
      getMaxHeight() -
      calculateInputToolbarHeight(newComposerHeight) -
      keyboardHeight;
    setComposerHeight(newComposerHeight);
    setMessageContainerHeight(
      prepareMessagesContainerHeight(newMessagesContainerHeight),
    );
  };

  useEffect(() => {
    const _keyboardDidShowSubscription = Keyboard.addListener(
      'keyboardDidShow',
      e => _keyboardDidShow(e),
    );
    const _keyboardDidHideSubscription = Keyboard.addListener(
      'keyboardDidHide',
      e => _keyboardDidHide(e),
    );
    return () => {
      _keyboardDidShowSubscription.remove();
      _keyboardDidHideSubscription.remove();
    };
  });

  useEffect(() => {
    if (dataMessages !== null) {

   
      const mapMessages = _.map(dataMessages, (value, uid) => {
        return {...value, uid};
      });
      mapMessages.sort((a, b) => {
        return new Date(b.time) - new Date(a.time);
      });
      setMessageList(mapMessages);
    }
  }, [dataMessages]);

  const renderItem = ({item, index}) => {
    return (
      <View
        style={{
          alignItems: item.type === 'send' ? 'flex-end' : 'flex-start',
          marginTop: 5,
          paddingLeft: '5%',
          paddingRight: '5%',
        }}>
        <View
          style={{
            backgroundColor: item.type === 'send' ? '#dbf5b4' : '#bfbfbf',
            borderRadius: 8,
            maxWidth: '95%',
            paddingTop: 6,
            paddingLeft: 7,
            paddingBottom: 8,
            paddingRight: 9,
            position: 'relative',
          }}>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'flex-end',
            }}>
            <Text
              style={{
                fontSize: 16,
                color: '#0d0d0d',
                alignSelf: 'flex-start',
                marginRight: 4,
                marginBottom: 8,
                left: 0,
                letterSpacing: 0.5,
              }}>
              {item.message}
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: '#999999',
                textAlign: 'right',
                alignSelf: 'flex-end',
                marginBottom: 5,
                letterSpacing: 0.5,
              }}>
              {moment(item.time).format('HH:mm')}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      <GeneralStatusBar />
      <Header title={person.name} navigation={navigation} />
      <View style={{flex: 1}} onLayout={onMainViewLayout}>
        <KeyboardAvoidingView style={styles.keyboardAvoidContainer}>
          {loading ? (
            <View style={styles.bodyLoading}>
              <ActivityIndicator />
            </View>
          ) : (
            <>
              <Animated.View
                style={{
                  height: messagesContainerHeight,
                }}>
                <View style={{flex: 1}}>
                  <FlatList
                    style={styles.flatlist}
                    inverted
                    data={messageList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderItem}
                  />
                </View>
              </Animated.View>
              <View
                style={{
                  position: 'absolute',
                  bottom: 0,
                  left: 0,
                  right: 0,
                }}>
                <View style={styles.parentSectionMessage}>
                  <View style={styles.sectionSendMessage}>
                    <TextInput
                      value={textMassage}
                      multiline
                      onChangeText={text => setTextMassage(text)}
                      onContentSizeChange={event =>
                        onInputSizeChanged(event.nativeEvent.contentSize)
                      }
                      style={[
                        styles.textInput,
                        {
                          height: Math.min(
                            MAX_COMPOSER_HEIGHT,
                            Math.max(MIN_COMPOSER_HEIGHT, composerHeight),
                          ),
                        },
                      ]}
                    />
                  </View>
                  <IconButton
                    icon="send"
                    size={18}
                    disabled={textMassage === ''}
                    color="#FFF"
                    style={styles.buttonSend}
                    onPress={sendMessage}
                  />
                </View>
              </View>
            </>
          )}
        </KeyboardAvoidingView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  keyboardAvoidContainer: {
    flex: 1,
  },
  bodyLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatlist: {
    flex: 1,
  },
  parentSectionMessage: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderTopWidth: 1,
    paddingVertical: 5,
    borderTopColor: '#DDDDDD',
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  },
  sectionSendMessage: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    flex: 1,
  },
  textInput: {
    fontSize: 14,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    paddingLeft: 10,
    paddingTop: 8,
    textAlign: 'left',
    borderRadius: 5,
    maxHeight: 80,
    backgroundColor: '#f5f5f5',
    color: '#000',
  },
  buttonSend: {
    borderRadius: 50,
    marginRight: 0,
    alignItems: 'center',
    backgroundColor: '#917369',
  },
  buttonAdd: {
    marginLeft: 0,
    alignItems: 'center',
  },
});

ChatRoom.defaultProps = {
  isAnimated: true,
  textInputBgColor: '#f5f5f5',
  messageTextColor: '#000',
};

export default ChatRoom;
