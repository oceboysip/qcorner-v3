import React, {useState, useEffect} from 'react';
import {FlatList, View, TouchableHighlight, Text, Image} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import _ from 'lodash';

import useGetSessionLogin from '../../components/useGetSessionLogin';
import useGetUsers from '../../components/useGetUsers';
import Header from '../../components/Header';
import GeneralStatusBar from '../../components/StatusBar';
import { useSelector } from 'react-redux';

const defaultAvatar = require('../../image/coklatprofil.png');

const NewChat = props => {
  const {navigation} = props;
  const {reducerSession} = useSelector(state => state); 
  const {sessionLogin} = useGetSessionLogin();
  const {users, loading} = useGetUsers(
    sessionLogin !== null ? reducerSession.email : null,
  );
  const [contacts, setContacts] = useState([]);

  const renderItem = ({item, index}) => {
    if (item.name === 'New Contact' || item.name === 'New Group') {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            padding: 15,
            borderBottomWidth: 1,
            borderColor: '#b7b7b7',
          }}>
          <Image
            source={{uri: item.profileImage}}
            style={{width: 50, height: 50, borderRadius: 50}}
          />
          <View style={{marginLeft: 15, marginTop: 8}}>
            <Text style={{fontSize: 23, fontWeight: 'bold'}}>{item.name}</Text>
          </View>
        </View>
      );
    }

    const SendNotif=(from,to,message,token) => {
  
   console.log(token);
    if (token) {

            let _Param = new FormData();
            _Param.append('from',from);
            _Param.append('to', to);
            _Param.append('message', message);
            console.log(_Param,'paramnya')
            axios({
              method: 'POST',
              url: constant.api_corner + '/sendmsg',
              data: _Param,
              headers: {
                'Authorization': 'Bearer ' +token
              }
            })
            .then(response => {
              console.log('Success');
              this.setState({ loadingSubmit: false });
            })
            .catch(function (error) {
              console.log('Error' + error.message);
              //Alert.alert(error.message);
              this.setState({ loadingSubmit: false });
            });
    }

  }
    return (
      <TouchableRipple
        onPress={() => {
          navigation.goBack();
          navigation.navigate('ChatRoom', {...item});
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            padding: 15,
            borderBottomWidth: 1,
            borderColor: '#b7b7b7',
          }}>
          <Image
            source={defaultAvatar}
            style={{width: 50, height: 50, borderRadius: 50}}
          />
          <View style={{marginLeft: 15}}>
            <Text style={{fontSize: 23, fontWeight: 'bold'}}>{item.name}</Text>
            <Text style={{fontSize: 13}}>{item.email}</Text>
          </View>
        </View>
      </TouchableRipple>
    );
  };

  useEffect(() => {
    if (users !== null) {
      const mapUsers = _.map(users, (value, uid) => {
        return {...value, uid};
      });
      //   const fakeContacts = [
      //     {
      //       email: 'newContacts@qcorner.com',
      //       name: 'New Contact',
      //       profileImage: 'https://cdn.onlinewebfonts.com/svg/img_162044.png',
      //       uid: '2asda20df8df889',
      //     },
      //     {
      //       email: 'newGroup@qcorner.com',
      //       name: 'New Group',
      //       profileImage:
      //         'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR__YCeBJfBkq2YAXzSFw7yCHru7zIYvO7sF9JmQPmYhGOEzUee',
      //       uid: '1asd90a8d90as8d',
      //     },
      //   ];

      //   mapUsers.push(fakeContacts[0]);
      //   mapUsers.push(fakeContacts[1]);
      //   mapUsers.reverse();
      setContacts(mapUsers);
    }
  }, [users]);

  return (
    <View>
      <GeneralStatusBar />
      <Header title="New Chat" navigation={navigation} />
      <FlatList
        data={contacts}
        renderItem={renderItem}
        keyExtractor={(item, index) => item.uid}
      />
    </View>
  );
};

export default NewChat;
