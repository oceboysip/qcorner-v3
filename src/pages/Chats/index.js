import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  FlatList,
  Image,
  View,
  StyleSheet,
  Text,
  ToastAndroid,
  Modal,
} from 'react-native';
import {TouchableRipple, IconButton, FAB} from 'react-native-paper';
import utf8 from 'utf8';
import base64 from 'base-64';
import database from '@react-native-firebase/database';
import useGetSessionLogin from '../../components/useGetSessionLogin';
import useGetChats from '../../components/useGetChats';
import TabPadding from '../../components/TabPadding';
import { useSelector } from 'react-redux';
import { Header } from '../../components';

const {width, height} = Dimensions.get('window');
const defaultAvatar = require('../../image/coklatprofil.png');

const Chats = ({navigation}) => {
  const {sessionLogin} = useGetSessionLogin();
  const {reducerSession} = useSelector(state => state); 
  const {loading, dataChats, refresh} = useGetChats(
    sessionLogin !== null ? reducerSession.email : null,
  );
  const [loadingDeleteChat, setLoadingDeleteChat] = useState(false);

  const onConfirmDelete = async (currentEmail, uid) => {
    setLoadingDeleteChat(true);
    try {
      await database()
        .ref(`/messages/${currentEmail}/${uid}`)
        .remove();
      await database()
        .ref(`/user_conversations/${currentEmail}/${uid}`)
        .remove();
      setLoadingDeleteChat(false);
      ToastAndroid.showWithGravity(
        'Berhasil hapus chat',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      refresh();
    } catch (err) {
      console.log('Error: ', err);
      setLoadingDeleteChat(false);
    }
  };

  const onDelete = uid => {
    // Convert to base64
    const current_email_bytes = utf8.encode(sessionLogin.data.user_data.email);
    const current_email_encode = base64.encode(current_email_bytes);
    Alert.alert(
      `Hapus chat`,
      'Anda yakin ingin menghapus chat ini ?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => onConfirmDelete(current_email_encode, uid)},
      ],
      {cancelable: false},
    );
  };

  const keyExtractor = item => item.uid;

  const renderHeader = () => {
    return <TabPadding />;
  };

  const renderEmptyComponent = () => {
    return (
      <View style={styles.containerEmptyComponent}>
        <Text>Chat tidak ada</Text>
      </View>
    );
  };

  const renderItem = ({item, index}) => {
    console.log('burung',item)
    return (
      <TouchableRipple
        onPress={() => navigation.navigate('ChatRoom', {...item})}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingHorizontal: 16,
            paddingVertical: 8,
            maxHeight: 100,
            minHeight: 80,
            borderBottomWidth: 1,
            borderColor: '#b7b7b7',
          }}>
          <Image
            source={
              item.profileImage ? {uri: item.profileImage} : defaultAvatar
            }
            style={{width: 50, height: 50, borderRadius: 50}}
          />
          <View style={{marginLeft: 15, flex: 1}}>
            <Text style={{fontSize: 23, fontWeight: 'bold'}}>{item.name}</Text>
            <Text numberOfLines={2} style={{fontSize: 13}}>
              {item.lastMessage}
            </Text>
          </View>
          <IconButton
            icon="delete"
            size={24}
            onPress={() => onDelete(item.uid)}
          />
        </View>
      </TouchableRipple>
    );
  };
  return (
    <View style={styles.container}>
      <Header
          onPress={() => navigation.goBack()}
          title={'QChat'}
        />
      {loading ? (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" />
        </View>
      ) : (
        <FlatList
          style={{flex: 1}}
          data={dataChats}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          ListHeaderComponent={renderHeader}
          ListEmptyComponent={renderEmptyComponent}
        />
      )}
      <Modal
        animationType="fade"
        transparent={true}
        visible={loadingDeleteChat}
        onRequestClose={() => {
          setLoadingDeleteChat(false);
        }}>
        <View style={styles.containerLoadingModal}>
          <View style={styles.modalView}>
            <ActivityIndicator />
          </View>
        </View>
      </Modal>
      
      <FAB
            style={styles.fab}
            icon="message-plus"
            color="white"
            onPress={() => navigation.navigate('NewChat')}
          />
    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fab: {
    position: 'absolute',
    bottom: 32,
    right: 24,
    backgroundColor: '#917369',
  },
  containerLoading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerEmptyComponent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerLoadingModal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    paddingHorizontal: 16,
  },
  modalView: {
    backgroundColor: '#FFF',
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    elevation: 4,
  },
});

export default Chats;
