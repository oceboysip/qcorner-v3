import {CommonActions} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  RefreshControl,
  StatusBar,
  Alert,
  Modal,
  ActivityIndicator,
  TextInput,
} from 'react-native';
import { showMessage, hideMessage } from "react-native-flash-message";
import {useDispatch, useSelector} from 'react-redux';
import {
  Logo_Barantan,
  simulasikartun,
  logoutimg,
  kontak_kami,
  dokumensaya,
  editprofil,
  BGprofilLIUKnogaris,
  berhasil,
  homecekstatus,
  homehistory,
  homekartun,
  homepanduan,
  homesimulasi,
  IconNotif,
  qna,
  background,
} from '../../assets';
import {Gap, Header} from '../../components';
import FitImage from 'react-native-fit-image';
import axios from 'axios';
import {Picker} from '@react-native-picker/picker';
const Simulasi = ({navigation, route}) => {
  const [loaded, setloaded] = useState(false);
  const [value, setValue] = useState(null);
  const [hitungan, setHitungan] = useState(0);
  const {reducerSession} = useSelector(state => state);
  const [mydata, setMyData] = useState('');
  const [data_permohonan_list, setDataPermohonan] = useState([{}]);
  const [data_komoditas, setDataKomoditas] = useState([]);
  const [data_komoditas_list_tipe, setDataKomoditasType] = useState([]);
  const [permohonan_selected, setPermohonanSelected] = useState('');

  const [komoditas_selected, setKomoditasSelected] = useState('');
  const [komoditas_selected_asli, setKomoditasSelectedAsli] = useState('');
  const [komoditas_tipe_selected, setKomoditasTipeSelected] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch();
  const [jenispermohonan, setJenisPermohonan] = useState([]);
  useEffect(() => {
    _jenisPermohonan();
    _jenisKomoditas();
  }, []);
  const _jenisPermohonan =  () => {
    const token = reducerSession.token;
    if (token == false) {
      alert(constant.pesan_relog);
      navigation.navigate('Login');
    }
    const permohonan_type = 'domestik';
    axios({
      method: 'get',
      url:
        'http://apiv2.bbkpsoetta.com/ppk/permohonan-type?permohonan_type=' +
        permohonan_type,
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
      .then(response => {
        // console.log('inipermohonan:', response.data.data);
        setDataPermohonan(response.data.data);
        // var status = response.data.code
        // if (status == 200){
        //   setDataPermohonan(response.data.data)
        // } else {
        //   alert('gagal ambil data jenis pemohon')
        // }
      })
      .catch(function (error) {
        alert('gagal ambil data jenis pemohon');
        console.log('yah error', +error);
        // throw error;
      });
  };

  const _jenisKomoditas =  () => {
    const token = reducerSession.token  

    const commodity_type = 'KT'
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/ppk/commodity-type?commodity_type='+commodity_type,
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      // console.log("==============DATA_JENIS_KOMODITAS=============")
      // console.log(response.data);
      setDataKomoditas(response.data.data)
      // var status = response.data.code
      // if (status == 200){
      //   this.setState({data_komoditas: response.data.data})
      // } else {
      //   alert('gagal ambil data komoditas')
      // }

    })
    .catch(function (error) {
      alert('gagal ambil data komoditas')
      console.log('yah error',+error);
      // throw error;
    });
  }
  const hitungTambah = () => {
    const awal = hitungan;
    const jml = awal + 1;
    setHitungan(jml);
  };

  const hitungKurang = () => {
    const awal = hitungan;
    const jml = awal - 1;
    if (awal > 0) {
      setHitungan(jml);
    }
  };
  const hitungSimulasi =  () => {
    setloaded(true);
    const token = reducerSession.token;
    const permohonan_type = permohonan_selected;
    const commodity_type = komoditas_selected_asli;
    const commodity_code = komoditas_tipe_selected;
    const volume = hitungan;

    const myurl ='http://apiv2.bbkpsoetta.com/ppk/price-simulation';
    // +permohonan_type+'&commodity_type=' +
    //   commodity_type +
    //   '&commodity_code=' +
    //   commodity_code +
    //   '&volume=' +
    //   volume;

    const body = {
      'permohonan_type':permohonan_type,
      'commodity_type':commodity_type,
      'commodity_code':commodity_code,
      'volume':volume,
    }
    console.log('myuri',body);
    // return
    axios({
      method: 'get',
      url:'http://apiv2.bbkpsoetta.com/ppk/price-simulation?',params:body,
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
      .then(response => {
       
        setloaded(false);
        setModalVisible(true)

        // console.log('==============DATA_LIST=============');
        // console.log(response.data);
        const status = response.data.code;
        // console.log('==============DATA_LIST_STATUS=============');

        console.log(status);

        if (status == 200) {
          setMyData(response.data.data);
        } else {
          alert('Hitungan tidak valid');
          setMyData(0);
        }
      })
      .catch(function (error) {
        setloaded(false);
        showMessage({
          message: "Mohon Di Lengkapi Datanya!!!",
          type: "danger",
        });
        // alert('Mohon Di Lengkapi Datanya ');
        console.log('yah wew', +error);
        throw error;
      });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      //fetchAllData();
      setRefreshing(false);
    }, 3000);
  };

  const _jenisKomoditasList  =  (typeKomoditas) => {
    // var token = this.state.mytoken._55
    const token = reducerSession.token;
     const type = typeKomoditas
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/ppk/commodity?type='+type,
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      console.log('listkomoditas',response.data);
      var status = response.data.code
      if (status == 200){
        setDataKomoditasType(response.data.data)
      } else {
        alert('gagal ambil komoditas list')
      }

    })
    .catch(function (error) {
      alert('gagal ambil data komoditas list')
      console.log('yah error',+error);
      // throw error;
    });
  }

  const komoditas = (value) => {
    setKomoditasSelected(value)
    const split_me = value.split('#')
    const value_komoditas = split_me[0]
    const id_komoditas = split_me[1] // ini yang dikirim ke api
    // alert(id_komoditas)
    _jenisKomoditasList(id_komoditas)
   setKomoditasSelectedAsli(value_komoditas)


    // alert(id_komoditas)
  }
  return (
    <ImageBackground
      source={background}
      style={{width: '100%', height: '100%'}}>
      <View style={{flex: 1}}>
        <Header
          onPress={() => navigation.pop()}
          title={'Simulasi Jasa Karantina'}
        />
        <ScrollView>
          <View style={{flex: 3, marginHorizontal: 30, marginTop: 70}}>
            <View style={{flex: 1, marginBottom: 10}}>
              <View style={styles.jusView}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1, alignContent: 'flex-start'}}>
                    <Picker
                      selectedValue={permohonan_selected}
                      style={{height: 35, width: '100%',paddingVertical:20}}
                      onValueChange={(value, itemIndex) =>
                        setPermohonanSelected(value)
                      }>
                     <Picker.Item label='Pilih Jenis Permohonan'  enabled={false} />
                     {/* <Picker.Item  label='Pilih Jenis Permohonan' value='' enabled='false'/> */}
                      {data_permohonan_list.map((value, index)=> {
                        return (
                          <Picker.Item
                            value={value.name}
                            label={value.name}
                            style={styles.pickerLabel}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </View>
                </View>
              </View>
                <Gap height={10}/>
              {/* batas */}

              <View style={styles.jusView}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1, alignContent: 'flex-start'}}>
                  <Picker
                      selectedValue={komoditas_selected}
                      style={{height: 30, width: '100%',paddingVertical:20}}
                      onValueChange={(value, itemIndex) =>
                        komoditas(value)
                      }>
                      
                     <Picker.Item label='Pilih Jenis Komoditas'  enabled={false}/>
                     {/* <Picker.Item  label='Pilih Jenis Permohonan' value='' enabled='false'/> */}
                      {data_komoditas.map((value, index)=> {
                        return (
                          <Picker.Item
                            value={value.code+'#'+value.id}
                            label={value.name}
                            style={styles.pickerLabel}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </View>
                </View>
              </View>
              <Gap height={10}/>

              {/* batas */}

              <View style={styles.jusView}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <View style={{flex: 1, alignContent: 'flex-start'}}>
                    <Picker
                    selectedValue={komoditas_tipe_selected}
                    style={{height: 30, width: "100%",paddingVertical:20}}
                    onValueChange={(value, index) =>
                      setKomoditasTipeSelected(value)
                      // this.komoditas(itemValue)
                    }>
                      <Picker.Item label='Pilih Jenis Komoditas'  enabled={false}/>
                      {
                        data_komoditas_list_tipe.map((value,index) =>{
                          return(
                          <Picker.Item  label={value.name} value={value.code} key={index}/>
                          );
                        })
                      }
                  </Picker>
                  </View>
                </View>
              </View>
                    <Gap height={10}/>
              {/* batas */}
              <View style={{flex: 1, marginLeft: 10, marginVertical: 10}}>
                <Text style={{fontSize: 15}}>Volume</Text>
              </View>

              <View style={{flex: 1, marginLeft: 10, flexDirection: 'row'}}>
                <TouchableOpacity onPress={hitungKurang}>
                  <View style={styles.bigCircle}>
                    <Text style={{color: 'white', fontSize: 20}}>-</Text>
                  </View>
                </TouchableOpacity>
                <View style={{marginHorizontal: 10}}>
                  <Text style={{fontSize: 30}}>{hitungan}</Text>
                </View>
                <TouchableOpacity onPress={hitungTambah}>
                  <View style={styles.bigCircle}>
                    <Text style={{color: 'white', fontSize: 20}}>+</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={{flex: 1, marginLeft: 10, marginVertical: 10}}>
                {loaded == true ? (
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        height: 40,
                        alignContent: 'center',
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingVertical: 5,
                        flex: 1,
                        marginTop: 8,
                        borderRadius: 200,
                        borderWidth: 0.5,
                        backgroundColor: 'white',
                      }}>
                      <ActivityIndicator
                        size="small"
                        color="#917369"
                        style={{marginRight: 10}}
                      />
                      <Text style={{color: '#917369', fontSize: 20}}>
                        Harap Tunggu ...
                      </Text>
                    </View>
                  </View>
                ) : (
                  <TouchableOpacity onPress={hitungSimulasi}>
                    <View style={{flex: 1}}>
                      <View
                        style={{
                          height: 40,
                          alignContent: 'center',
                          paddingVertical: 5,
                          alignItems: 'center',
                          flex: 1,
                          marginTop: 8,
                          borderRadius: 200,
                          backgroundColor: '#917369',
                        }}>
                        <Text style={{color: 'white', fontSize: 20}}>
                          Hitung Estimasi
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
          {/* pancingan */}
          <TouchableOpacity onPress={hitungSimulasi}>
            <View style={{flex: 1}}>
              <View>
                <Text style={{color: 'white', fontSize: 20}}></Text>
              </View>
            </View>
          </TouchableOpacity>
          {/* modal */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            // visible={true}
          >
            <View style={{flex: 1}}>
              <View style={{flex: 3}}></View>
              <View
                style={{
                  backgroundColor: 'white',
                  height: 150,
                  flex: 1,
                  paddingHorizontal: 20,
                }}>
                {/* content */}
                <ScrollView>
                  <View
                    style={{
                      flex: 1,
                      marginVertical: 10,
                      paddingBottom: 10,
                      borderBottomWidth: 0.7,
                      borderBottomColor: 'grey',
                      backgroundColor: '',
                    }}>
                    <Text>Hasil Estimasi</Text>
                  </View>
                  <View
                    style={{
                      flex: 4,
                      backgroundColor: '',
                      paddingBottom: 10,
                      borderBottomWidth: 0.7,
                      borderBottomColor: 'grey',
                    }}>
                    <Text style={{color: 'grey'}}>Biaya</Text>
                    <Text style={{fontWeight:'bold'}}>Rp. {mydata.price}-</Text>
                    <Text>
                      Biaya tanpa pemeriksaan laboratorium dan perlakuan mengacu
                      PP Np.35/2016{' '}
                    </Text>
                  </View>

                  <View style={{flex: 1, alignItems: 'center'}}>
                    <TouchableHighlight
                      onPress={() => {
                        setModalVisible(!modalVisible);
                      }}>
                      <Text>Tutup Modal</Text>
                    </TouchableHighlight>
                  </View>
                </ScrollView>

                {/* end content */}
              </View>
            </View>
          </Modal>

          <View style={{flex: 1, backgroundColor: ''}}>
            <FitImage
              resizeMode="cover"
              originalWidth={400}
              originalHeight={400}
              style={{marginTop: -50}}
              source={simulasikartun}
            />
          </View>

          <View
            style={{
              flex: 1,
              marginHorizontal: 20,
              paddingHorizontal: 20,
              display: 'none',
              paddingVertical: 20,
              borderColor: 'white',
              borderRadius: 20,
              marginBottom: 30,
              marginTop: -100,
              shadowOpacity: 0.8,
              elevation: 10,
              backgroundColor: 'white',
              borderBottomColor: 'white',
              borderWidth: 1,
            }}>
            <View
              style={{
                flex: 1,
                borderBottomWidth: 1,
                borderBottomColor: 'grey',
                marginBottom: 5,
              }}>
              <Text style={{fontWeight: 'bold', fontSize: 20}}>
                Hasil Estimasi
              </Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={{color: 'grey'}}>Biaya</Text>
              {/* <Text style={{fontWeight:'bold'}}>Rp. {this.state.mydata.price}-</Text> */}
              <Text>
                Biaya tanpa pemeriksaan laboratorium dan perlakuan mengacu PP
                Np.35/2016{' '}
              </Text>
            </View>
          </View>
        </ScrollView>
        {/* <FooterNavBar /> */}
      </View>
    </ImageBackground>
  );
};

export default Simulasi;

const styles = StyleSheet.create({
  jusView: {
    shadowOffset: {width: 10, height: 10},
    shadowColor: 'black',
    shadowOpacity: 0.8,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingVertical: 5,
    borderRadius: 25,
    borderWidth: 0,
  },
  bigCircle: {
    height: 40,
    width: 40,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#917369',
    backgroundColor: '#917369',
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
  ico: {
    flex: 1,
    alignContent: 'center',
  },
  textTitleView: {
    flex: 10,
    marginLeft: 10,
    alignContent: 'center',
  },
  textTitle: {
    fontSize: 20,
  },
  BDocument: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 10,
    flexDirection: 'row',
    borderColor: 'grey',
    borderBottomWidth: 0.9,
  },
  Bprofile: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderColor: 'grey',
    alignContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.9,
    borderLeftWidth: 0.9,
    borderRightWidth: 0.9,
    borderTopWidth: 0.9,
    shadowColor: 'black',
    backgroundColor: '',
    borderWidth: 0,
    paddingLeft: 15,
    paddingVertical: 10,
    height: 50,
  },
  pickerLabel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
});
