import { StyleSheet, Text, View,TouchableOpacity,ImageBackground,StatusBar,ScrollView,Image} from 'react-native'
import React,{useState,useEffect} from 'react'
import { BGprofilLIUKnogaris,edit,IconEdit,Logo_Barantan } from '../../assets'
import { Header } from '../../components'
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';

const Profile1 = ({navigation,route,params}) => {


  const [loaded,setloaded]=useState(false);
 const {reducerSession} = useSelector(state => state); 
 const [fcm,setfcm]=useState('');
 const [myname,setmyname]=useState(reducerSession.name);
 const [isMenuVisible,setisMenuVisible]=useState('');
 const [mydata,setMyData] = useState('');
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch()

  useEffect(() => {
 _getProfil();
  }, []);

  const _getProfil =  () => {
    const token = reducerSession.token;
    console.log('tokenprofile',token)
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/profile',
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      console.log('dataprofile:',response);
      setMyData(response.data.data)
      var status = response.data.code
      // if (status == 200){
      //   setMyData(response.data.data)
      // } else {
      //   alert('gagal ambil data profil')
      // }

    })
    .catch(function (error) {
      alert('gagal ambil data profil',+error)
      console.log('error',+error);
      // throw error;
    });
  }
  return (
    <ImageBackground source={BGprofilLIUKnogaris} style={{width: '100%', height: '100%'}}>      
    <StatusBar 
      barStyle="light-content"
      hidden={false}
      backgroundColor='#917369'
    />
    <View style={{ flex: 1}}>
        <View style={{flex:1,backgroundColor:''}}>
        <Header onPress={() => navigation.pop()} title={'Profile'} />
            <ScrollView>
              <View style={{style:1,marginVertical:10,alignItems:'center'}}>
                <Image style={{width:130,height:130}} source={Logo_Barantan}></Image>
                   
              </View>
              {/* <View style={{flex:1,alignItems:'center'}}>
                  <Text style={{fontWeight:'bold',fontSize:25}}>{this.state.mydata.name}</Text>
                  <View style={{marginTop:10,flex:1,flexDirection:'row',alignContent:'center',alignItems:'center'}}>
                    <Image style={{width:30,height:30}} source={require('../image/location.png')}></Image>
                    <Text style={{fontSize:18}}>
                      {this.state.mydata.country}
                    </Text>
                  </View>
              </View> */}
              <View style={{backgroundColor:'white',paddingHorizontal:10,borderColor:"white",borderWidth:1,borderRadius:25,borderBottomColor:"black"}}>          
                <View style={{paddingVertical:5,marginVertical:5,marginBottom:29}}>
                    {/* <HeaderDetailEdit GoTo='ProfilEdit'/> */}
                    <TouchableOpacity onPress={() => navigation.navigate('ProfileEdit',{data:mydata,type:'Home'})} >
                        <View style={{alignItems:'flex-end',flexDirection:'row', paddingRight:10}}>
                            <IconEdit/>
                            <Text style={styles.txt_head}>   Ganti Password</Text>
                          
                        </View>
                    </TouchableOpacity>
                </View>
                
                    <Text style={styles.txt_head}>Nama</Text>
                    <Text style={{marginBottom:20,fontSize:16}}>{mydata.name}</Text>
                    <Text style={styles.txt_head}>Tempat, tanggal lahir</Text>
                    <Text style={{marginBottom:20,fontSize:16}}>{mydata.name}</Text>
                    <Text style={styles.txt_head}>Email</Text>
                    <Text style={{marginBottom:20,fontSize:16}}>{mydata.email}</Text>
                    <Text style={styles.txt_head}>No. Telpon</Text>
                    <Text style={{marginBottom:20,fontSize:16}}>{mydata.telepon}</Text>
                    <Text style={styles.txt_head}>Alamat Lengkap</Text>
                    <Text style={{marginBottom:20,fontSize:16}}>{mydata.country}</Text>
                    <Text style={styles.txt_head}>Kota</Text>
                    <Text style={{marginBottom:20,fontSize:16}}>{mydata.country}</Text>
                    <Text style={styles.txt_head}>Kode Pos</Text>
                    <Text style={{marginBottom:20,fontSize:16}}>{mydata.country}</Text>
           </View>
           </ScrollView>
        </View>
    </View>
  </ImageBackground>
  )
}

export default Profile1

const styles = StyleSheet.create({})