import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {
    IconPaketData,
    IconPLN,
    IconPulsa,
} from '../../assets';
import {Gap, Header} from '../../components';
import {colors} from '../../utils';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();
const { Navigator, Screen } = Tab;

const TabMenu = ({ navigation, route }) => {
    return (
        <View style={{padding: 16}}>
            {[
                { name: 'Pulsa', screen: 'ChoosePulsa', imageAsset: <IconPulsa width={30} height={30} />, },
                { name: 'Paket Data', screen: '', imageAsset: <IconPaketData width={30} height={30} />, },
                { name: 'PLN', screen: '', imageAsset: <IconPLN width={30} height={30} />, },
            ].map((item, idx) => {
                return (
                    <TouchableOpacity
                        key={idx}
                        activeOpacity={0.7}
                        onPress={() => {
                            if (item.screen) navigation.navigate(item.screen);
                        }}
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 24,
                            justifyContent: 'space-between',
                        }}
                    >
                        <View style={{flexDirection: 'row', alignItems: 'center',}}>
                            {item.imageAsset}
                            <Gap width={12} />
                            <Text style={{fontWeight: '600'}}>{item.name}</Text>
                        </View>
                        <FontAwesome
                            name='angle-right'
                            color={colors.text.label}
                            size={20}
                        />
                    </TouchableOpacity>
                )
            })}
        </View>
    )
}

const TabFavorite = ({ navigation, route }) => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Belum ada data</Text>
        </View>
    )
}

const LainnyaScreen = ({ navigation, route }) => {
    return (
        <View style={{ flex: 1 }}>
            <Header title="Lainnya" onPress={() => navigation.goBack()} />
            <Navigator
                initialRouteName='Menu'
            >
                <Screen
                    name="Menu"
                    component={TabMenu}
                    options={{
                        tabBarActiveTintColor: colors.primary,
                        tabBarInactiveTintColor: colors.text.label,
                        tabBarLabelStyle: { textTransform: 'none', fontSize: 15, fontWeight: '600' },
                        tabBarIndicatorStyle: { backgroundColor: colors.primary },
                    }}
                />
                <Screen
                    name="Favorit"
                    component={TabFavorite}
                    options={{
                        tabBarActiveTintColor: colors.primary,
                        tabBarInactiveTintColor: colors.text.label,
                        tabBarLabelStyle: { textTransform: 'none', fontSize: 16, fontWeight: '600' },
                        tabBarIndicatorStyle: { backgroundColor: colors.primary },
                    }}
                />
            </Navigator>
        </View>
    );
};

export default LainnyaScreen;