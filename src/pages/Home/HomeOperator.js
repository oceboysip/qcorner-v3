import React, { useEffect, useState ,isFocussed} from 'react';
import {
  Image, ImageBackground,
  ScrollView, StyleSheet, Text, TouchableHighlight, View,
  TouchableOpacity,
  TouchableNativeFeedback,Alert,
  RefreshControl,Dimensions,useWindowDimensions,
} from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import {
  background, berhasil, homecekstatus, homehistory, homekartun, homepanduan, homesimulasi, IconNotif, pengajuan_ppk, qna
,icon_bell_coklat} from '../../assets';
import { useForm } from '../../utils';
import axios from 'axios';
import {FAB} from 'react-native-paper';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { showMessage, hideMessage } from "react-native-flash-message";
import { getAPI,postAPI } from '../../utils/httpService';
import { Notifikasi } from './notifications.json';
// import FooterNavBar from "./template/FooterNavBar";
//import StatusBarCustom from "./template/StatusBarCustom";
import LottieView from 'lottie-react-native';


const HomeOperator = ({navigation, route}) => {

  const { width, height } = useWindowDimensions();
  const {reducerSession} = useSelector(state => state); 
  const [myname,setmyname]=useState(reducerSession.name);
  const [isMenuVisible,setIsMenuVisible]=useState(false);
  const [data, setData] = useState({});
  const [welcomes, setWelcomes] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch()

  useEffect(() => {
    onRefresh();
  }, []);


  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      welcome();
      setRefreshing(false);
    }, 4000);
   

   
  };

  const welcome =()=>{
    const token = reducerSession.token;
    if (token == false) {
      alert("sesi anda telah berakhir silahkan lakukan login kembali");
      navigation.navigate("Login");
    }
    getAPI('welcome').then((response) => {
      console.log('welcome',response)
      setWelcomes(true);
    });
  }
  const cekfeedbacknew =  () => {
    const token = reducerSession.token;
    if (token == false) {
      alert("sesi anda telah berakhir silahkan lakukan login kembali");
      navigation.navigate("Login");
    }
    getAPI('quisioner/norespon').then((response) => {
     // console.log('wew',response)
      if (response.code === 200){
          setIsMenuVisible({isMenuVisible:response.status})
      } else {
        showMessage({
          message: "Quisioner telah terisi",
          type: "info",
        });
      }
    });
  }

  const Alertnya=(idnya)=>{
    
    Alert.alert(
      "Feedback",
      "Berikan Kami Feedback?",
      [
        {
          text:"Yes",
          style:"default",
          onPress: ()=>( konfirmasifeedback(1,idnya) ),             
         },
         {
           text:"No",
           style:"destructive",
           onPress: ()=>( konfirmasifeedback(2,idnya) ),         
         },
      ],
      {cancelable:false}
    );
  }
const konfirmasifeedback = (paham,donk) =>{
  const token = reducerSession.token;
  if (token == false) {
    alert("sesi anda telah berakhir silahkan lakukan login kembali");
    navigation.navigate("Login");
  }
  const body={
    'status':paham,
    'id_status_quisioner':donk
  };
  
  postAPI('quisioner/confrim',body).then((response) => {
    console.log('hasil nya')
    if (response.data.status === 0){
      navigation.navigate('Feedback');
      //navigation.navigate('BeforeFeedback')
    } else if(response.data.status === 2) {
      showMessage({
        message: "Terima kasih",
        type: "none",
      });
    }else{
      showMessage({
        message: "Terima kasih",
        type: "info",
      });
    }
  });

}
  const cekquesioner =  () => {
    const token = reducerSession.token;
    if (token == false) {
      alert("sesi anda telah berakhir silahkan lakukan login kembali");
      navigation.navigate("Login");
    }
    getAPI('quisioner/check').then((response) => {
      //console.log(response.data.id_status_quisioner)
      if (response.code === 200){
        setIsMenuVisible(true)
        Alertnya(response.data.id_status_quisioner);
      } else {
        showMessage({
          message: "Quisioner telah terisi",
          type: "info",
        });
      }
    });
  }

 
  return (
    <View>
    <ImageBackground
    source={background}
    style={{ width: "100%", height: "100%" }}
  >
   {/* <StatusBarCustom /> */}
   <View style={{ flex: 1 }}>
          <View style={{ flex: 1}}>

          <View>
      
          <View style={{alignItems:'flex-end',paddingTop:20,paddingRight:20}}>
          <TouchableNativeFeedback  onPress={()=> navigation.navigate('Notifikasi')}>
            <Image source={icon_bell_coklat} style={{width:20,height:25}}/>
            </TouchableNativeFeedback>
          </View>
      
          </View>

            <ScrollView 
             refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            style={{ flex: 1 }}>
              <ImageBackground
                source={homekartun}
                style={{ flex: 1, width: "100%", height: "100%" }}
              >
                 <View style={{ flex: 1,alignItems: "flex-start",marginTop: 20,marginLeft: 20,marginBottom:"50%",backgroundColor: "",}} >
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: "bold",
                      color: "#917369",
                    }}
                  >
                    Hello, {myname}
                  </Text>
                  <Text style={{ fontSize: 15, fontWeight: "bold" }}>
                    Apa yang dapat kami Bantu?
                  </Text>
                </View>
                <View style={{
                  flex: 1,
                  flexDirection: "row",
                  paddingTop: 50,
                  paddingBottom: 0,
                  margin: 25,
                }}>
                <TouchableNativeFeedback  onPress={()=> navigation.navigate('StatusPermohonanOperator')}  style={styles.imageMenu} >
                  <View style={styles.rounded} underlayColor="#fff" >
                    <Image
                      style={styles.imgDashboard}
                      source={homecekstatus}
                    />
                    <Text style={{ fontSize: 10 , textAlign: 'center'}}>
                      Permohonan</Text>
                </View>
                  
                </TouchableNativeFeedback>
                <TouchableNativeFeedback onPress={()=> navigation.navigate('Simulasi')} style={styles.imageMenu} >
                   <View style={styles.rounded} underlayColor="#fff" >
                    <Image
                      style={styles.imgDashboard}
                      source={homesimulasi}
                    />
                      <Text style={{ fontSize: 10 , textAlign: 'center'}}>
                       Simulasi</Text>
                  </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback onPress={()=> navigation.navigate('FilePendukung')} style={styles.imageMenu} >
                   <View style={styles.rounded} underlayColor="#fff" >
                    <Image
                      style={styles.imgDashboard}
                      source={berhasil}
                    />
                      <Text style={{ fontSize: 10 , textAlign: 'center'}}>
                       File Pendukung</Text>
                  </View>
                </TouchableNativeFeedback>
                </View>
                </ImageBackground>
                </ScrollView>
                <View style={{justifyContent:'space-between'}}>

                {isMenuVisible && (
                  <TouchableOpacity style={styles.rounded2}  onPress={() => navigation.navigate('BeforeFeedback')}>
                  <LottieView height={70} source={require('./notifications.json')} autoPlay loop />
                </TouchableOpacity> 
                
                )}
               
            </View>
            </View>
            </View>
            <FAB
            style={styles.fab}
            icon="chat"
            onPress={() => navigation.navigate('Chats')}
          />
    </ImageBackground>
  </View>
  );
};

export default HomeOperator;
// const {width, height} = Dimensions.get('screen');

const styles = StyleSheet.create({
  imgDashboard: {
    width: 40,
    height: 40,
    marginBottom:5
  },
  imgDashboard2: {
    width: 50,
    height: 50,
  },
  fab:{
    position: 'absolute',
    margin: 20,
    right: 0,
    bottom: 10,
    backgroundColor: '#917369',
  },
  fab2:{
    position: 'absolute',
    margin: 20,
    marginRight:-40,
    bottom: 10,
    backgroundColor: '#917369',
  },
  imageMenu: {
    flex: 1,
  alignItems: "center",
  justifyContent: "center",
},
  rounded: {
    // marginHorizontal:20,
    // marginTop:10,
    shadowOffset: { width: 10, height: 10 },
    shadowColor: "black",
    shadowOpacity: 1,
    elevation: 10,
    maxWidth:88,
    maxHeight:120,
    paddingTop: 10,
    flex:1,
    alignContent:'center',
    alignItems:'center',
    marginLeft:15,
    marginRight:15,
    paddingBottom: 10,
    backgroundColor: "white",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "white",
  },

  rounded2: {
    // marginHorizontal:20,
    // marginTop:10,
    
    maxWidth:300,
    maxHeight:200,
    paddingTop: 20,
    flex:1,
    alignContent:'center',
    alignItems:'center',
    marginLeft:0,
    marginRight:'70%',
    paddingBottom: 90,
  },
  
  
});

