import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  TouchableHighlight,
  Image,
  TouchableOpacity,
  Modal,
  TextInput,
  ActivityIndicator,
} from "react-native";
import React, { useState, useEffect } from "react";
import { background, dua, pengajuansertifikat, IconCamera } from "../../assets";
import { Header, Gap } from "../../components";
import FitImage from "react-native-fit-image";
import { useDispatch, useSelector } from "react-redux";
import { launchImageLibrary, launchCamera } from "react-native-image-picker";
import axios from "axios";
import RNFetchBlob from 'rn-fetch-blob';

const PengajuanVerifikasi = ({ navigation, route, params }) => {
  const [uri_path, setUriPath] = useState(false);
  const [file, setFile] = useState(null);
  console.log("uripath:", uri_path);
  console.log("iniparams:", route.params);
  const [keterangan, setKeterangan] = useState("");
  const { no_aju } = route.params;
  const [loaded, setLoaded] = useState(false);
  const [data, setData] = useState({});
  const { reducerSession } = useSelector((state) => state);
  const [modalVisible, setModalVisible] = useState(false);
  const [disableButton, setDisableButton] = useState(false);
  const [loading, setLoading] = useState(true);
  const [image, setImage] = useState(null);

  useEffect(() => {
    _getData();
    listData();
    cariDetailAju();
    requestCamera();
    console.log("noajuhhhhhh:", no_aju);
  }, []);

  const listData = (no_aju) => {
    const token = reducerSession.token;
    console.log("tokenss:", token);
    if (token == false) {
      alert(constant.pesan_relog);
      navigation.navigate("Login");
    }
    //console.log("tokennya:"+token)
    axios({
      method: "get",
      url: "http://apiv2.bbkpsoetta.com/applicant/inquiry-list-new",
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        const status = response.data.code;
        if (status == 200) {
          setData(response.data.data);
        } else {
          // alert('no')
        }
      })
      .catch(function (error) {
        console.log("yah error token keqnya", +error);
      });
  };
  const clickSetuju = () => {
    _postData("SETUJU");
  };

  const clickModal = () => {
    setModalVisible(true);
  };

  const clickPerbaiki = () => {
    _postData("PERBAIKI");
  };

  const _postData = (param) => {
    setLoaded(true);
    const token = reducerSession.token;
    const ket = keterangan;
    if (token == false) {
      alert(constant.pesan_relog);
      navigation.navigate("Login");
    }

    axios
      .post(
        "http://apiv2.bbkpsoetta.com/applicant/inquiry/" +
          no_aju +
          "/draft-certificate-verification",
        {
          type: param,
          notes: ket,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
      .then((response) => {
        // var resp = JSON.stringify(response.data)
        // console.log("respon: "+resp)
        console.log("response:", response);
        alert(
          "Draft permohonan telah berhasil di submit, harap tunggu proses selanjutnya"
        );
        setLoaded(false);
        navigation.navigate("StatusPermohonan");
      })
      .catch(
        function (error) {
          setLoaded(false);
          alert("Submit error ,Silahkan hub administrator: " + error);
          throw error;
          console.log(error);
        }.bind(this)
      );
  };

  // cek perizinan kamera
  async function requestCamera() {
    let valid = false;

    if (Platform.OS === "ios") {
      valid = true;
    } else {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA
      );

      valid = granted === PermissionsAndroid.RESULTS.GRANTED;
    }

    if (valid) {
      imagePicker("camera");
    } else {
      showMessage({
        message: "Izin kamera di tolak",
      });
    }
  }

  async function imagePicker(type) {
    setDisableButton(true);
    setTimeout(() => setDisableButton(false), 2000); // menghindari crash
    const Picker = type == "galeri" ? launchImageLibrary : launchCamera;
    const { assets } = await Picker({
      mediaType: "photo",
    });

    if (assets) {
      setImage(assets[0].uri);
      setForm({
        ...form,
        foto_rek: {
          name: assets[0].fileName,
          type: assets[0].type,
          uri: assets[0].uri,
        },
      });
    }
  }

  const _getData = async () => {
    var dataState = await cariDetailAju(no_aju);
    console.log("datasea:", dataState);
    var data = JSON.stringify(dataState);
    console.log("dataku_str: " + data);
    var data_parse = JSON.parse(data);
    var dataku = data_parse.data.current_requirement;
    var data_doc = JSON.stringify(dataku);
    console.log("dataku_current: " + data_doc);

    if (data_doc != "null") {
      var data_doc_2 = JSON.parse(data_doc);
      var data_doc_uri = data_doc_2.file_path;
      console.log("dataku_oy: " + data_doc_2);
      if (data_doc_uri) {
        setUriPath(data_doc_uri);
      }
    } else {
      alert("Dokument tidak ditemukan silahkan hubungi administrator");
    }
  };
  const cariDetailAju = () => {
    const token = reducerSession.token;
    console.log("tokenss:", token);
    if (token == false) {
      alert(constant.pesan_relog);
      navigation.navigate("Login");
    }
    //console.log("tokennya:"+token)
    axios({
      method: "get",
      url: "http://apiv2.bbkpsoetta.com/applicant/inquiry/" + no_aju,
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        // setData(+JSON.stringify(response.data);
        // console.log("==============DATA_LIST=============")

        // console.log("the size => "+JSON.stringify(response.data.data));
        const status = response.data.code;
        const data = response.data.data;
        console.log("inidata:", data);
        setUriPath(response.data.data.current_requirement.file_path);
        console.log("uri:", response.data.data.current_requirement.file_path);

        // if (status == 200){
        //  setData(response.data.data);
        //  setUriPath(response.data.current_requirement.file_path);
        //  console.log

        // } else {
        //   // alert('no')
        // }
      })
      .catch(function (error) {
        console.log("yah error token keqnya", +error);
      });
  };

  const download=(judul,url)=>{
    
    const android = RNFetchBlob.android
 
    RNFetchBlob.config({
        addAndroidDownloads : {
          useDownloadManager : true,
          title : 'Qcorner Download',
          description : 'Download file'+judul,
        
          mediaScannable : true,
          notification : true,
        }
      })
      .fetch('GET', url)
      .then((res) => {
          android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
      })
   
  }
  const GetUrl=(param)=>{
    const url=param;
    console.log(url);
    return url;
  }
  return (
    <ImageBackground
      source={background}
      style={{ width: "100%", height: "100%" }}
    >
      <Header onPress={() => navigation.pop()} title={"Pengajuan Verifikasi"} />
      <View style={{ flex: 1 }}>
        {/* <HeaderLengkap is_GoTo_hidden={true} is_textHeader_hidden={true}/> */}
        <ScrollView>
          <View style={{ flex: 1, backgroundColor: "", marginBottom: 20 }}>
            <FitImage
              resizeMode="cover"
              originalWidth={400}
              originalHeight={50}
              source={dua}
            />
          </View>
          <View style={{ flex: 1, alignItems: "center", marginTop: 0 }}>
            <Text
              style={{ fontSize: 30, color: "#917369", fontWeight: "bold" }}
            >
              Verifikasi Draft{" "}
            </Text>
            <Text
              style={{ fontSize: 30, color: "#917369", fontWeight: "bold" }}
            >
              Permohonan
            </Text>
            <Text style={{ fontSize: 15, marginTop: 10,alignSelf:'center',alignContent:'center',flex:1,textAlign:'center' }}>
              Mohon periksa kembali Draft Sertifikat
              dibawah ini, pastikan dokumen - dokumen Anda sudah benar sebelum melanjutkan
              ke proses berikutnya
            </Text>
            {/* <Text style={{ fontSize: 15,marginTop:10,justifyContent:'center',alignItems:'center' }}>
              Mohon periksa kembali Draft Sertifikat
              dibawah ini, pastikan dokumen - dokumen Anda sudah benar sebelum melanjutkan
              ke proses berikutnya
            </Text> */}
            {/* <Text style={{ fontSize: 15 }}>
              sudah benar sebelum melanjutkan
            </Text> */}
            {/* <Text style={{ fontSize: 15 }}>ke proses berikutnya</Text> */}
          </View>
          <View style={{ flex: 3, marginHorizontal: 30, marginTop: 20 }}>
            {uri_path && (
              <View>
                <TouchableHighlight
                  onPress={() =>
                    navigation.navigate("PdfExample", {
                      uri: uri_path,
                      link_back: "PengajuanVerifikasi",
                      text_header: "Dokumen",
                    })
                  }
                >
                  <View style={styles.shadowView}>
                    <Image
                      style={{
                        height: 50,
                        width: 300,
                        justifyContent: "center",
                        alignSelf: "center",
                      }}
                      source={pengajuansertifikat}
                    />
                  </View>
                </TouchableHighlight>
                <View style={{ flex: 1, flexDirection: "row", marginTop: 20 }}>
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: "",
                      paddingHorizontal: 10,
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        download(
                          "Draft Sertifikat",
                          GetUrl(uri_path.toString())
                        )
                      }
                    >
                      {/* <View style={{backgroundColor:'#917369',width:"100%",height:40,
                                  alignContent:'center',justifyContent:'center',alignItems:'center',
                                  borderRadius:25,borderColor:"grey",borderWidth:1}}>          
                        <View><Text style={{color:'white',fontSize:12}}>Download</Text></View>
                    </View> */}
                    </TouchableOpacity>

                    {/* <View
                      style={{
                        flex: 1,
                        backgroundColor: "",
                        paddingHorizontal: 40,
                      }}
                    >
                      <TouchableOpacity onPress={clickSetuju}>
                        <View
                          style={{
                            backgroundColor: "#917369",
                            width: "100%",
                            height: 40,
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 25,
                            borderColor: "grey",
                            borderWidth: 1,
                          }}
                        >
                          <View>
                            <Text style={{ color: "white", fontSize: 12 }}>
                              Setuju
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View> */}
                    <Gap height={10} />
                    <View
                      style={{
                        flex: 1,
                        backgroundColor: "",
                        paddingHorizontal: 40,
                      }}
                    >
                      <TouchableOpacity 
                       onPress={() => download('QcornerFileDownload',GetUrl(uri_path.toString()))}
                      >
                        <View
                          style={{
                            backgroundColor: "white",
                            width: "100%",
                            height: 40,
                            alignContent: "center",
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 25,
                            borderColor: "grey",
                            borderWidth: 1,
                          }}
                        >
                          <View>
                            <Text style={{ color: "#917369", fontSize: 12 }}>
                              Download
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                    <Gap height={10} />
                    {/* <View
                      style={{
                        flex: 1,
                        backgroundColor: "",
                        paddingHorizontal: 40,
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => imagePicker("galeri")}
                        disabled={disableButton}
                      >
                        <View
                          style={{
                            backgroundColor: "#917369",
                            width: "100%",
                            height: 40,
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 25,
                            borderColor: "grey",
                            borderWidth: 1,
                          }}
                        >
                          <View>
                            <Text style={{ color: "white", fontSize: 12 }}>
                              Upload Draft
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View> */}

                    {/* modal */}
                    <Modal
                      animationType="slide"
                      transparent={true}
                      visible={modalVisible}
                      // visible={true}
                    >
                      <View style={{ flex: 1 }}>
                        <View style={{ flex: 3 }}></View>
                        <View
                          style={{
                            backgroundColor: "white",
                            height: 150,
                            flex: 1,
                            paddingHorizontal: 10,
                          }}
                        >
                          {/* content */}
                          <ScrollView>
                            <View style={{ flex: 1, marginVertical: 10 }}>
                              <Text>
                                Silahkan masukkan keterangan perbaikan
                              </Text>
                            </View>
                            <View style={{ flex: 2 }}>
                              <View
                                style={{
                                  backgroundColor: "white",
                                  paddingLeft: 10,
                                  marginBottom: 10,
                                  borderRadius: 25,
                                  borderColor: "grey",
                                  borderWidth: 1,
                                }}
                              >
                                <TextInput
                                  style={{ height: 40, borderColor: "#FFFFFF" }}
                                  onChangeText={(keterangan) =>
                                    setKeterangan(keterangan)
                                  }
                                ></TextInput>
                              </View>
                            </View>
                            <View style={{ flex: 2 }}>
                              <View
                                style={{
                                  flex: 1,
                                  backgroundColor: "",
                                  paddingHorizontal: 10,
                                }}
                              >
                                <TouchableOpacity onPress={clickPerbaiki}>
                                  <View
                                    style={{
                                      backgroundColor: "#917369",
                                      width: "100%",
                                      height: 40,
                                      alignContent: "center",
                                      justifyContent: "center",
                                      alignItems: "center",
                                      borderRadius: 25,
                                      borderColor: "grey",
                                      borderWidth: 1,
                                    }}
                                  >
                                    <View>
                                      <Text
                                        style={{ color: "white", fontSize: 20 }}
                                      >
                                        Perbaiki
                                      </Text>
                                    </View>
                                  </View>
                                </TouchableOpacity>
                              </View>
                            </View>
                            <View style={{ flex: 1, alignItems: "center" }}>
                              <TouchableHighlight
                                onPress={() => {
                                  setModalVisible(!modalVisible);
                                }}
                              >
                                <Text>Tutup Modal</Text>
                              </TouchableHighlight>
                            </View>
                          </ScrollView>

                          {/* end content */}
                        </View>
                      </View>
                    </Modal>

                    {/* end modal */}
                  </View>
                </View>
                {loaded && (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      marginTop: 20,
                      alignSelf: "center",
                    }}
                  >
                    <ActivityIndicator
                      size="large"
                      color="#917369"
                      style={{ marginRight: 10 }}
                    />
                    <View>
                      <Text style={{ color: "#917369", fontSize: 20 }}>
                        Harap Tunggu ...
                      </Text>
                    </View>
                  </View>
                )}
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    </ImageBackground>
  );
};

export default PengajuanVerifikasi;

const styles = StyleSheet.create({
  buttonSimpan: {
    backgroundColor: "red",
    width: "100%",
    borderRadius: 3,
    paddingVertical: 12,
  },
  iconCamera: {
    width: 20,
    height: 15,
    tintColor: "white",
    marginHorizontal: -5,
    marginVertical: 3,
  },
  image: {
    height: 150,
    borderRadius: 5,
  },
  photoContainer: {
    height: 150,
    backgroundColor: "white",
    marginTop: 10,
    borderRadius: 5,
    elevation: 3,
  },
  textUnpick: {
    flex: 1,
  },
  buttonPhoto: {
    backgroundColor: "red",
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 3,
  },
  viewPhoto: {
    backgroundColor: "#CACACA",
    padding: 10,
    borderRadius: 5,
  },
  textInput: {
    fontFamily: "Poppins-Regular",
    fontSize: 15,
    color: "white",
  },
  bankPicker: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 50,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginHorizontal: 2.5,
    paddingHorizontal: 5,
  },
  picker: {
    width: "100%",
    height: "100%",
  },
});
