import { StyleSheet, Text, View ,ImageBackground,ScrollView,TouchableOpacity,Image,StatusBar,TouchableHighlight} from 'react-native'
import React ,{useEffect,useState} from 'react'
import { background, sertifikat,pengajuanpreviewsertifikat,esertifikatbwh } from '../../assets'
import FitImage from 'react-native-fit-image';
import { Header } from '../../components';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';

const Esertifikat = ({navigation,route}) => {
  const {no_aju}= route.params
  const [uri_path, setUriPath] = useState(false);
  const { reducerSession } = useSelector((state) => state);

  useEffect(() => {
    cariDetailAju();
  }, []);

  const cariDetailAju = () => {
    const token = reducerSession.token;
    console.log("tokenss:", token);
    if (token == false) {
      alert(constant.pesan_relog);
      navigation.navigate("Login");
    }
    //console.log("tokennya:"+token)
    axios({
      method: "get",
      url: "http://apiv2.bbkpsoetta.com/applicant/inquiry/" + no_aju,
      headers: {
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => {
        // setData(+JSON.stringify(response.data);
        // console.log("==============DATA_LIST=============")

        // console.log("the size => "+JSON.stringify(response.data.data));
        const status = response.data.code;
        const data = response.data.data;
        console.log("inidata:", data);
        setUriPath(response.data.data.current_requirement.file_path);
        console.log("uri:", response.data.data.current_requirement.file_path);

        // if (status == 200){
        //  setData(response.data.data);
        //  setUriPath(response.data.current_requirement.file_path);
        //  console.log

        // } else {
        //   // alert('no')
        // }
      })
      .catch(function (error) {
        console.log("yah error token keqnya", +error);
      });
  };
  return (
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>      
    <StatusBar 
      barStyle="light-content"
      hidden={false}
      backgroundColor='#F9EACD'
    />
    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} title={'Preview Certifikat'} />
            <ScrollView style={{flex:1,marginTop:30}}>
            <View style={{flex:1,alignItems:'flex-start',marginLeft:30}}> 
            <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Serah Terima</Text>
            <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Dokumen</Text>
            </View> 
            <View style={styles.vRadius}>
                <View style={styles.ico}>
                    <FitImage
                        indicator={true}
                        indicatorColor="white"
                        resizeMode="cover"
                        source={sertifikat}
                        style={{height:150,width:150}}
                    />
                </View>
                <TouchableHighlight onPress={() => navigation.navigate('PdfExample',{uri:uri_path,link_back:'Home',text_header:'Dokumen'})} >
                
                  <View style={styles.shadowView}>          
                      <Image
                      style={{height:50,width:310}}
                      source={pengajuanpreviewsertifikat}
                      />
                  </View>
                </TouchableHighlight>
                <View style={styles.ico}>
                    <Text style={{fontSize:14,color:'black'}}>Mohon selesaikan penandatanganan Sertifikat</Text>
                    <Text style={{fontSize:14,color:'black'}}>dengan mendatangi langsung ke counter</Text>
                    <Text style={{fontSize:14,color:'black'}}>layanan prioritas dengan membawa bukti</Text>
                    <Text style={{fontSize:14,color:'black'}}>pembayaran serta dokumen karantina terhadap</Text>
                    <Text style={{fontSize:14,color:'black'}}>komoditas sesuai dengan permohonan untuk</Text>
                    <Text style={{fontSize:14,color:'black'}}>ditukarkan dengan sertifikat asli</Text>
                </View>
                <View style={styles.ico}>
                    <FitImage
                        indicator={true}
                        indicatorColor="white"
                        resizeMode="cover"
                        source={esertifikatbwh}
                        style={{height:200,width:'90%'}}
                    />
                </View>
            </View>
            </ScrollView>
    </View>
  </ImageBackground>
  )
}

export default Esertifikat

const styles = StyleSheet.create({
    vRadius:{
        flex:1,
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        borderColor:"grey",
        alignContent:'center',
        // flexDirection:'row',
        
        borderWidth:0,paddingHorizontal:15,
        paddingVertical:10
      },
      ico:{
          flex:1,  
          alignContent:'center',
          alignItems:'center',
          marginVertical:5
        },
        ico2:{
          flex:1,  
          alignContent:'center',
          alignItems:'center'
        },
      textTitleView: {
        flex:1,
        alignContent:'center',
        marginVertical:5
      },
      textTitle:{
        fontSize:20,
      },
      shadowView: {
        shadowOffset: { width: 10, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 0.8,
        elevation: 10,
        backgroundColor:'white',marginBottom:10,
        paddingLeft:10,paddingVertical:10,borderRadius:25,
        borderWidth:0
    },
})