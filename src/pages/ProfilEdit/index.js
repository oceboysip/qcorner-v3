import { StyleSheet, Text, View,ImageBackground,ScrollView,TextInput,TouchableOpacity,Modal} from 'react-native'
import React,{useEffect,useState} from 'react'
import { background } from '../../assets'
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { Header } from '../../components'
import { ModalIndicator,ModalInformation } from '../../components';

const ProfileEdit = ({route,params,navigation}) => {
const {reducerSession} = useSelector(state => state); 
const [loaded,setLoaded]=useState(false);
const [LoadingSubmit,setLoadingSubmit]=useState(false);
const [loadingSuccess,setloadingSuccess]=useState(false);
const [LoadingWarning,setLoadingWarning]=useState(false);
const [loadingError,setloadingError]=useState(false);
const [password,setPassword]=useState('');
const[repassword,setRepassword]=useState('');
const [message,setMessage]=useState('');


    const submit =  () => {
        setLoadingSubmit(true);
        const  token =reducerSession.token
        if(password === "" || repassword === "" ){
          alert("mohon Lengkapi password anda !");
           setLoadingSubmit(false);
          return false;
          }

 // komen
        if(password !== repassword){
          alert("Pasword tidak sama");
          setLoadingSubmit(false);
          return false;
        }
//         let uploaddata = new FormData();
//         uploaddata.append('password',password);
//         uploaddata.append('password_confirmation',repassword);
        const body = {
            'password':password,
            'password_confirmation':repassword,
        }
        console.log('inibody',body)
    
      axios({
        method: 'post',
        url: 'http://apiv2.bbkpsoetta.com/profile/change-password',
        data: body,
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' +token,
        }
      }).then(response => {
        //alert('masuk');
        setLoadingSubmit(false);
        if (response.data.status){
         setloadingSuccess(true);
          setMessage(response.data.message);
        setTimeout(() => {
              setloadingSuccess(false);
              setMessage(message);
              navigation.navigate('BerhasilPassword',{type:'home'})   
          }, 1500);
        }
        }).catch(function (error) {
            alert('errorssddds');
            setLoadingSubmit(false);
            console.log('errorsss : ',+error);
          });
      }
  return (
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>

    <View style={{ flex: 1}}>
      <Header onPress={() => navigation.pop()}  />
      <ScrollView>
        <View style={{flex:1,alignItems:'center',marginTop:20}}>
          <Text style={{fontSize:30}}>Rubah Password</Text>
          <Text style={{fontSize:15,marginTop:10}} >
           Mohon Masukan password baru anda 
          </Text>
          <Text style={{fontSize:15}}>
            Dan Konfirmasi Password 
          </Text>
      
        </View> 
        <View style={{flex:3,marginHorizontal:30,marginTop:20}}>
          <View style={{backgroundColor:'white',marginBottom:30,paddingLeft:10,borderRadius:25,borderColor:"grey",borderWidth:1}}>          
      
          </View>
          <View style={{backgroundColor:'white',marginBottom:30,paddingLeft:10,borderRadius:25,borderColor:"grey",borderWidth:1}}>          
          <TextInput
                     placeholder="Type Password"
                     secureTextEntry={true}
                     style={{ height: 40, borderColor: '#FFFFFF'}}
                    //  onChangeText={ (password) => this.setState({ password }) }
                    //  value={this.state.password}
                    value={password}
                    onChangeText={(val) => setPassword(val)}
                  />
          </View>
          <View style={{backgroundColor:'white',marginBottom:30,paddingLeft:10,borderRadius:25,borderColor:"grey",borderWidth:1}}>          
              <TextInput
                      placeholder="Type Re-Password"
                      secureTextEntry={true}
                      style={{ height: 40, borderColor: '#FFFFFF'}}
                    //   onChangeText={ (repassword) => this.setState({ repassword }) }
                    //   value={this.state.repassword}
                    value={repassword}
                    onChangeText={(val) => setRepassword(val)}
                  />
          </View>
          { loaded ? (
              <View style={{justifyContent:'center',alignContent:'center',alignItems:'center',marginRight:10, marginBottom:80}}>
                <Text>Loading ...</Text>
              </View>
            ):(
          <TouchableOpacity onPress={submit}>
            <View style={{backgroundColor:'#917369',marginHorizontal:30,height:40,alignContent:'center',justifyContent:'center',alignItems:'center',marginTop:10,marginBottom:10,borderRadius:25,borderColor:"grey",borderWidth:1}}>          
                <View><Text style={{color:'white',fontSize:20}}>Ganti Password</Text></View>
            </View>
          </TouchableOpacity>
            )}
       

        </View>
       
             {/* notif start */}
             <ModalIndicator
              visible={LoadingSubmit}
              type="large"
              indicators='skype'
              message="Harap tunggu, kami sedang memproses permintaan Anda"
          />

          <Modal
              animationType="fade"
              transparent
              visible={loadingSuccess}
          >
              <ModalInformation
                  label={message}
              />
          </Modal>

          <Modal
              animationType="fade"
              transparent
              visible={LoadingWarning}
          >
              <ModalInformation
                  label={message}
                  warning
              />
          </Modal>

          <Modal
              animationType="fade"
              transparent
              visible={loadingError}
          >
              <ModalInformation
                  label={message}
                  error
              />
          </Modal>
        
      </ScrollView>
    </View>
  </ImageBackground>
  )
}

export default ProfileEdit;

const styles = StyleSheet.create({
    txt_head: {
        color: '#999ca0',
        marginTop:15
    },
    txt_val: {
      color: '#333a42'
    }
})