import React, {useState, useEffect} from 'react';

import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,ImageBackground,
  PermissionsAndroid,
  Platform,
  ScrollView,
} from 'react-native';
import {launchImageLibrary, launchCamera} from 'react-native-image-picker';
import ModalSelector from 'react-native-modal-selector';
import { useSelector } from 'react-redux';

import {background, IconCamera, IconTriangle} from '../../assets';
import {FormInput, FormInputSelect, Gap, Header, Scaffold, Text} from '../../components';
import { Container } from '../../styled';
import {colors, showMessage} from '../../utils';
import { getAPI, postAPI } from '../../utils/httpService';

const Archive = ({navigation}) => {
  const [image, setImage] = useState(null);
  const [disableButton, setDisableButton] = useState(false);
  const [loading, setLoading] = useState(true);
  const { reducerSession } = useSelector((state) => state);
 
  const [form, setForm] = useState({
    nama_file: '',
    foto_rek: {
      uri:  '',
      name: '',
      type: '',
    },
  });

  const onSubmit = () => {
    let messageError = '';

    if (form.nama_file === '') {
      messageError = 'Nama File Wajib Diisi !';
    } 

    if (messageError !== '') {
      showMessage({message: messageError});
      return;
    }

    const body = new FormData();
    body.append('id',reducerSession.role.id);
    body.append('nama', form.nama_file);
    body.append('file', form.foto_rek);

    const headers = {
      'Content-Type': 'multipart/form-data',
    };

    setLoading(true);
    //console.log('kirim',body)
    postAPI('arcive/upload', body, headers).then((res) => {
      console.log('hasilnya', res);

      setLoading(false);

      showMessage({
        message: res.message,
        type: res.status ? 'success' : 'danger',
      });

      setTimeout(() => {
        navigation.pop();
      }, 3000);
    })
    .catch((err) => {
      console.log('err', err);

      setLoading(false);

      showMessage({
        message: err.message,
      });
    });
  }

  // cek perizinan kamera
  async function requestCamera() {
    let valid = false;

    if (Platform.OS === 'ios') {
      valid = true;
    } else {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
      );
      
      valid = granted === PermissionsAndroid.RESULTS.GRANTED;
    }
    
    if (valid) {
      imagePicker('camera');
    } else {
      showMessage({
        message: 'Izin kamera di tolak',
      });
    }
  }

  async function imagePicker(type) {
    setDisableButton(true);
    setTimeout(() => setDisableButton(false), 2000); // menghindari crash
    const Picker = type == 'galeri' ? launchImageLibrary : launchCamera;
    const {assets} = await Picker({
      mediaType: 'photo',
    });
    
    if (assets) {
      setImage(assets[0].uri);
      setForm({
        ...form,
        foto_rek: {
          name: assets[0].fileName,
          type: assets[0].type,
          uri: assets[0].uri,
        }
      })
    }
  }

  return (
   
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} title={'Draft Permohonan Perbaikan'} />
      <ScrollView style={{margin: 20}}>
        <FormInput
          placeholder='Namafile'
          keyboardType='default'
          value={form.nama_file}
          onChangeText={(val) => setForm({ ...form, nama_file: val })}
        />
         <Gap height={24} />
        
        <Text align='left'>
          Upload Foto Rekening
        </Text>

        <Gap height={16} />

        <View style={styles.viewPhoto}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={styles.buttonPhoto}
              onPress={() => imagePicker('galeri')}
              disabled={disableButton}>
              <Text color={colors.white1} size={15}>
                {image ? 'Ubah' : 'Pilih'} File
              </Text>
            </TouchableOpacity>
            <Gap width={7.5} />
            <TouchableOpacity
              style={styles.buttonPhoto}
              onPress={requestCamera}
            >
              <Image source={IconCamera} style={styles.iconCamera} resizeMode='contain' />
            </TouchableOpacity>
            {!image && <Text style={styles.textUnpick}>Belum ada file dipilih</Text>}
          </View>
          {image && (
            <View style={styles.photoContainer}>
              <Image
                source={{uri: image}}
                style={styles.image}
                resizeMethod="resize"
              />
            </View>
          )}
        </View>
        <Container paddingHorizontal={16}>
          <TouchableOpacity onPress={() => onSubmit()} style={styles.buttonSimpan}>
            <Text color={'#000000'} size={17}>Simpan</Text>
          </TouchableOpacity>
        </Container>
      </ScrollView>
    
     </View>
     
     
      </ImageBackground>
   
  );
};

const styles = StyleSheet.create({
  buttonSimpan: {
    backgroundColor: colors.primary,
    width: '100%',
    borderRadius: 3,
    marginTop:20,
    paddingVertical: 12,
  },
  iconCamera: {
    width: 20,
    height: 15,
    tintColor: 'white',
    marginHorizontal: -5,
    marginVertical: 3,
  },
  image: {
    height: 150,
    borderRadius: 5,
  },
  photoContainer: {
    height: 150,
    backgroundColor: 'white',
    marginTop: 10,
    borderRadius: 5,
    elevation: 3,
  },
  textUnpick: {
    flex: 1,
  },
  buttonPhoto: {
    backgroundColor: colors.primary,
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 3,
  },
  viewPhoto: {
    backgroundColor: '#CACACA',
    padding: 10,
    borderRadius: 5,
  },
  textInput: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: colors.text.secondary,
  },
  bankPicker: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginHorizontal: 2.5,
    paddingHorizontal: 5,
  },
  picker: {
    width: '100%',
    height: '100%',
  },
});

export default Archive;
