
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, View,ScrollView ,Text,TouchableOpacity} from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import { background ,bgadacewek} from '../../assets';
import { Header } from '../../components';
import { GetDomestik } from '../../redux/action/permohonan';
import { useForm } from '../../utils';
import axios from "axios";
import { FAB } from "react-native-paper";
import { Button } from "native-base";
import RNFetchBlob from 'rn-fetch-blob';
import {CommonActions, useIsFocused} from '@react-navigation/native';
const FilePendukung = ({navigation, route}) => {

const [loaded,setloaded]=useState(false);
const {reducerSession} = useSelector(state => state); 
const [list_data_dokumen,setListDataDokumen]=useState('');
const [refreshing, setRefreshing] = useState(false);
const dispatch = useDispatch()

const isFocussed = useIsFocused();
  useEffect(() => {
    if (isFocussed) _listData();
  }, [isFocussed]);
  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      //fetchAllData();
      setRefreshing(false);
    }, 3000);
  };
  const _listData =  () => {
    const token = reducerSession.token;
    if (token == false) {
      alert(constant.pesan_relog);
      navigation.navigate("Login");
    }
    axios({
      method: "POST",
      url: 'http://apiv2.bbkpsoetta.com/arcive/lists',
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then(response => {
        setListDataDokumen(response.data.data.data);
      })
      .catch(error => {
        console.log("Error", +error);
      });
  };
  var warna = "#ffffff";
  let i = 1;

  
  const download=(judul,url)=>{
    
    const android = RNFetchBlob.android
 
    RNFetchBlob.config({
        addAndroidDownloads : {
          useDownloadManager : true,
          title : 'Qcorner Download',
          description : 'Download file'+judul,
        
          mediaScannable : true,
          notification : true,
        }
      })
      .fetch('GET', url)
      .then((res) => {
          android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
      })
   
  }
  const GetUrl=(param)=>{
    const url=param;
    console.log(url);
    return url;
  }
  return (
    <ImageBackground
    source={bgadacewek}
    style={{ width: "100%", height: "100%", position: "absolute" }}
  >
    <View style={{ flex: 1 }}>
    <Header onPress={() => navigation.pop()} title={'File Pendukung'} />
      <ScrollView>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View
            style={{
              flex: 1,
              alignItems: "flex-start",
              marginLeft: 30,
              marginTop: 50,
              backgroundColor: ""
            }}
          >
            <Text
              style={{ fontSize: 28, color: "#917369", fontWeight: "bold" }}
            >
              Archive{" "}
            </Text>
            <Text
              style={{ fontSize: 28, color: "#917369", fontWeight: "bold" }}
            >
              List
            </Text>
          </View>
        </View>
        <View style={{ flex: 1, marginHorizontal: 30, marginTop: 40 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#917369",
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              borderColor: "grey",
              borderWidth: 1
            }}
          >
            <View
              style={{ paddingVertical: 10, flex: 1, flexDirection: "row" }}
            >
              <View style={{ flex: 2, alignItems: "flex-start" }}>
                <Text
                  style={{ color: "white", marginLeft: 10, fontSize: 12 }}
                >
                  No
                </Text>
              </View>
              <View style={{ flex: 2, alignItems: "flex-start" }}>
                <Text
                  style={{ color: "white", marginLeft: 5, fontSize: 12 }}
                >
                  Name
                </Text>
              </View>

              <View
                style={{
                  flex: 1.5,
                  marginLeft: 15,
                  alignItems: "flex-start"
                }}
              >
                <Text style={{ color: "white", fontSize: 12 }}>Aksi</Text>
              </View>
            </View>
          </View>

          {list_data_dokumen.length > 0 &&
            list_data_dokumen.map(item => (
              <View style={{ flex: 1, backgroundColor: warna }}>
                <View
                  style={{
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    flex: 1,
                    flexDirection: "row",
                    borderColor: "#ffff",
                    borderBottomWidth: 2
                  }}
                >
                  <View style={{ flex: 1, backgroundColor: "" }}>
                    <Text style={{ fontSize: 12,paddingHorizontal:10 }}>{i ++}</Text>
                  </View>

                  <View style={{ flex: 1,paddingHorizontal:20 }}>
                    <Text style={{ fontSize: 12,width:'100%',marginHorizontal:-20, }}> ({item.name}) </Text>
                  </View>

                  <TouchableOpacity
                    onPress={() =>
                      download(
                        item.name,
                        GetUrl(item.file.toString())
                      )
                    }
                  ><Text style={{ flex: 1,paddingHorizontal:20,fontWeight:'bold',marginHorizontal:5 }}>DOWNLOAD</Text>
                    {/* <Button
                      small
                      style={{
                        flex: 1,
                        justifyContent: "center",
                        marginRight: 0,
                        marginLeft: 15,
                        width: "90%",
                        backgroundColor: "#917369",
                        alignItem: "center"
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text
                          style={{
                            flex: 1,
                            justifyContent: "center",
                            fontSize: 12,
                            alignItem: "center",
                            color: "white",
                            fontWeight: "bold"
                          }}
                        >
                          Download
                        </Text>
                      </View>
                    </Button> */}
                  </TouchableOpacity>
                </View>
              </View>
            ))}
        </View>
      </ScrollView>
      <FAB
        style={styles.fab}
        icon="plus"
        onPress={() => navigation.navigate("Archive")}
      />
    </View>
  </ImageBackground>
  );
};

export default FilePendukung;


const styles = StyleSheet.create({
  imgDashboard: {
    width: 100,
    height: 75
  },
  rounded: {
    // marginHorizontal:20,
    // marginTop:10,
    shadowOffset: { width: 10, height: 10 },
    shadowColor: "black",
    shadowOpacity: 1,
    elevation: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "white",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "white"
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 65,
    backgroundColor: "#917369"
  }
   
    });
