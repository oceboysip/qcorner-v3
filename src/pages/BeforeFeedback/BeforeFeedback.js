import { StyleSheet, Text, View ,ImageBackground,FlatList,Alert,TouchableOpacity} from 'react-native'
import React,{useEffect,useState} from 'react'
import {useDispatch, useSelector} from 'react-redux';
import { background, bgadacewek } from '../../assets';
import axios from 'axios';
import { Header } from '../../components';
import { ScrollView } from 'react-native-gesture-handler';
import { getAPI,postAPI } from '../../utils/httpService';

const BeforeFeedback = ({navigation}) => {
    const {reducerSession} = useSelector(state => state);
    const [result,setResult] =useState([])

    useEffect(() => {
        console.log('reducerdibefore:', reducerSession)
        cekfeedbacknew();
      }, []);

      const checkConfirm = (id) => {
        Alert.alert(
          "Feedback",
          `Apakah anda akan mengisi quisioner ?`,
          [
            {
              text: "Yes",
              style: "default",
              onPress: () =>konfirmasifeedback(1, id),
            },
            {
              text: "No",
              style: "destructive",
              onPress: () => konfirmasifeedback(2, id),
            },
          ],
          { cancelable: false }
        );
      }
      const konfirmasifeedback = (paham,donk) =>{
        const token = reducerSession.token;
        if (token == false) {
          alert("sesi anda telah berakhir silahkan lakukan login kembali");
          navigation.navigate("Login");
        }
        const body={
          'status':paham,
          'id_status_quisioner':donk
        };
        console.log(body);
        postAPI('quisioner/confrim',body).then((response) => {
          console.log('hasil nya')
          if (response.data.status === 0){
            navigation.navigate('Feedback');
            //navigation.navigate('BeforeFeedback')
          } else if(response.data.status === 2) {
            showMessage({
              message: "Terima kasih",
              type: "none",
            });
          }else{
            showMessage({
              message: "Terima kasih",
              type: "info",
            });
          }
        });
      
      }
      const confirmQuisioner =  (status, id) => {
        const token =reducerSession.token
        if (token == false) {
          alert("sesi anda telah berakhir silahkan lakukan login kembali");
          navigation.navigate("Login");
        }
        axios.post(
            'http://apiv2.bbkpsoetta.com/quisioner/confrim',
            {
              status,
              id_status_quisioner: id,
            },
            {
              headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
              },
            }
          )
          .then((response) => {
            console.log("inikonfrimasi:", response);
            if (response.data.data.status === 0) {
              navigation.navigate('Feedback');
            } else {
                 navigation.navigate('Home')
            }
            
          }).catch(err => {
               navigation.navigate('Home')
          })
      }
      const cekfeedbacknew = () => {
        const token =reducerSession.token
        if (token == false) {
          alert("sesi anda telah berakhir silahkan lakukan login kembali");
          navigation.navigate("Login");
        }
     
        console.log(token, "lewat token feedback");
        axios({
          method: "get",
          url: 'http://apiv2.bbkpsoetta.com/quisioner/norespon',
          headers: {
            Authorization: "Bearer " + token,
          },
        })
          .then((response) => {
            console.log("norespon:", response, "lewat token feedback response");
            setResult(response.data.data.data);
            console.log("inidatanya:", response.data.data.data);
            console.log("zzz:", response.data);
          })
          .catch(function (error) {
            console.log(
              "There has been a problem with your fetch operation: " + error.message
            );
            // ADD THIS THROW error
            throw error;
          });
      };


const _RenderDetailData = (id,judul,tanggal)=>{
var warna = '#ffffff'
return (
  <View style={{flex:1,backgroundColor:warna}}>
   <View style={{paddingHorizontal:10,paddingVertical:10,flex:1,flexDirection:'row',borderColor:'#ffff',borderBottomWidth:2}}>
        
        <View style={{flex:2,backgroundColor:''}}>
          <Text style={{fontSize:12}}>{judul}</Text>
        </View>
        <View style={{flex:1,backgroundColor:''}}>
          <Text style={{fontSize:12}}>{tanggal}</Text>
        </View>
        <View style={{flex:1,backgroundColor:'',alignContent:'center'}}>
          <TouchableOpacity onPress={() => checkConfirm(id)}>
            <Text style={{fontSize:12,alignItems:'center',alignContent:'center'}}> View Detail</Text>   
          </TouchableOpacity>                 
        </View>
        </View>

 
      </View>
);
}

  return (

    <ImageBackground source={bgadacewek} style={{width: '100%', height: '100%', position: 'absolute'}}>
    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} />
      <ScrollView>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1,alignItems:'flex-start',marginLeft:30,marginTop:50,backgroundColor:''}}> 
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Survey / </Text>
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Quesioner</Text>
            </View>
          </View>
        <View style={{flex:1,marginHorizontal:30,marginTop:40}}>
          <View style={{flex:1,backgroundColor:'#917369',borderTopLeftRadius:20,borderTopRightRadius:20,borderColor:"grey",borderWidth:1}}>          
            <View style={{paddingVertical:10,flex:1,flexDirection:'row'}}>

              <View style={{flex:1,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>Keterangan</Text>
              </View>
              
              <View style={{flex:2,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>Tanggal</Text>
              </View>
             
              <View style={{flex:1,alignItems:'center'}}>
               <Text style={{color:'white',fontSize:12}}>Aksi</Text>                    
              </View>

          </View>
        </View>

     
      <FlatList
            style={{marginBottom:30}}
            data={result}
            renderItem=
            {
              ({item,index}) => 
                    _RenderDetailData(item.id_status_quisioner,item.title,item.tanggal)
            }
          />
      
          </View>
      </ScrollView>

    </View>
    </ImageBackground>
    
  )
}

export default BeforeFeedback

const styles = StyleSheet.create({
    page: {
        padding: 10,
        flex: 1,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
      },
})