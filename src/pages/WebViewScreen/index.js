import React,{useEffect, useState} from 'react';
import { View, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';
import {Header, Scaffold} from '../../components';
import { useSelector } from 'react-redux';

export default function WebViewScreen({ navigation,route }) {

  // useEffect(()=>{

  // },[])
  const {
    url, title
  } = route.params;
  const {reducerSession} = useSelector(state => state);
  const useWebKit = true;

  console.log("TOKEN: "+reducerSession.token)
  // set a cookie
  // const newCookie = () => {
  //   tokenjuara: reducerSession.token
  // };
  
  // CookieManager.set('http://159.223.57.73:330/', newCookie, useWebKit)
  //   .then((res) => {
  //     console.log('CookieManager.set from webkit-view =>', res);
  //   });

  let cookiesnya =
      "JUARATOKEN=" +
      reducerSession.token +
      "; DEVICETYPE=" +
      "ANDROID";

  console.log("COOKIES "+cookiesnya)
  console.log("URL ---> "+url)

  const [cookiesWeb, setCookiesWeb] = useState(cookiesnya)
  const [canGoBack, setCanGoBack] = useState(false)
  const [canGoForward, setCanGoForward] = useState(false)
  const [currentUrl, setCurrentUrl] = useState('')

  function renderActivityIndicatorLoadingView() {
    return (
      <ActivityIndicator
        color={'#231231'}
        size='large'
        style={{
          position: 'absolute',
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      />
    );
  }

  const CustomHeaderWebView = (props) => {
    const { uri, onLoadStart, ...restProps } = props;
    const [currentURI, setURI] = useState(props.source.uri);
    const newSource = { ...props.source, uri: currentURI };

    console.log("newSource --> ", newSource)
    console.log("props --> ", props)
    const jsCode = "window.postMessage(document.cookie)"
    
    return (
      <WebView
        {...restProps}
      source={newSource}
      renderLoading={renderActivityIndicatorLoadingView}
      sharedCookiesEnabled={true}
      thirdPartyCookiesEnabled={true}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      automaticallyAdjustContentInsets={true}
      mediaPlaybackRequiresUserAction={true}
      allowsInlineMediaPlayback={true}
      bounces={true}
      useWebKit={true}
      allowUniversalAccessFromFileURLs={true}
      allowFileAccess={true}
      injectedJavaScript={cookiesWeb}
      startInLoadingState={true}
      incognito={true}
      onNavigationStateChange={navState => {
        setCanGoBack(navState.canGoBack)
        setCanGoForward(navState.canGoForward)
        setCurrentUrl(navState.url)
      }}
      onMessage={(event) => {
        console.log("Respon JS------>>>>>> 1", event.nativeEvent.data);
        if (event.nativeEvent.data.includes(".pdf")) {
          Linking.openURL(event.nativeEvent.data);
        } else {
        }
      }}
      onShouldStartLoadWithRequest={(request) => {
        console.log("Respon request ------>>>>>> 1", request);
        if (request.url === currentURI) return true;
        setURI(request.url);
        return false;
      }}
      />
    );
  };

  return (
    <Scaffold
      statusBarColor={'#026107'}
      useSafeArea={false}
      header={
          <View style={{height: 60, statusBarHeight : 0, backgroundColor: '#026107', justifyContent: 'flex-end'}}>
              <Header
                  title="Kembali Ke Qcorner"
                  onPress={() => navigation.goBack()}
                  iconLeftButton='close'
                  color={'#FFFFFF'}
                  style={{backgroundColor: '#026107'}}
              />
          </View>
      }
    >
      {/* <CustomHeaderWebView
            source={{
              uri: url,
              headers: {
                Cookie: cookiesWeb,
                // Cookie: "JUARATOKEN=eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiNjFlNTg4ZjA5MTI5MDAyNTE0ODQ2ZDU3IiwiZnJpZW5kbHlfdG9rZW4iOiJvWldSWnJ0VjVkejN5aTMyZlhLOCIsImV4cCI6MTY0NzE3NjUzOSwiaWF0IjoxNjQ3MDkwMTM5LCJpc3MiOiJodHRwczovL210bS5jby5pZCJ9.Bi5o0qi4aVRG4RegtTdTk7s8tYJg9UeD7N8HLAj1rCA; DEVICETYPE=ANDROID"
              },
            }}
          /> */}

      <WebView
            source={{
              uri: url,
              headers: {
                // Cookie: cookiesWeb,
                Authorization: "Bearer "+reducerSession.token,
                Device: "Android"
              },
            }}
            // incognito={true}
           
          />

      
    </Scaffold>
  );
}
