
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, View,ScrollView ,Text,TouchableOpacity} from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import { background ,bgadacewek} from '../../assets';
import { Header } from '../../components';
import { GetDomestik } from '../../redux/action/permohonan';
import { useForm } from '../../utils';
import axios from "axios";
import { FAB } from "react-native-paper";
import { Button } from "native-base";
import RNFetchBlob from 'rn-fetch-blob';
import {CommonActions, useIsFocused} from '@react-navigation/native';
const Notifikasi = ({navigation, route}) => {

const [loaded,setloaded]=useState(false);
const {reducerSession} = useSelector(state => state); 
const [list_data_dokumen,setListDataDokumen]=useState('');
const [refreshing, setRefreshing] = useState(false);
const dispatch = useDispatch()

const isFocussed = useIsFocused();
  useEffect(() => {
    if (isFocussed) _listData();
  }, [isFocussed]);
  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      //fetchAllData();
      setRefreshing(false);
    }, 3000);
  };
  const _listData =  () => {
    const token = reducerSession.token;
    if (token == false) {
      navigation.navigate("Login");
    }
    axios({
      method: "GET",
      url: 'http://apiv2.bbkpsoetta.com/notifications',
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then(response => {
        console.log('datawoy',response.data.data.data);
        setListDataDokumen(response.data.data.data);
      })
      .catch(error => {
        console.log("Error", +error);
      });
  };
  var warna = "#ffffff";
  let i = 1;

  
  
  const GetUrl=(param)=>{
    const url=param;
    console.log(url);
    return url;
  }
  return (
    <ImageBackground
    source={bgadacewek}
    style={{ width: "100%", height: "100%", position: "absolute" }}
  >
    <View style={{ flex: 1 }}>
    <Header onPress={() => navigation.pop()} title={'Notifikasi'} />
      <ScrollView>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View
            style={{
              flex: 1,
              alignItems: "flex-start",
              marginLeft: 30,
              marginTop: 50,
              backgroundColor: ""
            }}
          >
            <Text
              style={{ fontSize: 28, color: "#917369", fontWeight: "bold" }}
            >
              Notifikasi
            </Text>
            <Text
              style={{ fontSize: 28, color: "#917369", fontWeight: "bold" }}
            >
              
            </Text>
          </View>
        </View>
        <View style={{ flex: 1, marginHorizontal: 30, marginTop: 40 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: "#917369",
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              borderColor: "grey",
              borderWidth: 1
            }}
          >
            <View
              style={{ paddingVertical: 10, flex: 1, flexDirection: "row" }}
            >
             
              <View style={{ flex: 1, marginleft:20,alignItems: "center" }}>
                <Text
                  style={{ color: "white", marginleft:20,marginLeft: 5,alignItems:'center', fontSize: 12 }}
                >
                  Deskripsi
                </Text>
              </View>

              <View
                style={{
                  flex: 1.5,
                  marginLeft: 15,
                  alignItems: "center"
                }}
              >
                <Text style={{ color: "white", fontSize: 12 }}>Tanggal</Text>
              </View>
            </View>
          </View>

          {list_data_dokumen.length > 0 &&
            list_data_dokumen.map(item => (
              <View style={{ flex: 1, backgroundColor: warna }}>
                <View
                  style={{
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    flex: 1,
                    flexDirection: "row",
                    borderColor: "#ffff",
                    borderBottomWidth: 2
                  }}
                >
                  {/* <View style={{ flex: 1, backgroundColor: "" }}>
                    <Text style={{ fontSize: 12,paddingHorizontal:5 }}>{i ++}</Text>
                  </View> */}

                  <View style={{ flex: 1,paddingHorizontal:5 }}>
                    <Text style={{ fontSize: 12,width:'100%', }}>{item.content} </Text>
                  </View>

                  <Text style={{ flex: 1,paddingHorizontal:5,marginHorizontal:0 }}>{item.created_at}</Text>
               
                </View>
              </View>
            ))}
        </View>
      </ScrollView>
    
    </View>
  </ImageBackground>
  );
};

export default Notifikasi;


const styles = StyleSheet.create({
  imgDashboard: {
    width: 100,
    height: 75
  },
  rounded: {
    // marginHorizontal:20,
    // marginTop:10,
    shadowOffset: { width: 10, height: 10 },
    shadowColor: "black",
    shadowOpacity: 1,
    elevation: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "white",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "white"
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 65,
    backgroundColor: "#917369"
  }
   
    });
