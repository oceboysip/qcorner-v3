import { StyleSheet, Text, View,ImageBackground,ScrollView,FlatList ,TouchableOpacity} from 'react-native'
import React,{useEffect,useState} from 'react'
import { Header } from '../../components';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import { bgadacewek } from '../../assets';

const Informasi = ({navigation,route}) => {
    const [list_data_dokumen,setListDataDokumen]=useState('');
    const [uri_path, setUriPath] = useState(false);
    const { reducerSession } = useSelector((state) => state);
    useEffect(() => {
      _listData()
      }, []);

    const _listData = () => {
        const token = reducerSession.token;
        if (token == false){
          alert(constant.pesan_relog)
          navigation.navigate('Login')
        }
        console.log("tokennya:"+token)
        axios({
          method: 'get',
          url: 'http://apiv2.bbkpsoetta.com/infokarantina',
          headers: {
            'Authorization': 'Bearer ' +token
          }
        })
        .then(response => {
          // console.log("==============DATA_LIST=============")
          // console.log(response.data.code);
          // console.log("the size => "+response.data.data.length);
          const status = response.data.code;
          console.log('datas',response.data.data)
          console.log(status);
          if (status == 200){
            console.log("masuk sini",response.data.data);
            setListDataDokumen(response.data.data.data)
          } else {
            alert('no')
          }
    
        })
        .catch(function (error) {
          console.log('yah error token keqnya',+error);
        });
        
      }

      const _RenderDetailData = (j,id,judul,image)=>{
   
        const warna = '#ffffff'
        
      console.log('jnya',j++);
      return (
        <View style={{flex:1,backgroundColor:warna}}>
         <View style={{paddingHorizontal:10,paddingVertical:10,flex:1,flexDirection:'row',borderColor:'#ffff',borderBottomWidth:2}}>
              
              {/* <View style={{flex:1,backgroundColor:''}}>
                <Text style={{fontSize:12}}>{j++}</Text>
              </View> */}
              <View style={{flex:2,backgroundColor:''}}>
                <Text style={{fontSize:12}}>{judul}</Text>
              </View>
              <View style={{flex:1,backgroundColor:''}}>
                <TouchableOpacity onPress={() => navigation.navigate('PdfExample', {uri: image})}>
                  <Text style={{fontSize:12}}>Lihat Info Karantina</Text>   
                </TouchableOpacity>                 
              </View>
              </View>


            </View>
      );
     
    };
    let j=1;
  return (
    <ImageBackground source={bgadacewek} style={{width: '100%', height: '100%', position: 'absolute'}}>

    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} />
      <ScrollView>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1,alignItems:'flex-start',marginLeft:30,marginTop:50,backgroundColor:''}}> 
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Informasi </Text>
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Karantina</Text>
            </View>
          </View>
        <View style={{flex:1,marginHorizontal:30,marginTop:40}}>
          <View style={{flex:1,backgroundColor:'#917369',borderTopLeftRadius:20,borderTopRightRadius:20,borderColor:"grey",borderWidth:1}}>          
            <View style={{paddingVertical:10,flex:1,flexDirection:'row'}}>

              {/* <View style={{flex:1,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>No.</Text>
              </View> */}
              
              <View style={{flex:2,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>Info</Text>
              </View>
             
              <View style={{flex:1,alignItems:'center'}}>
               <Text style={{color:'white',fontSize:12}}>View</Text>                    
              </View>

          </View>
        </View>

     
      <FlatList
            style={{marginBottom:30}}
            data={list_data_dokumen}
            renderItem=
            {
              ({item,index}) => 
                    _RenderDetailData(j,item.id,item.infokarantina,item.view)
            }
          />
      
          </View>
      </ScrollView>

    </View>
    </ImageBackground>
  )
}

export default Informasi

const styles = StyleSheet.create({})