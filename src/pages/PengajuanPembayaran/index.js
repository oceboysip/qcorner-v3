import { StyleSheet, Text, View ,ImageBackground,ScrollView,TouchableOpacity,Image,TouchableHighlight} from 'react-native'
import React ,{useEffect,useState} from 'react'
import { background, banktransfer, icondown, tiga,billings,pengajuansertifikat} from '../../assets'
import FitImage from 'react-native-fit-image';
import { Header ,Gap} from '../../components';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import { reducerForgot } from '../../redux/reducer/forgot';
import RNFetchBlob from 'rn-fetch-blob';

export default function PengajuanPembayaran({navigation,params,route}) {
    const [billing,setBilling]=useState("");
    const [pembayaran,setPembayaran]=useState(false);
    const {no_aju} = route.params;
    const [data,setData]=useState({});
    const [value,setValue]=useState(null);
    const {reducerSession} = useSelector(state => state);
    const [uri_path,setUriPath]=useState('');
    const options = [
        {
        gbr: require('../../assets/image/banktransfer.png'),
            key: 'bank',
            text: 'Bank Transfer',
        },
    ];
    useEffect(() => {
        _getbank();
        cariDetailAju();
      }, []);
    
      // const _getData =  async () => {
      //    const dataState = route.params
      //   console.log('datastate:',dataState)
      //   const data = JSON.stringify(dataState)
      //   // console.log("dataku_str: "+JSON.stringify(dataState.current_requirement.notes.biling))
    
      //   setBilling(dataState.data.current_requirement.notes.biling)
      //   setPembayaran(dataState.data.current_requirement.notes.nominal)
    
      //   var data_parse = JSON.parse(data.data)
      //   var dataku = data_parse.data.current_requirement
      //   var data_doc = JSON.stringify(dataku)
      //   var data_doc_2 = JSON.parse(data_doc)
      //   var data_doc_uri = data_doc_2.file_path
      //   console.log("dataku_oy: "+data_doc_uri)
      //   this.setState({uri_path:data_doc_uri});
      //   if(data_doc_uri){
      //     this.setState({uri_path:data_doc_uri});
      //   }
      // }
      const lanjutPembayaran = () => {
        navigation.navigate('PengajuanPembayaran2',{noaju:no_aju, data: data, value:value,billing:billing,pembayaran:pembayaran})
        console.log('lanjutaju:',no_aju)    
      }

      const cariDetailAju =  () => {
        const token = reducerSession.token;
        console.log('tokenss:',token)
        if (token == false){
          alert(constant.pesan_relog)
          navigation.navigate('Login')
        }
        //console.log("tokennya:"+token)
        axios({
          method: 'get',
          url: 'http://apiv2.bbkpsoetta.com/applicant/inquiry/'+no_aju,
          headers: {
            'Authorization': 'Bearer ' +token
          }
        })
        .then(response => {
          // const data = response.data.data
          
          setData(response.data.data);
          console.log('setset',response.data.data)
          setBilling(response.data.data.current_requirement.notes)
          console.log('billing:',response.data.data.current_requirement.notes)
          setPembayaran(response.data.data.current_requirement.notes.nominal)
          setUriPath(response.data.data.current_requirement.file_path)
          console.log('uri:',response.data.data.current_requirement.file_path)
    
        })
        .catch(function (error) {
          console.log('yah error token keqnya',+error);
        });
        
      }
      const _getbank=  () => {
        const  token = reducerSession.token;
    
        // this.GetDetailBank();
         GetInquiryBank();
        if (token == false){
          alert(constant.pesan_relog)
         navigation.navigate('Login')
        }
              
      
      }

      const GetInquiryBank=  () =>{
         const token = reducerSession.token;
          axios({
            method: 'get',
            url: 'http://apiv2.bbkpsoetta.com/applicant/inquiry/'+no_aju,
            headers: {
              'Authorization': 'Bearer ' +token
            }
          })
          .then(response => {
            console.log('data_aju:',response.data.data.documents);
    
            if(response.data.code === 200){
            //   this.setState({uri_path:response.data.data.current_requirement.file_path});
            //   this.setState({billing:response.data.data.current_requirement.notes.biling});
            //   this.setState({pembayaran:response.data.data.current_requirement.notes.nominal});
            setBilling(response.data.data.current_requirement.notes);
            console.log('setbilling:',response.data.data.current_requirement.notes)
            }
      
          })
          .catch(function (error) {
            console.log('yah error',+error.message);
            // throw error;
          });
      }
      const download=(judul,url)=>{
    
        const android = RNFetchBlob.android
     
        RNFetchBlob.config({
            addAndroidDownloads : {
              useDownloadManager : true,
              title : 'Qcorner Download',
              description : 'Download file'+judul,
            
              mediaScannable : true,
              notification : true,
            }
          })
          .fetch('GET', url)
          .then((res) => {
              android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
          })
       
      }
      const GetUrl=(param)=>{
        const url=param;
        console.log(url);
        return url;
      }
  return (
      
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>

        <View style={{ flex: 1}}>
        <Header onPress={() => navigation.pop()} title={'Pengajuan Pembayaran'} />
          <ScrollView>
            <View style={{flex:1,}}>
              <FitImage
                resizeMode="cover"
                originalWidth={400}
                originalHeight={50}
                source={tiga}
                />
            </View>
            <View style={{flex:1,alignItems:'center',marginTop:50,}}>
              <Text style={{fontSize:30,color:'#917369',fontWeight:'bold',}}>Pembayaran </Text>
              <Text style={{ fontSize: 15, marginTop: 10,alignSelf:'center',alignContent:'center',flex:1,textAlign:'center',paddingHorizontal:10, }}>
              Mohon periksa kembali Kwitansi Pembayaran
              dibawah ini, pastikan dokumen - dokumen Anda sudah benar sebelum melanjutkan
              ke proses berikutnya
            </Text>
              
            </View> 
            <Gap height={10}/>
            <View style={{flex:3,marginHorizontal:80,marginTop:20}}>
            <TouchableHighlight
                  onPress={() =>
                    navigation.navigate("PdfExample", {
                      uri: uri_path,
                      link_back: "PengajuanVerifikasi",
                      text_header: "Dokumen",
                    })
                  }
                >
                 
                    <Image
                      style={{
                        height: 60,
                        width: 300,
                        justifyContent: "center",
                        alignSelf: "center",
                      }}
                      source={billings}
                    />
                
                </TouchableHighlight>
                <View
                      style={{
                        flex: 1,
                        backgroundColor: "",
                        paddingHorizontal: 40,
                        marginTop:20,
                      }}
                    >
                      <TouchableOpacity 
                       onPress={() => download('QcornerFileDownload',GetUrl(uri_path.toString()))}
                      >
                        <View
                          style={{
                            backgroundColor: "white",
                            width: '100%',
                            height: 40,
                            alignContent: "center",
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 25,
                            borderColor: "grey",
                            borderWidth: 1,
                          }}
                        >
                          <View>
                            <Text style={{ color: "#917369", fontSize: 12 }}>
                              Download
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
              {/* <View style={{flex:1,marginBottom:10}}>
                <View style={styles.shadowView}>
                <TouchableHighlight onPress={() => navigation.navigate('PdfExample',{uri:uri_path,link_back:'PengajuanVerifikasi',text_header:'Dokumen'})} >
                  <View style={{flex:1,flexDirection:'row'}}>          
                      <View style={{flex:5,alignContent:'flex-start',paddingVertical:10}}>
                      <Text style={{fontSize:15,fontWeight:'bold'}}> </Text>
                    </View>
                    <View style={{flex:1,alignItems:'flex-end',paddingVertical:8,paddingRight:10}}>

                      <Image style={{width:25,height:25,alignContent:'flex-end'}} source={billings}></Image>
                    </View>
                  </View>
                  </TouchableHighlight>
                </View>
                <View style={{flex:1}}>
                <TouchableOpacity onPress={lanjutPembayaran}>
                  <View style={{alignItems:'center',flex:1,backgroundColor:'#917369',
                                marginHorizontal:30,height:40,alignContent:'center',
                                justifyContent:'center',alignItems:'center',
                                marginTop:10,marginBottom:10,borderRadius:25,
                                borderColor:"grey",borderWidth:1}}>          
                      <View><Text style={{color:'white',fontSize:20}}>Download</Text></View>
                  </View>
              </TouchableOpacity>
              </View>
              </View> */}
              {/* <View style={{flex:1,marginBottom:10}}>
                <View style={styles.kotakBilling}>
                  <View style={{flex:1,flexDirection:'row'}}>          
                    <View style={{flex:7,alignItems:'flex-start'}}>
                      <Text style={{color:'#333333'}}>No. Billing</Text>
                    </View>
                    <View style={{flex:2}}>
    <Text style={{color:'#333333'}}>{billing}</Text>
                    </View>
                  </View>
                  {pembayaran && 
                  <View style={{flex:1,flexDirection:'row'}}>
                          
                    <View style={{flex:1,alignItems:'flex-start'}}>
                    
                      <Text style={{color:'#333333'}}>Total Pembayaran</Text>
                    </View>
                    <View style={{flex:1,alignItems:'flex-end'}}>
                    
                      <Text style={{color:'#333333',fontSize:15,fontWeight:'bold'}}>Rp. {pembayaran}</Text>
                 
                    </View>
                  </View>
}
                </View>
              </View> */}

            </View>
            <View >
              {/* <View style={{flex:1,marginBottom:10}}>
                <Text style={{fontSize:15,fontWeight:'bold',paddingHorizontal:40,}}>Metode Pembayaran</Text>              
              </View> */}
              <View style={{flex:1}}>
                {/* {options.map(item => {
                  return (
                    <View style={{flex:1,flexDirection:'row',borderBottomColor:'#dbdbde',borderBottomWidth:1}}>
                      <View style={{flex:2}}>
                        <Image style={{width:40,height:40,alignContent:'flex-start'}} source={item.gbr}></Image>  
                      </View>
                      <View style={{flex:6,paddingVertical:7}}> 

                        <View key={item.key} style={styles.buttonContainer}>
                          <Text>{item.text}</Text>
                          <TouchableOpacity
                            style={styles.circle}
                            onPress={() => {
                              setValue({
                                value: item.key,
                              });
                            }}
                          >
                            {value == item.key && <View style={styles.checkedCircle} />}
                          </TouchableOpacity>
                        </View>
                      </View>

                    </View>
                  );
                })} */}
              </View>
            
         
            </View>
          </ScrollView>

        </View>
        </ImageBackground>
  )
}

const styles = StyleSheet.create({    jusView: {
  shadowOffset: { width: 10, height: 10 },
  shadowColor: 'black',
  shadowOpacity: 0.8,
  // elevation: 10,
  backgroundColor:'white',marginBottom:10,
  paddingLeft:10,paddingVertical:10,borderRadius:25,
  borderWidth:0
},
shadowView: {
  shadowOffset: { width: 10, height: 10 },
  shadowColor: 'black',
  shadowOpacity: 0.8,
  elevation: 10,
  backgroundColor:'white',marginBottom:10,
  paddingLeft:10,paddingVertical:10,borderRadius:25,
  borderWidth:0
},
kotakBilling:{
  shadowOffset: { width: 10, height: 10 },
  shadowColor: 'black',
  shadowOpacity: 0.8,
  elevation: 10,
  backgroundColor:'#917369',marginBottom:10,
  paddingHorizontal:10,paddingVertical:10,borderRadius:15,
  borderWidth:0
},
kotakBayarViaBank:{
  shadowOffset: { width: 10, height: 10 },
  shadowColor: 'black',
  shadowOpacity: 0.8,
  // elevation: 2,
  backgroundColor:'#FDF5E0',marginBottom:10,
  paddingHorizontal:10,paddingVertical:10,borderRadius:15,
  borderWidth:0
},
bigCircle: {
height: 40,
width: 40,
borderRadius: 30,
borderWidth: 1,
  borderColor: '#917369',
  backgroundColor: '#917369',
alignItems: 'center',
  justifyContent: 'center',
  alignContent: 'center'
},
// opt button
buttonContainer: {
flexDirection: 'row',
justifyContent: 'space-between',
alignItems: 'center',
marginBottom: 10,
},

circle: {
height: 20,
width: 20,
borderRadius: 10,
borderWidth: 1,
borderColor: '#ACACAC',
alignItems: 'center',
justifyContent: 'center',
},

checkedCircle: {
width: 14,
height: 14,
borderRadius: 7,
backgroundColor: '#794F9B',
},
// kotak pembayaran
kotakPembayaran: {
  flex:1,backgroundColor:'white',height:'100%',width:'100%',
  paddingHorizontal:30,paddingVertical:10,
  borderTopLeftRadius:20,borderTopRightRadius:20,
  marginTop:10,
  borderColor:'#917369',borderTopWidth:1,
  borderLeftWidth:1,borderRightWidth:1,
},})