import React from 'react';
import { StyleSheet, Dimensions, View,TouchableOpacity,Image,Text } from 'react-native';
import Pdf from 'react-native-pdf';
import { Header } from '../../components';
import {FAB} from 'react-native-paper';
import RNFetchBlob from 'rn-fetch-blob';

const PdfExample = ({route,navigation}) => {
    console.log('aaa:',route.params)
    // const {uri_pdf} = route.params.uri?route.params.uri : 'http://samples.leanpub.com/thereactnativebook-sample.pdf';
    const uri_pdf = route.params.uri ? route.params.uri : "http://samples.leanpub.com/thereactnativebook-sample.pdf";
    const source = { uri: route.params.uri?route.params.uri : 'http://samples.leanpub.com/thereactnativebook-sample.pdf', cache: true };
    // const {link_back} =params.link_back? params.link_back : 'Home';
    // const{text_header} = params.text_header? params.text_header : 'Dokumen';
    // const source = uri_pdf

    const GetUrl=(param)=>{
        const url=param;
        console.log(url);
        return url;
      }

      const download=(judul,url)=>{
    
        const android = RNFetchBlob.android
     
        RNFetchBlob.config({
            addAndroidDownloads : {
              useDownloadManager : true,
              title : 'Qcorner Download',
              description : 'Download file'+judul,
            
              mediaScannable : true,
              notification : true,
            }
          })
          .fetch('GET', url)
          .then((res) => {
              android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
          })
       
      }
  return (
    <View style={{flex:1}}>    
    <View style={{backgroundColor:'#f2dfb6',borderBottomColor:'#EBEBEB',borderBottomWidth:10}}>
        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate(this.state.link_back)}>
            <View style={{alignItems:'flex-start',paddingLeft:10}}>
                <Image style={{width:40,height:40}} source={require('../image/back.png')}></Image>
            </View>
        </TouchableOpacity> */}
        <Header title='Dokument' onPress={()=>navigation.goBack()} />
    </View>
    <View style={styles.container}>

        <Pdf
            source={source}
            onLoadComplete={(numberOfPages,filePath)=>{
                console.log(`number of pages: ${numberOfPages}`);
            }}
            onPageChanged={(page,numberOfPages)=>{
                console.log(`current page: ${page}`);
            }}
            onError={(error)=>{
                console.log(error);
            }}
            onPressLink={(uri)=>{
                console.log(`Link presse: ${uri}`)
            }}
            style={styles.pdf}/>
    </View>
    {/* <FAB
    style={styles.fab}
    icon="download"
    onPress={() => download('QcornerFileDownload',GetUrl(uri_pdf.toString()))}
  /> */}
    </View>
  )
}

export default PdfExample

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 65,
        backgroundColor: '#917369',
      },
})