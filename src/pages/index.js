import Splash from './Splash';
import Login from './Login';
import Home from './Home';
import Profile from './Profile';
import Simulasi from './Simulasi';
import Panduan from './Panduan';
import FilePendukung from './Archive/FilePendukung';
import StatusPermohonan from './StatusPermohonan';
import Survey from './Survey';
import History from './History';
import PengajuanVerifikasi from './PengajuanVerifikasi';
import ProsesVerifikasi from './ProsesVerifikasi';
import BeforeFeedback from './BeforeFeedback/BeforeFeedback';
import PenerbitanBilling from './PenerbitanBilling';
import PengajuanPembayaran from './PengajuanPembayaran';
import PdfExample from './PDF';
import PengajuanPerbaikan from './PengajuanPerbaikan';
import DraftPermohonanPerbaikan from './DraftPermohonanPerbaikan';
import PengajuanPembayaran2 from './PengajuanPembayaran2';
import Esertifikat from './esertifikat';
import DiTerima from './DiTerima';
import Feedback from './Feedback';
import Informasi from './Informasi';
import Archive from './Archive';
import Profile1 from './Profile1';
import ProfileEdit from './ProfilEdit';
import WebViewScreen from './WebViewScreen';
import Notifikasi from './Notifikasi';
import HomeOperator from './Home/HomeOperator';
import Useraktivasi from './Login/Useraktivasi';
import Chats from './Chats';
import StatusPermohonanOperator from './StatusPermohonan/StatusPermohonanOperator'

export {
  Profile1, Chats,
  Notifikasi,
  Useraktivasi,
  StatusPermohonanOperator,
  WebViewScreen,
  ProfileEdit,
  Splash,
  Login,
  Home,
  HomeOperator,
  Profile,
  Simulasi,
  StatusPermohonan,
  Survey,
  FilePendukung,
  Panduan,
  History,
  PengajuanVerifikasi,
  ProsesVerifikasi,
  BeforeFeedback,
  PenerbitanBilling,
  PengajuanPembayaran,
  PdfExample,
  PengajuanPerbaikan,
  DraftPermohonanPerbaikan,
  PengajuanPembayaran2,
  Esertifikat,
  DiTerima,
  Feedback,
  Informasi,
  Archive,

};
