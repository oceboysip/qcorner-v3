import { StyleSheet, Text, View ,ImageBackground,ScrollView,Image} from 'react-native'
import React,{useState,useEffect} from 'react'
import { background, dua,cewekditolak } from '../../assets'
import FitImage from 'react-native-fit-image';
import { Header } from '../../components';
import {useDispatch, useSelector} from 'react-redux';
import axios from 'axios';

const ProsesVerifikasi = ({params,route,navigation}) => {
    const {no_aju} = route.params
    const [data,setData] = useState('');
    const {reducerSession} = useSelector(state => state);


    const CariAju = () => {
      this.setState({load_aju_error: true});
      
      // this.props.navigation.navigate('ResetPassword')    
    }
    useEffect(() => {
    }, []);
  return (
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
        <Header onPress={() => navigation.pop()} title={'Proses Verifikasi'} />
    <View style={{ flex: 1}}>
     
      <ScrollView>
        <View style={{flex:1,backgroundColor:'',marginBottom:20}}>
          <FitImage
            resizeMode="cover"
            originalWidth={400}
            originalHeight={50}
            source={dua}
            />
        </View>
        <View style={{flex:1,alignItems:'center',marginTop:0}}>
          <Text style={{fontSize:30,color:'#917369',fontWeight:'bold', alignItems:'center', textAlign:'center'}}>Proses Verifikasi Permohonan</Text>
          <Text style={{fontSize:15,marginTop:10}}>
            Permohonan Anda
          </Text>
          <Text style={{fontSize:15}}>
            dengan Nomor Aju
          </Text>
          <Text style={{fontSize:15,fontWeight:'bold'}}>
            {no_aju}
          </Text>
          
          <Text style={{fontSize:15}}>
            Sedang di verifikasi
          </Text>
          <Text style={{fontSize:15}}>
          </Text>
          <Image source={cewekditolak} style={{width:150,height:300}}/>
        </View> 
      </ScrollView>

    </View>
    </ImageBackground>
  )
}

export default ProsesVerifikasi

const styles = StyleSheet.create({
    txt_head: {
        color: '#999ca0'
    },
    txt_val: {
      color: '#333a42'
    }
})