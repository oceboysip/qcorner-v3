import { CommonActions } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, View } from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import { background } from '../../assets';
import { Header } from '../../components';
import { GetDomestik } from '../../redux/action/permohonan';
import { useForm } from '../../utils';
const Survey = ({navigation, route}) => {

 const [loaded,setloaded]=useState(false);
 const {reducerSession} = useSelector(state => state); 
 const [data_permohonan,setdata_permohonan]=useState([]);
 const [myname,setmyname]=useState(reducerSession.name);
 const [isMenuVisible,setisMenuVisible]=useState('');
 

  const [form, setForm] = useForm({
    email: '',
    password: '',
    fcm_token: '',
  });
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch()
  const [jenispermohonan,setJenisPermohonan]=useState([]);
  const [selectedLanguage, setSelectedLanguage] = useState();
  useEffect(() => {
    console.log("ini home");
    console.log(reducerSession)
    //LoadMaster()
  
  }, []);
  const LoadMaster = () => {
    setJenisPermohonan(dispatch(GetDomestik()));
    console.log('wedeh',jenispermohonan)
  }

  const redirectTo = (name, params) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name, params}],
      }),
    );
  };

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      //fetchAllData();
      setRefreshing(false);
    }, 3000);
  };
  
  return (
  <ImageBackground source={background} style={{ width: "100%", height: "100%" }} >
   <View style={{ flex: 1}}>
        <Header onPress={()=>navigation.pop()} title={'Survey'}/>
    </View>
    
  </ImageBackground>
  );
};

export default Survey;


const styles = StyleSheet.create({
   
    });
