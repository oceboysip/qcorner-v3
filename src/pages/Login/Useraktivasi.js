import { CommonActions } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import {
  Image, ImageBackground,
  ScrollView, StyleSheet, Text,TextInput, View, Alert,
} from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import {
    Logo_Barantan,background,qna
} from '../../assets';
import { Header } from '../../components';
import { useForm } from '../../utils';
import { postLOGIN } from '../../utils/httpService';

import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";

import ButtonCommon from '../../components/Button/ButtonCommon';

const Useraktivasi = ({navigation, route}) => {
   const {device_token} = route.params
  const [loaded,setloaded]=useState(false);
  const {reducerSession} = useSelector(state => state); 
  const [form, setForm] = useForm({
    code_perusahaan:'',
    npwp: '',
    ktp: '',
    email: '',
    password: '',
    phone: '',
    token: device_token,
  });
  function formatNpwp(value) {
    if (typeof value === 'string') {
      return value.replace(/(\d{2})(\d{3})(\d{3})(\d{1})(\d{3})(\d{3})/, '$1.$2.$3.$4-$5.$6');
    }
  }
  const redirectTo = (name, params) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name, params}],
      }),
    );
  };
  const onSubmit =  () => {

    let body = {
      npwp: formatNpwp(form.npwp),
      ktp: form.ktp,
      email: form.email,
      password: form.password,
      phone: form.phone,
      device_token: form.token,
    };
 

    
    postLOGIN('/user/activasi',body).then(response => {
      console.log(response.data.message)
      showMessage({
        message:  response.data.message,
        type: "success",
      });
      redirectTo("Login")
    }).catch(function (response) {
      showMessage({
        message:  response.content.errors.message,
        type: "danger",
      });
  
      // ADD THIS THROW error
      throw error;
    });
  };



  return (
  <ImageBackground source={background} style={{ width: "100%", height: "100%" }} >
  <Header onPress={() => navigation.pop()}  />
   <View style={{ flex: 1}}>
        <ScrollView style={{flex:1,padding:20}}>
        <View style={{style:1,marginVertical:10,alignItems:'center'}}>
            <Image style={{width:130,height:130}} source={Logo_Barantan}></Image>
        </View>

       
              <View style={{
                  backgroundColor: 'white',
                  paddingLeft: 20,
                  marginBottom: 10,
                  borderRadius: 25,
                  borderColor: 'grey',
                  borderWidth: 1,
                }}>
                <TextInput
                  placeholder="NPWP (15 Digits)"
                  value={form.npwp}
                  maxLength={15}
                  keyboardType='numeric'
                  onChangeText={val => setForm('npwp', val)}
                  style={{height: 40, borderColor: '#FFFFFF'}}
                />
              </View>
              <View style={{
                  backgroundColor: 'white',
                  paddingLeft: 20,
                  marginBottom: 10,
                  borderRadius: 25,
                  borderColor: 'grey',
                  borderWidth: 1,
                }}>
                <TextInput
                  placeholder="KTP (16 Digits)"
                  keyboardType='numeric'
                  maxLength={16}
                  value={form.ktp}
                  onChangeText={val => setForm('ktp', val)}
                  style={{height: 40, borderColor: '#FFFFFF'}}
                />
              </View>

              <View style={{
                  backgroundColor: 'white',
                  paddingLeft: 20,
                  marginBottom: 10,
                  borderRadius: 25,
                  borderColor: 'grey',
                  borderWidth: 1,
                }}>
                <TextInput
                  placeholder="Email"
                  value={form.email}
                  onChangeText={val => setForm('email', val)}
                  style={{height: 40, borderColor: '#FFFFFF'}}
                />
              </View>

              <View style={{
                  backgroundColor: 'white',
                  paddingLeft: 20,
                  marginBottom: 10,
                  borderRadius: 25,
                  borderColor: 'grey',
                  borderWidth: 1,
                }}>
                <TextInput
                  placeholder="Password"
                  value={form.password}
                  onChangeText={val => setForm('password', val)}
                  style={{height: 40, borderColor: '#FFFFFF'}}
                />
              </View>
              <View style={{
                  backgroundColor: 'white',
                  paddingLeft: 20,
                  marginBottom: 10,
                  borderRadius: 25,
                  borderColor: 'grey',
                  borderWidth: 1,
                }}>
                <TextInput
                  placeholder="Hp/Telps"
                  value={form.phone}
                  keyboardType='numeric'
                  onChangeText={val => setForm('phone', val)}
                  style={{height: 40, borderColor: '#FFFFFF'}}
                />
              </View>
              <View style={{
                  
                  paddingLeft: 20,
                  borderRadius: 25,
                }}>
              <ButtonCommon  onPress={onSubmit}  title="Verify" />
             
             <View style={{
                  marginTop: 20
                }}>
              <Text> Pastikan Nomor NPWP (XX.XXX.173.1-907.000) atau KTP sesuai dengan yang tertera di identitas dan
               Anda sudah terdaftar di PPK Online</Text>
              </View>
              </View>

        </ScrollView>

    </View> 
  </ImageBackground>
  );
};

export default Useraktivasi;

