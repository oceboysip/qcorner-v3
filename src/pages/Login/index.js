import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { CommonActions } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator, Dimensions, Image, ImageBackground, ScrollView, StyleSheet, Text, TextInput, TouchableNativeFeedback, View
} from 'react-native';
import PushNotification from 'react-native-push-notification';
import { useDispatch, useSelector } from 'react-redux';
import {
  background, karaketer2, logo
} from '../../assets';
import ButtonCommon from '../../components/Button/ButtonCommon';
import { signInAction } from '../../redux/action/auth';
import { showMessage, useForm } from '../../utils';
import { localPushNotification } from '../../utils/pushNotification';
const Login = ({ navigation, route }) => {
  const { params } = route;

  const [loaded, setloaded] = useState(false);
  const { reducerSession } = useSelector(state => state);
  const [form, setForm] = useForm({
    email: '',
    password: '',
    fcm_token: '',
  });
  const [login, setLogin] = useState(false);
  const [tokennya, settokenya] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (login == true) {
      if (reducerSession.token) {
        console.log('tokenlogin', reducerSession.role)
        if (reducerSession.role.name === 'Operator') {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: 'OperatorApp' }],
            }),
          );
        } else {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: 'MainApp' }],
            }),
          );
        }
      }
    }
  }, [reducerSession.token])

  useEffect(() => {
    PushNotification.configure({


      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log('DEVICETOKEN:', token.token);
        setForm('fcm_token', token.token);
      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);

        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);
        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
    console.log(params)

    if (params && params.logout) {
      showMessage({
        message: 'Silakan login ulang',
      });

      navigation.setParams({ logout: false });
    }
  }, [params]);

  const onSubmit = () => {
    dispatch(signInAction(form, navigation));
    setLogin(true);
    //console.log('batukdari dulu')   
  };

  const Useraktivasi = () => {
    navigation.navigate("Useraktivasi", { device_token: form.fcm_token })
  }


  return (
    <ImageBackground
      source={background}
      style={{ width: '100%', height: '100%', flex: 1 }}>

      <ScrollView
        style={{
          flex: 1,
          marginTop: 50,

        }}>
        <View style={styles.image_corner_vw}>
          <Image
            style={styles.image_corner}
            source={logo}
          />
          <Text>Version 3.0</Text>
        </View>

        <View style={{ flex: 1, marginHorizontal: 30 }}>

          <View
            style={{
              backgroundColor: 'white',
              paddingLeft: 10,
              marginBottom: 10,
              borderRadius: 25,
              borderColor: 'grey',
              borderWidth: 1,
            }}>
            <TextInput
              placeholder="Email atau no hp"
              value={form.email}
              style={{ height: 40, borderColor: '#FFFFFF' }}
              onChangeText={val => setForm('email', val)}
            />
          </View>
          <View
            style={{
              backgroundColor: 'white',
              paddingLeft: 10,
              marginTop: 10,
              marginBottom: 10,
              borderRadius: 25,
              borderColor: 'grey',
              borderWidth: 1,
            }}>
            <TextInput
              placeholder="Kata sandi"
              secureTextEntry={true}
              style={{ height: 40, borderColor: '#FFFFFF' }}
              onChangeText={val => setForm('password', val)}
              value={form.password}
            />
          </View>
          {loaded == true ? (
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  marginHorizontal: 30,
                  height: 40,
                  alignContent: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: 'black',
                  borderWidth: 0.5,
                  flex: 1,
                  marginTop: 8,
                  borderRadius: 200,
                  backgroundColor: 'white',
                  alignItems: 'center',
                }}>
                <ActivityIndicator
                  size="small"
                  color="#917369"
                  style={{ marginRight: 10 }}
                />
                <Text style={{ color: '#917369', fontSize: 20 }}>
                  Harap Tunggu ..
                </Text>
              </View>
            </View>
          ) : (<>
            <ButtonCommon onPress={onSubmit} title="Login" />
            <ButtonCommon onPress={Useraktivasi} title="Aktivasi" />
          </>
          )}

        </View>
        <View style={{ backgroundColor: '', flex: 1, alignItems: 'center' }}>
          <View style={{ marginHorizontal: 30, flex: 1, alignContent: 'flex-end', alignItems: 'center' }}>
            <Image
              style={{ height: 250, width: 300, resizeMode: 'stretch', }}
              source={karaketer2}
            />
          </View>
        </View>
      </ScrollView>

    </ImageBackground>
  );
};

export default Login;

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  image_corner_vw: {
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    // backgroundColor:'yellow',
    height: 180,
  },
  image_corner: {
    width: 140,
    height: 150,
    resizeMode: 'stretch',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modal: {
    backgroundColor: "#00000099",
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalContainer: {
    backgroundColor: "#f9fafb",
    width: "80%",
    borderRadius: 5
  },
  modalHeader: {

  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    padding: 15,
    color: "#000"
  },
  divider: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray"
  },
  modalBody: {
    backgroundColor: "#fff",
    paddingVertical: 20,
    paddingHorizontal: 10
  },
  modalFooter: {
  },
  actions: {
    borderRadius: 5,
    marginHorizontal: 10,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  actionText: {
    color: "#fff"
  }
});
