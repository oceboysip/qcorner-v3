import { CommonActions } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, View,ScrollView ,Text,FlatList} from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import { background, bgadacewek } from '../../assets';
import { Header } from '../../components';
import { GetDomestik } from '../../redux/action/permohonan';
import { useForm } from '../../utils';
import axios from 'axios';
import { getAPI } from '../../utils/httpService';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Panduan = ({navigation, route}) => {

 const [loaded,setloaded]=useState(false);
 const {reducerSession} = useSelector(state => state); 
  const dispatch = useDispatch()
  const [jenispermohonan,setJenisPermohonan]=useState([]);
  const [selectedLanguage, setSelectedLanguage] = useState();
  const [list_data_dokumen,setListDataDokumen] = useState('');
  useEffect(() => {
  GetData();
  }, []);

  const GetData =()=>{
    getAPI('panduan')
    .then(res => {
        console.log('res', res.data.data);
        setListDataDokumen(res.data.data)

    })
  }

  const _listData = async () => {
    const token = reducerSession.token;
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    console.log("tokennya:"+token)
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/panduan',
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      // console.log("==============DATA_LIST=============")
      // console.log(response.data.code);
      // console.log("the size => "+response.data.data.length);
      setListDataDokumen(response.data.data.data)
      const status = response.data.code;
      console.log(status);
      if (status == 200){
        console.log("masuk sini",response.data.data);
       setListDataDokumen(response.data.data.data)
      } else {
        // alert('no')
      }

    })
    .catch(function (error) {
      console.log('yah error token keqnya',+error);
    });
    
  }


const _RenderDetailData = (id,judul,image)=>{
   
    var warna = '#ffffff'
 
  
  return (
    <View style={{flex:1,backgroundColor:warna}}>
     <View style={{paddingHorizontal:10,paddingVertical:10,flex:1,flexDirection:'row',borderColor:'#ffff',borderBottomWidth:2}}>
          
          <View style={{flex:1,backgroundColor:''}}>
            <Text style={{fontSize:12}}>{id}</Text>
          </View>
          <View style={{flex:2,backgroundColor:''}}>
            <Text style={{fontSize:12}}>{judul}</Text>
          </View>
          <View style={{flex:1,backgroundColor:''}}>
            <TouchableOpacity onPress={() => navigation.navigate('PdfExample', {uri: image})}>
              <Text style={{fontSize:12}}>Lihat Panduan</Text>   
            </TouchableOpacity>                 
          </View>
          </View>

   
        </View>
  );
}
  
  return (
    <ImageBackground source={bgadacewek} style={{width: '100%', height: '100%', position: 'absolute'}}>

    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} title={'Panduan'} />
      <ScrollView>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1,alignItems:'flex-start',marginLeft:30,marginTop:50,backgroundColor:''}}> 
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Panduan</Text>
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Karantina</Text>
            </View>
          </View>
        <View style={{flex:1,marginHorizontal:30,marginTop:40}}>
          <View style={{flex:1,backgroundColor:'#917369',borderTopLeftRadius:20,borderTopRightRadius:20,borderColor:"grey",borderWidth:1}}>          
            <View style={{paddingVertical:10,flex:1,flexDirection:'row'}}>

              <View style={{flex:1,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>No.</Text>
              </View>
              
              <View style={{flex:2,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>Panduan</Text>
              </View>
             
              <View style={{flex:1,alignItems:'center'}}>
               <Text style={{color:'white',fontSize:12}}>View</Text>                    
              </View>

          </View>
        </View>

     
      <FlatList
            style={{marginBottom:30}}
            data={list_data_dokumen}
            renderItem=
            {
              ({item,index}) => 
                   _RenderDetailData(item.id,item.panduan,item.view)
            }
          />
      
          </View>
      </ScrollView>

    </View>
    </ImageBackground>
  );
};

export default Panduan;


const styles = StyleSheet.create({
   
    });
