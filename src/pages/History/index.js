import { CommonActions } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, View,ScrollView,Text,FlatList,TouchableOpacity} from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import { background, bgadacewek } from '../../assets';
import { Header } from '../../components';
import { GetDomestik } from '../../redux/action/permohonan';
import { useForm } from '../../utils';
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";

const History = ({navigation, route}) => {
  const [list_data_aju,setListDataAju]=useState([]);
  const [no_aju,setNoAju]=useState([]);
 const [loaded,setloaded]=useState(false);
 const {reducerSession} = useSelector(state => state); 
 const [data_permohonan,setdata_permohonan]=useState([]);
 const [myname,setmyname]=useState(reducerSession.name);
 const [isMenuVisible,setisMenuVisible]=useState('');
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch()
  const [jenispermohonan,setJenisPermohonan]=useState([]);
  const [selectedLanguage, setSelectedLanguage] = useState();
  useEffect(() => {
  _listData();
  }, []);
  const cariDetailAju =  (no_aju) => {
    const token = reducerSession.token;
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    console.log('iniajuan',no_aju)
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/applicant/inquiry/'+no_aju,
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      // console.log("==============DATA_AJU_DETAIL=============")
      // console.log(response.data);
      var data = response.data
      console.log('datakirim:',response.data)
      setNoAju(response.data.data.aju_number)
      window.dataAjuDetail = data
    })
    .catch(error => {
      console.log('error : ',+error);
      window.dataAjuDetail = false
    });

    return window.dataAjuDetail
}
  const detailAju =  (no_aju,status,detailStatus) => {
    // console.log(no_aju);
    const token = reducerSession.token
    var dataAju = cariDetailAju(no_aju);
    console.log('dataajuh:',dataAju)
    if (dataAju == false){
      showMessage({
        message: "Data Kosong Silahkan Menghubungi Operator",
        type: "danger",
      });
      return
    }
    // var dataku = JSON.parse(dataAju)
    // console.log("status:"+status);
    // return
    
    if (status == 'Draft Sertifikat'){
      clearInterval(interval)
      navigation.navigate('PengajuanVerifikasi',{no_aju: no_aju,data:dataAju})
    }else if(status == 'Pending'){
      clearInterval(interval)
      navigation.navigate('PengajuanPerbaikan',{no_aju: no_aju,data:dataAju})
    }else if(status == 'Proses Verifikasi'){
     navigation.navigate('ProsesVerifikasiPermohonan',{no_aju: no_aju,data:dataAju})
    }else if(status == 'persetujuan'){
      showMessage({
        message: "Status Sudah Di Setujui",
        type: "danger",
      });
    }else if(status == 'Proses Penerbitan Biling'){
      navigation.navigate('PenerbitanBilling',{no_aju: no_aju,data:dataAju,token:token})
    }else if(status == 'Penerbitan Biling'){
      navigation.navigate('PengajuanPembayaran',{no_aju: no_aju,data:dataAju,token:token})
    }else if(status == 'Penerbitan Sertifikat' ){
      navigation.navigate('Esertifikat',{no_aju: no_aju,data:dataAju})      
    }else if(status == 'serah_terima'){
      navigation.navigate('Esertifikat',{no_aju: no_aju,data:dataAju})            
    }else if(status == 'Ditolak'){
      navigation.navigate('PengajuanPpkDitolak',{no_aju: no_aju,data:dataAju})
    }else if(status == 'Dimusnahkan'){
      navigation.navigate('PengajuanPpkDitolak',{no_aju: no_aju,data:dataAju})
    }
    else if(status == 'Menunggu verifikasi'){
      navigation.navigate('PengajuanSetelahSubmit',{no_aju: no_aju,data:dataAju})
    }else if(status == 'Tutup (Selesai)'){
      console.log(detailStatus)
      if(detailStatus== 'Ditolak' || detailStatus== 'Dimusnahkan'){
        navigation.navigate('PengajuanPpkDitolak',{no_aju: no_aju,data:dataAju})
      }else{

      navigation.navigate('esertifikat',{no_aju: no_aju,data:dataAju})  
      }
    }else{
      showMessage({
        message: "Status Masih Dalam Proses",
        type: "danger",
      });
    }
  }
  const _listData =  () => {
    const token = reducerSession.token;
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    console.log("tokennya:"+token)
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/applicant/inquiry-list?status=Tutup (Selesai)',
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      console.log("==============DATA_LIST=============")
      console.log(response.data.data);
      const data = response.data.data
      // const no_aju = response.data.data.aju_number;
      // console.log('ajuin:',no_aju)
      setNoAju(response.data.data.aju_number)
      const no_aju = data.filter(item => item.aju_number === setNoAju) 
      setListDataAju(response.data.data)
      var status = response.data.code
      // if (status == 200){
      //   setListDataAju(response.data.data)
      //   setNoAju(response.data.data.aju_number)
      // } else {
      //   // alert('no')
      // }

    })
    .catch(function (error) {
      console.log('yah error token keqnya',+error);
    });
    
  }
  const _RenderDetailData = (no_aju,status,status_detail,id)=>{
    // if(status == 'Tutup (Selesai)'){
      var statustiket = status
      var hit = id % 2
      if(hit != 0){
       var warna = '#EBE5DF'
      }else{
        var warna = '#ffffff'
      }
      
      return (
        // <View style={{flex:1,backgroundColor:'#EBE5DF'}}>
        <View style={{flex:1,backgroundColor:warna}}>
          <View style={{paddingHorizontal:10,paddingVertical:10,flex:1,flexDirection:'row',borderColor:'#ffff',borderBottomWidth:2}}>
              <View style={{flex:2,backgroundColor:''}}>
                <Text style={{fontSize:12}}>{no_aju}</Text>
              </View>
              <View style={{flex:1,backgroundColor:''}}>
                <TouchableOpacity onPress={() =>detailAju(no_aju,statustiket,status_detail)}>
                  <Text style={{fontSize:12}}>{statustiket}</Text>   
                </TouchableOpacity>                 
              </View>
            </View>
        </View>
      );
    }


  return (
    <ImageBackground source={bgadacewek} style={{width: '100%', height: '100%', position: 'absolute'}}>

    <View style={{ flex: 1}}>
    <Header onPress={() => navigation.pop()} title={'History'} />
      <ScrollView>
          <View style={{flex:1,flexDirection:'row'}}>
            <View style={{flex:1,alignItems:'flex-start',marginLeft:30,marginTop:50,backgroundColor:''}}> 
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>History</Text>
              <Text style={{fontSize:28,color:'#917369',fontWeight:'bold'}}>Permohonan</Text>
            </View>
          </View>
        <View style={{flex:1,marginHorizontal:30,marginTop:40}}>
          <View style={{flex:1,backgroundColor:'#917369',borderTopLeftRadius:20,borderTopRightRadius:20,borderColor:"grey",borderWidth:1}}>          
            <View style={{paddingVertical:10,flex:1,flexDirection:'row'}}>
              <View style={{flex:2,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>No.Aju</Text>
              </View>
              {/* <View style={{flex:1,alignItems:'center'}}>
                <Text style={{color:'white',fontSize:12}}>Pemohon</Text> 
              </View> */}
              <View style={{flex:1,alignItems:'center'}}>

               <Text style={{color:'white',fontSize:12}}>Status</Text>                    
              </View>
            </View>
          </View>

          <FlatList
            style={{marginBottom:30}}
            data={list_data_aju}
            renderItem=
            {
              ({item,index}) => 
                    _RenderDetailData(item.aju_number,item.inquery_status,item.inquery_status,index)
            }
          />
          </View>
      </ScrollView>

    </View>
    </ImageBackground>
  );
};

export default History;


const styles = StyleSheet.create({
   
    });
