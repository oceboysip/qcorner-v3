import { CommonActions } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import {
  Image, ImageBackground,
  ScrollView, StyleSheet, Text, TouchableHighlight, View,
  TouchableOpacity,
  TouchableNativeFeedback,
  RefreshControl,StatusBar, Alert,
} from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import {
    Logo_Barantan,logoutimg,kontak_kami,dokumensaya,editprofil, BGprofilLIUKnogaris,berhasil, homecekstatus, homehistory, homekartun, homepanduan, homesimulasi, IconNotif, qna
} from '../../assets';
import { Header } from '../../components';
import { useForm } from '../../utils';
import axios from 'axios';
import { showMessage, hideMessage } from "react-native-flash-message";

const Profile = ({navigation, route}) => {

 const [loaded,setloaded]=useState(false);
 const {reducerSession} = useSelector(state => state); 
 const [fcm,setfcm]=useState('');
 const [myname,setmyname]=useState(reducerSession.name);
 const [isMenuVisible,setisMenuVisible]=useState('');
 const [mydata,setMyData] = useState('');
  const [refreshing, setRefreshing] = useState(false);
  const dispatch = useDispatch()

  useEffect(() => {
 _getProfil();
  }, []);

  const redirectTo = (name, params) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name, params}],
      }),
    );
  };
  const _getProfil =  () => {
    const token = reducerSession.token;
    console.log('tokenprofile',token)
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/profile',
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      console.log('dataprofile:',response);
      setMyData(response.data.data)
      var status = response.data.code
      // if (status == 200){
      //   setMyData(response.data.data)
      // } else {
      //   alert('gagal ambil data profil')
      // }

    })
    .catch(function (error) {
      alert('gagal ambil data profil',+error)
      console.log('error',+error);
      // throw error;
    });
  }

  const logout = () => {
    Alert.alert(
      'Logout',
      'Apakah Anda yakin akan mengakhiri sesi ini ?',
      [
        {text: 'Cancel', onPress: () => console.log('Ask me later pressed')},
        {text: 'OK', onPress: () => onLogout()},
      ],
      
    );
  }
  const onLogout = () => {
     dispatch({type: 'USER.CLEAR_AUTH'});
     redirectTo('Login');
  };

  const onRefresh = () => {
    setRefreshing(true);

    setTimeout(() => {
      //fetchAllData();
      setRefreshing(false);
    }, 3000);
  };
  const ButtonAlert = () => {
  // Alert.alert(
  //   "Alert Title",
  //   "My Alert Msg",
  //   [
  //     {
  //       text: "Cancel",
  //       onPress: () => console.log("Cancel Pressed"),
  //       style: "cancel"
  //     },
  //     { text: "OK", onPress: () => console.log("OK Pressed") }
  //   ]
  // );

  showMessage({
    message: "Mohon Hubungi Operator Kami Terimakasih",
    type: "info",
  });
  }

  return (
    
  <ImageBackground source={BGprofilLIUKnogaris} style={{ width: "100%", height: "100%" }} >
  <Header onPress={() => navigation.pop()} noback />
   <View style={{ flex: 1}}>
        <ScrollView style={{flex:1}}>
        <View style={{style:1,marginVertical:10,alignItems:'center'}}>
            <Image style={{width:130,height:130}} source={Logo_Barantan}></Image>
        </View>
        <View style={{flex:1,alignItems:'center',marginBottom:20}}>
            <Text style={{fontWeight:'bold',fontSize:25}}>
            {reducerSession.name}
            </Text>
        </View>
                <TouchableNativeFeedback onPress={() => navigation.navigate('Profile1',{data:mydata,type:'Home'})} >
                   <View style={styles.Bprofile}>
                        <View style={styles.ico}>
                          <Image style={{width:30,height:30}} source={editprofil}></Image>
                        </View>
                        <View style={styles.textTitleView}>
                          <Text style={styles.textTitle}>Profil</Text>
                        </View>
                    </View>
                  </TouchableNativeFeedback>
                  {reducerSession.role.name!=='Operator' ?
                  <TouchableNativeFeedback onPress={() => navigation.navigate('History')}>
                    <View style={styles.BDocument}>
                      <View style={styles.ico}>
                        <Image style={{width:30,height:30}} source={dokumensaya}></Image>
                      </View>
                      <View style={styles.textTitleView}>
                        <Text style={styles.textTitle}>Dokumen Saya</Text>
                      </View>
                    </View>
                  </TouchableNativeFeedback> : <></>}
                  <TouchableNativeFeedback  onPress={() =>  navigation.navigate('WebViewScreen', { title: 'Website Soetta' ,url: 'bbkpsoetta.com' })}>
                    <View style={styles.BDocument}>
                      <View style={styles.ico}>
                        <Image style={{width:30,height:30}} source={kontak_kami}></Image>
                      </View>
                      <View style={styles.textTitleView}>
                        <Text style={styles.textTitle}>Kontak Kami</Text>
                      </View>
                    </View>
                  </TouchableNativeFeedback>
                  <TouchableNativeFeedback onPress={logout}>
                    <View style={styles.BDocument}>
                      <View style={styles.ico}>
                        <Image style={{width:30,height:30}} source={logoutimg}></Image>
                      </View>
                      <View style={styles.textTitleView}>
                        <Text style={styles.textTitle}>Logout</Text>
                      </View>
                    </View>
                  </TouchableNativeFeedback>


        </ScrollView>

    </View> 
  </ImageBackground>
  );
};

export default Profile;


const styles = StyleSheet.create({
    ico:{
        flex:1,  
        alignContent:'center'

      },
    textTitleView: {
      flex:10,
      marginLeft:10,
      alignContent:'center'
    },
    textTitle:{
      fontSize:20,
    },
    BDocument:{
        flex:1,backgroundColor:'white',paddingHorizontal:15,
                                  paddingVertical:10,flexDirection:'row',
                                  borderColor:"grey",borderBottomWidth:0.9
    },
    Bprofile: {
        flex:1,backgroundColor:'white',borderTopLeftRadius:20,
        borderTopRightRadius:20,borderColor:"grey",
        alignContent:'center',
        flexDirection:'row',
        borderBottomWidth:0.9,
        borderLeftWidth:0.9,
        borderRightWidth:0.9,
        borderTopWidth:0.9,
        shadowColor: 'black',
        backgroundColor:'',
        borderWidth:0,paddingLeft:15,paddingVertical:10,height:50
      }
    
    });
