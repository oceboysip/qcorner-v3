import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/native';
import React, {useEffect, useRef} from 'react';
import {StyleSheet, View, Image, Animated, StatusBar, Platform} from 'react-native';
import { useSelector } from 'react-redux';

import {logo} from '../../assets';
import { colors } from '../../utils';
import { getAPI } from '../../utils/httpService';

export default function Splash({ navigation }) {
  const { reducerSession } = useSelector(state => state);

  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    if (Platform.OS === 'ios') {
      handleNavigate();
    } else {
      setTimeout(() => {
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 400,
          useNativeDriver: true,
        }).start();
        
        setTimeout(() => {
          handleNavigate();
        }, 1000);
      }, 1000);
    }    
  }, []);

  const handleNavigate = async () => {
    if (reducerSession.token) {
      // api check token soon needed
      let session = await AsyncStorage.getItem('mySess')
      var parse_session  = JSON.parse(session);
      var count = Object.keys(parse_session).length;
      // return
      if ( count > 2){
      
        if (reducerSession.role.name==='Operator') {
          redirectTo('OperatorApp');
        }else if (reducerSession.role.name!=='Operator'){
          
          redirectTo('MainApp');
        }
      } else {
        redirectTo('Login');
      }
      
    } else {
      navigation.replace('Login');
    }
  };

  const redirectTo = (name, params) => {
    navigation.dispatch(
        CommonActions.reset({
            index: 0,
            routes: [
                { name, params }
            ],
        })
    );
  }

  return (
    <Animated.View style={{...styles.container, opacity: fadeAnim}}>
      <Image source={logo} style={styles.image} />
      <View style={{margin: 10}} />
      <StatusBar translucent={true} barStyle="default" />
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 95,
    height: 110,
  }
});
