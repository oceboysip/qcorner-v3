import { StyleSheet, Text, View ,ImageBackground,ScrollView,TouchableOpacity,Image,TouchableHighlight} from 'react-native'
import React ,{useEffect,useState} from 'react'
import { background, banktransfer, icondown, tiga } from '../../assets'
import FitImage from 'react-native-fit-image';
import { Header } from '../../components';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import { reducerForgot } from '../../redux/reducer/forgot';

const PengajuanPembayaran2 = ({route,navigation}) => {
    // const [billing,setBilling]=useState("");
    const{billing}=route.params;
    console.log('woi',route.params)
    const {pembayaran}=route.params;
    const {no_aju} = route.params;
    const [data,setData]=useState({});
    const [value,setValue]=useState('');
    const {reducerSession} = useSelector(state => state);
    const [uri_path,setUriPath]=useState('');
    const [namabank,setNamaBank]=useState('');
    const [option2,setOption2]=useState([]);
    const [name,setName]=useState("");
    
const option2s = [
	{
    gbr: require('../../assets/image/banktransfer.png'),
		key: 'banktransfer',
		text: 'Bank Transfer',
	},
];

useEffect(() => {
    _getbank();
  }, []);
const _getbank=  () => {
    const token = reducerSession.token
    console.log('tokenbank',token)
    // var datax = this.state.data
    // var valuetipe = this.state.valuex
    
    if (token == false){
      alert(constant.pesan_relog)
      navigation.navigate('Login')
    }
    axios({
      method: 'get',
      url: 'http://apiv2.bbkpsoetta.com/banks',
      headers: {
        'Authorization': 'Bearer ' +token
      }
    })
    .then(response => {
      console.log('databank',response.data.data);
      setOption2(response.data.data)
      console.log('option2',response.data.data)
    //   const names = data.filter(item => item.account_owner_name === setName) 
      console.log('oi',names)
    //   const status = response.data.code
    //   if (status == 200){
    //     setOption2(response.data.data)
    //   } else {
    //     alert('gagal ambil data bank')
    //   }
    })
    .catch(function (error) {
      alert('gagal ambil data bank')
      console.log('yah error',+error);
    });
  }
  return (
    <ImageBackground source={background} style={{width: '100%', height: '100%'}}>

        <View style={{ flex: 1}}>
        <Header onPress={() => navigation.pop()} title={'Pengajuan Pembayaran'} />
          <ScrollView>
            <View style={{flex:1,backgroundColor:'',marginBottom:20}}>
              <FitImage
                resizeMode="cover"
                originalWidth={400}
                originalHeight={50}
                source={tiga}
                />
            </View>
            <View style={{flex:1,alignItems:'center',marginTop:0}}>
              <Text style={{fontSize:30,color:'#917369',fontWeight:'bold'}}>Pembayaran </Text>
              
            </View> 
            <View style={{flex:3,marginHorizontal:30,marginTop:20}}>
              <View style={{flex:1,marginBottom:10}}>
                <View style={styles.shadowView}>
                  <View style={{flex:1,flexDirection:'row'}}>          
    
                    <View style={{flex:5,alignContent:'flex-start',paddingVertical:10}}>
                      <Text style={{fontSize:15,fontWeight:'bold'}}>Detail Permohonan</Text>
                    </View>
                    <View style={{flex:1,alignItems:'flex-end',paddingVertical:8,paddingRight:10}}>

                      <Image style={{width:25,height:25,alignContent:'flex-end'}} source={icondown}></Image>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{flex:1,marginBottom:10}}>
                <View style={styles.kotakBilling}>
                  <View style={{flex:1,flexDirection:'row'}}>          
                    <View style={{flex:7,alignItems:'flex-start'}}>
                      <Text style={{color:'white'}}>No. Billing</Text>
                    </View>
                    <View style={{flex:2}}>
                      <Text style={{color:'white'}}>{billing}</Text>
                    </View>
                  </View>
                  
                  <View style={{flex:1,flexDirection:'row'}}>          
                    <View style={{flex:1,alignItems:'flex-start'}}>
                      <Text style={{color:'white'}}>Total Pembayaran</Text>
                    </View>
                    {pembayaran &&
                    <View style={{flex:1,alignItems:'flex-end'}}>
    <Text style={{color:'white',fontSize:15,fontWeight:'bold'}}>Rp. {pembayaran}</Text>
                    </View>}
                  </View>

                </View>
              </View>

            </View>
            <View style={styles.kotakPembayaran}>
              <View style={{flex:1,marginBottom:10}}>
                <Text style={{fontSize:15,fontWeight:'bold'}}>Pembayaran Via Bank Transfer</Text>              
              </View>
              <View style={{flex:1}}>
                <View style={styles.kotakBayarViaBank}>
                  <View style={{flex:1}}>
                  {option2.map(item => {
                    if(item.status == 'active'){
                    return (
                      <View style={{flex:1,flexDirection:'row',borderBottomColor:'#dbdbde',borderBottomWidth:1}}>
                        <View style={{flex:6,paddingVertical:7}}> 

                          <View key={item.id} style={styles.buttonContainer}>
                            <Text style={{fontWeight:'bold'}}>{item.name}</Text>
                            <TouchableOpacity
                              style={styles.circle}
                              onPress={() => {
                                setValue({
                                  value: item.id,
                                  namabank: item.name,
                                  nama: item.account_owner_name,
                                  norek: item.account_number
                                });
                              }}
                            >
                              {value === item.id && <View style={styles.checkedCircle} />}
                            </TouchableOpacity>
                          </View>
                        </View>

                      </View>
                    );
                            }
                  })}
                </View>
                </View>
              </View>
              <View style={{flex:1}}>
                <Text>Catatan:</Text>
              </View>
              <View style={{flex:1,marginBottom:10,paddingLeft:5,flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <Text>.</Text>
                </View>
                <View style={{flex:15}}>
                  <Text>Jika transfer anda diminta memasukan kode bank. </Text>
                  {/* <Text>Nomor Rekening : {namabank} - {norek}</Text> */}
                  {/* <Text>Atas Nama : {nama}</Text> */}
                </View>
                
              </View>
              <View style={{flex:1,marginBottom:10,paddingLeft:5,flexDirection:'row'}}>
                <View style={{flex:1}}>
                  <Text>.</Text>
                </View>
                <View style={{flex:15}}>
                  <Text>Anda mungkin akan dikenakan biaya tambahan jika transfer dilakukan dari rekening selain Bank Permata</Text>
                </View>
              </View>
              <View style={{flex:1}}>
                <TouchableOpacity >
                  <View style={{alignItems:'center',flex:1,backgroundColor:'#917369',
                                marginHorizontal:30,height:40,alignContent:'center',
                                justifyContent:'center',alignItems:'center',
                                marginTop:10,marginBottom:10,borderRadius:25,
                                borderColor:"grey",borderWidth:1}}>          
                      <View><Text style={{color:'white',fontSize:20}}>Bayar</Text></View>
                  </View>
                </TouchableOpacity>
              </View>
              
            </View>
            <View style={{flex:1,alignItems:'center',backgroundColor:'#917369',paddingVertical:10}}>
                <Text style={{color:'white'}}>Silahkan click tombol Bayar</Text>
                {/* <Text style={{color:'white'}}>telah ditunjuk sebelum xxx.xx.xx.xx</Text> */}
                {/* <Text style={{color:'white'}}>pukul xx.xx</Text>
                <Text style={{color:'white'}}>pukul xx.xx. Apabila terlewati maka harus</Text>
                <Text style={{color:'white'}}>melakukan permohonan ulang</Text> */}
            </View>
          </ScrollView>

        </View>
        </ImageBackground>
  )
}

export default PengajuanPembayaran2

const styles = StyleSheet.create({
    jusView: {
        shadowOffset: { width: 10, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 0.8,
        // elevation: 10,
        backgroundColor:'white',marginBottom:10,
        paddingLeft:10,paddingVertical:10,borderRadius:25,
        borderWidth:0
    },
    shadowView: {
        shadowOffset: { width: 10, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 0.8,
        elevation: 10,
        backgroundColor:'white',marginBottom:10,
        paddingLeft:10,paddingVertical:10,borderRadius:25,
        borderWidth:0
    },
    kotakBilling:{
        shadowOffset: { width: 10, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 0.8,
        elevation: 10,
        backgroundColor:'#917369',marginBottom:10,
        paddingHorizontal:10,paddingVertical:10,borderRadius:15,
        borderWidth:0
    },
    kotakBayarViaBank:{
        shadowOffset: { width: 10, height: 10 },
        shadowColor: 'black',
        shadowOpacity: 0.8,
        // elevation: 2,
        backgroundColor:'#FDF5E0',marginBottom:10,
        paddingHorizontal:10,paddingVertical:10,borderRadius:15,
        borderWidth:0
    },
    bigCircle: {
		height: 40,
		width: 40,
		borderRadius: 30,
		borderWidth: 1,
        borderColor: '#917369',
        backgroundColor: '#917369',
		alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center'
	},
    // opt button
    buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginBottom: 10,
	},

	circle: {
		height: 20,
		width: 20,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: '#ACACAC',
		alignItems: 'center',
		justifyContent: 'center',
	},
  
	checkedCircle: {
		width: 14,
		height: 14,
		borderRadius: 7,
		backgroundColor: '#794F9B',
    },
    // kotak pembayaran
    kotakPembayaran: {
        flex:1,backgroundColor:'white',height:'100%',
        paddingHorizontal:30,paddingVertical:10,
        borderTopLeftRadius:20,borderTopRightRadius:20,
        marginTop:10,
        borderColor:'#917369',borderTopWidth:1,
        borderLeftWidth:1,borderRightWidth:1
    },
})